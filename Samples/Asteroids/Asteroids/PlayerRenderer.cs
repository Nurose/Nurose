﻿using Nurose.Graphics;
using Nurose.System;
using Nurose.Types;
using Nurose.Utilities;
using System.Collections.Generic;

namespace Asteroids
{
    public class PlayerRenderer : RenderComponent
    {
        private List<Vertex> vertices = new List<Vertex>();

        public PlayerRenderer(Entity entity) : base(entity)
        {
        }

        public void Update()
        {
            var offset = 15f * Mathf.DegreeToVector(Transform.LocalRotation);
            vertices.Clear();
            vertices.Add(new Vertex(Transform.GlobalPosition, Color.White));
            vertices.Add(new Vertex(Transform.GlobalPosition + Vector2.Right * 25, Color.White));
            vertices.Add(new Vertex(Transform.GlobalPosition + Vector2.One * 25, Color.White));
            vertices.Add(new Vertex(Transform.GlobalPosition + Vector2.Up * 25, Color.White));
        }

        public override void Draw()
        {
            Drawer.DrawVertices(vertices, PrimitiveType.Quads);
        }
    }
}
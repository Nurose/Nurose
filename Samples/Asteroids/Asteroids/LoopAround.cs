﻿using Nurose.System;
using Nurose.Types;
using Nurose.Window;

namespace Asteroids
{
    [SingleComponent]
    public class LoopAround : Component
    {
        public float Padding = 0;

        public LoopAround(Entity entity) : base(entity)
        {
        }

        public void FixedUpdate()
        {
            Transform.LocalPosition = Loop(Transform.LocalPosition, NuroseMain.Window.Size);
        }

        private Vector2 Loop(Vector2 i, Vector2 bounds)
        {
            if (i.X > bounds.X + Padding)
                i.X = -Padding;

            if (i.X < -Padding)
                i.X = bounds.X + Padding;

            if (i.Y > bounds.Y + Padding)
                i.Y = Padding;

            if (i.Y < -Padding)
                i.Y = bounds.Y + Padding;

            return i;
        }
    }
}

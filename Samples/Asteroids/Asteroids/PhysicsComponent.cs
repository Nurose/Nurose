﻿using Nurose.System;
using Nurose.Types;

namespace Asteroids
{
    [SingleComponent]
    public class PhysicsComponent : Component
    {
        public Vector2 PositionalVelocity { get; set; }
        public float RotationalVelocity { get; set; }

        public PhysicsComponent(Entity entity) : base(entity)
        {
        }

        public void FixedUpdate()
        {
            Sync();
        }

        private void Sync()
        {
            Transform.LocalPosition += PositionalVelocity;
            Transform.LocalRotation += RotationalVelocity;
        }

        public void AddForce(Vector2 force)
        {
            PositionalVelocity += force;
        }

        public void AddTorque(float torque)
        {
            RotationalVelocity += torque;
        }
    }
}

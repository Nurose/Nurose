﻿using Nurose.System;
using Nurose.Utilities;
using Nurose.Window;

namespace Asteroids
{
    [RequireComponent(typeof(LoopAround))]
    [SingleComponent]
    public class Projectile : Behaviour
    {
        private const float Speed = 15f;
        private const float Life = 0.4f;

        private readonly float updateCreated;

        public Projectile(Entity entity) : base(entity)
        {
            updateCreated = FixedTime.UpdatesSinceStart;
        }

        public override void FixedUpdate()
        {
            Transform.LocalPosition += Mathf.DegreeToVector(Transform.LocalRotation*-1) * Speed;

            if (FixedTime.UpdatesSinceStart - updateCreated > Life * 60)
                Entity.Scene.Remove(Entity);
        }

        public void Start()
        {
            Entity.GetComponent<LoopAround>().Padding = 5;
        }
    }
}

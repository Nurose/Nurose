﻿using Nurose.Graphics;
using Nurose.System;
using Nurose.Types;
using Nurose.Utilities;
using Nurose.Window;

namespace Asteroids
{
    [RequireComponent(typeof(PhysicsComponent), typeof(LoopAround))]
    [SingleComponent]
    public class PlayerController : Component
    {
        private const float Acceleration = 0.5f;
        private const float RotationSpeed = 0.8f;
        private const float Dampening = 0.99f;

        private PhysicsComponent physics;

        public PlayerController(Entity entity) : base(entity)
        {
        }

        public void Start()
        {
            physics = Entity.GetComponent<PhysicsComponent>();
            Entity.GetComponent<LoopAround>().Padding = 16;
        }

        public void FixedUpdate()
        {
            physics.PositionalVelocity *= Dampening;
            physics.RotationalVelocity *= Dampening * .95f;

            HandleControls();
        }

        private void HandleControls()
        {
            if (NuroseMain.Input.GetKey(Key.W))
            {
                var force = Acceleration * Mathf.DegreeToVector(Transform.LocalRotation);
                force.Y *= -1;
                physics.AddForce(force);

                //for (int i = 0; i < 5; i++)
                //    CreateParticles();
            }

            if (NuroseMain.Input.GetKey(Key.A))
                physics.AddTorque(-RotationSpeed);

            if (NuroseMain.Input.GetKey(Key.D))
                physics.AddTorque(RotationSpeed);

            if (NuroseMain.Input.GetKey(Key.Space) && FixedTime.UpdatesSinceStart % 10 == 0)
                CreateBullet();
        }

        private void CreateParticles()
        {
            var particle = Entity.Scene.Add(Transform.GlobalPosition);

            particle.Transform.Size = Vector2.One * 2;
            var force = -5 * Acceleration * Mathf.DegreeToVector(Transform.LocalRotation) + Mathf.RandomPointInUnitCircle();
            force.Y *= -1;
            particle.AddComponent<LoopAround>();
            particle.AddComponent<PhysicsComponent>().AddForce(force);
            particle.AddComponent<Particle>();
            var rectRenderer = particle.AddComponent<RectangleRenderer>();
            rectRenderer.Color = Color.White;
        }

        private void CreateBullet()
        {
            var bullet = Entity.Scene.Add(Transform.GlobalPosition);

            bullet.Transform.Size = Vector2.One * 5;
            bullet.Transform.LocalRotation = Transform.LocalRotation;

            bullet.AddComponent<LoopAround>();
            bullet.AddComponent<Projectile>();
            var rectRenderer = bullet.AddComponent<RectangleRenderer>();
            rectRenderer.Color = Color.White;
        }
    }
}

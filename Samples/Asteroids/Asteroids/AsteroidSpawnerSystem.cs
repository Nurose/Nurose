﻿using System;
using System.Collections.Generic;
using Nurose.Graphics;
using Nurose.System;
using Nurose.Types;
using Nurose.Utilities;
using Nurose.Window;

namespace Asteroids
{
    public class AsteroidSpawnerSystem : SceneSystem
    {
        private const string Instructions = "Use W to accelerate\nUse A and D to rotate\nHold SPACE to fire";
        private readonly Scene scene;

        public AsteroidSpawnerSystem(Scene scene) : base(scene)
        {
            this.scene = scene;
        }

        public int Score { get; set; }

        public void FixedUpdate()
        {
            if (FixedTime.UpdatesSinceStart % 60 == 0)
                if (scene.GetEntitiesWithComponent<Asteroid>().Count < 10)
                {
                    var position = new Vector2();

                    if (Mathf.RandomBool())
                        position.Y = Mathf.RandomFloat(Mathf.RandomBool() ? 0 : Game.WindowWidth, Game.WindowHeight);
                    else
                        position.X = Mathf.RandomFloat(Game.WindowWidth, Mathf.RandomBool() ? 0 : Game.WindowHeight);

                    CreateAsteroid(Mathf.RandomFloat(50, 70), position);
                }
        }

        public void CreateAsteroid(float size, Vector2 pos)
        {
            var asteroid = scene.Add(pos);

            asteroid.Transform.Size = Vector2.One * size;
            asteroid.Transform.LocalRotation = Mathf.RandomFloat(0, 360);

            var looper = asteroid.AddComponent<LoopAround>();
            looper.Padding = size / 2;

            asteroid.AddComponent<Asteroid>();
             asteroid.AddComponent<AsteroidRenderer>();
        }

        public void Draw()
        {

        }
    }
}

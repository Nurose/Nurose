﻿using Nurose.Graphics;
using Nurose.System;
using Nurose.Types;
using Nurose.Utilities;
using Nurose.Window;
using System.Collections.Generic;

namespace Asteroids
{
    public class AsteroidRenderer : RenderComponent
    {
        private int resolution = 16;
        private int index;
        List<Vertex> vertices = new List<Vertex>();
        private float[] magnitude;
        public AsteroidRenderer(Entity entity) : base(entity)
        {
            magnitude = new float[resolution];
            for (int i = 0; i < resolution - 1; i++)
                magnitude[i] = Mathf.MinMax(0.6f, 1f, Mathf.RandomFloat());

        }

        public override void Draw()
        {
            vertices.Clear();
            float angle = 0;
            for (int i = 0; i < resolution - 1; i++)
            {
                vertices.Add(new Vertex(Mathf.DegreeToVector(angle) * Transform.Size.X * magnitude[i] + Transform.GlobalPosition, Color.White));

                angle += 360f / resolution;
            }
            NuroseMain.Window.RenderTarget.SetModelTransform(Transform.Zero);
            NuroseMain.Window.RenderTarget.RegisterVertices(vertices, out index);
            NuroseMain.Window.RenderTarget.Draw(index, PrimitiveType.TriangleFan);
            //Drawer.DrawVertices(vertices, PrimitiveType.TriangleFan);
        }
    }
}

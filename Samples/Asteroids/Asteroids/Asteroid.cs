﻿
using Nurose.System;
using Nurose.Utilities;

namespace Asteroids
{
    [RequireComponent(typeof(LoopAround))]
    [SingleComponent]
    public class Asteroid : Component
    {
        private float speed = 1f;
        private float rotationSpeed = 0.8f;

        public Asteroid(Entity entity) : base(entity)
        {
        }

        public void FixedUpdate()
        {
            Transform.LocalPosition += Mathf.DegreeToVector(Transform.LocalRotation) * speed;
            CheckCollision();
        }

        private void CheckCollision()
        {
            if (Mathf.Distance(Transform.GlobalPosition,
                    Entity.Scene.FindComponent<PlayerController>().Transform.GlobalPosition) < Transform.Size.X)
            {
                var main = SceneManager.Current.Find("main");
                main.IsActive = false;
                main.IsDrawn = false;

                var gameover = SceneManager.Current.Find("gameOver");
                gameover.IsActive = true;
                gameover.IsDrawn = true;
                return;
            }

            var bullets = Entity.Scene.GetEntitiesWithComponent<Projectile>();

            foreach (Entity bullet in bullets)
            {
                if (Mathf.Distance(Transform.GlobalPosition, bullet.Transform.GlobalPosition) < Transform.Size.X)
                {
                    if (Transform.Size.X >= 15)
                    {
                        float size = Transform.Size.X / 2;

                        for (int i = 0; i < 4; i++)
                            Entity.Scene.GetSystem<AsteroidSpawnerSystem>().CreateAsteroid(size, Transform.GlobalPosition);
                    }

                    Entity.Scene.GetSystem<AsteroidSpawnerSystem>().Score += (int)Entity.Transform.Size.X;

                    Entity.Scene.Remove(bullet);
                    Entity.Scene.Remove(Entity);
                    return;
                }
            }
        }
    }
}

﻿using Nurose.System;
using Nurose.Utilities;
using Nurose.Window;

namespace Asteroids
{
    [RequireComponent(typeof(PhysicsComponent), typeof(LoopAround))]
    [SingleComponent]
    public class Particle : Component
    {
        private PhysicsComponent physics;
        private float life = 0.4f;
        private readonly float updateCreated;

        public Particle(Entity entity) : base(entity)
        {
            updateCreated = FixedTime.UpdatesSinceStart;
        }

        public void Start()
        {
            life = Mathf.RandomFloat(0.2f, 1.5f);
            physics = Entity.GetComponent<PhysicsComponent>();
            Entity.GetComponent<LoopAround>().Padding = 1;
        }

        public void FixedUpdate()
        {
            physics.PositionalVelocity *= 0.98f;

        }
    }
}

﻿using System.Collections.Generic;
using Nurose.Graphics;
using Nurose.System;
using Nurose.Types;

namespace Asteroids
{
    public class GameOverScreen : SceneSystem
    {
        private const string Text = "GAME OVER";

        private Scene scene;

        public GameOverScreen(Scene scene) : base(scene)
        {
            this.scene = scene;
        }
    }
}
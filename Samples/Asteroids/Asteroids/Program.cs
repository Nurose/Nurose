﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nurose.Graphics;
using Nurose.System;
using Nurose.Types;
using Nurose.Window;

namespace Asteroids
{
    public class Game : NuroseMain
    {
        public static float WindowWidth => Window.Width;
        public static float WindowHeight => Window.Height;

        public readonly Scene mainScene;
        public readonly Scene gameOverScene;
        public Game()
        {
            SetWindowType(typeof(Nurose.OpenTK.OpenTKWindow));

            Create("Asteroids!", 800, 600, 60);
            mainScene = SceneManager.Add("main");

            Window.TargetFps = 60;
            gameOverScene = SceneManager.Add("gameOver");
            gameOverScene.IsActive = false;
            gameOverScene.IsDrawn = false;

            mainScene.AddSystem<AsteroidSpawnerSystem>();
            gameOverScene.AddSystem<GameOverScreen>();

            AddPlayer();

            Start();
        }

        private void AddPlayer()
        {
            var player = mainScene.Add("Player");
            player.Transform.LocalPosition = new Vector2(0, 0);
            player.Transform.Size = new Vector2(16, 16);
            player.AddComponent<PhysicsComponent>();
            player.AddComponent<LoopAround>();
            player.AddComponent<PlayerController>();
            player.AddComponent<RectangleRenderer>();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var game = new Game();
        }
    }
}

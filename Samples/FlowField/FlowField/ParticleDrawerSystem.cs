﻿using Nurose.Core;
using System.Linq;

namespace FlowField
{
    public class ParticleDrawerSystem : WorldSystem
    {
        private VertexBuffer VertexBuffer;
        private Vertex[] Vertices;


        public void Start()
        {
            Vertices = new Vertex[0];
            VertexBuffer = new VertexBuffer();

            Drawer.Draw(new SetPixelCoordinates());
            Drawer.SetTexture(Texture.White1x1);
        }

        public void Draw()
        {
            if (Time.SecondsSinceStart < 1)
                return;

            var particles = World.Components.GetAll<Particle>();

            if (Vertices.Length != particles.Count() * 2)
                Vertices = new Vertex[particles.Count() * 2];

            int i = 0;
            foreach (var particle in particles)
            {
                Vertices[i].Color = particle.Color;
                Vertices[i].Position = new Vector3(particle.Position, 1);
                i++;
            }

            VertexBuffer.SetVertices(Vertices);
            Drawer.Draw(VertexBuffer, PrimitiveType.Points);
        }
    }
}

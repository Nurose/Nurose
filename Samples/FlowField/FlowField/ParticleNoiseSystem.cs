﻿

using Nurose.Core;
using SharpNoise;
using SharpNoise.Modules;

namespace FlowField
{
    public class ParticleNoiseSystem : WorldSystem
    {
        private Perlin Perlin { get; set; }
        private Vector2[,] NoiseFieldVelocity { get; set; }
        private float BlockSize { get; set; } = 16;


        public void Start()
        {
            Perlin = new Perlin()
            {
                Frequency = 0.01,
                Quality = NoiseQuality.Fast,
                OctaveCount = 16,
                Lacunarity = 0
            };
            NoiseFieldVelocity = new Vector2[(int)(NuroseMain.Window.Size.X / BlockSize), (int)(NuroseMain.Window.Size.X / BlockSize)];
            UpdateNoiseField();
        }

        public void UpdateNoiseField()
        {
            for (int x = 0; x < NoiseFieldVelocity.GetLength(0); x++)
            {
                for (int y = 0; y < NoiseFieldVelocity.GetLength(1); y++)
                {
                    float value = GetNoiseValue(x, y);
                    NoiseFieldVelocity[x, y] = Utils.RadianToVector(value * Utils.Pi * 2);
                }
            }
        }

        private float GetNoiseValue(int x, int y)
        {
            return (float)Perlin.GetValue(x, y, Time.SecondsSinceStart * 10);
        }

        public void Draw()
        {
            UpdateNoiseField();
            foreach (var particle in World.Components.GetAll<Particle>())
            {
                int x = Utils.MinMax(0, NoiseFieldVelocity.GetLength(0) - 1, (int)(particle.Position.X / BlockSize));
                int y = Utils.MinMax(0, NoiseFieldVelocity.GetLength(1) - 1, (int)(particle.Position.Y / BlockSize));
                particle.Force += NoiseFieldVelocity[x, y];
               // particle.Force += Utils.DegreeToVector(GetNoiseValue((int)particle.Position.X, (int)particle.Position.Y) * 360);
            }
        }

    }

}

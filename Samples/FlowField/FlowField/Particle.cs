﻿using Nurose.Core;

namespace FlowField
{
    public class Particle : Component
    {
        public Vector2 Position;
        public Vector2 Velocity;
        public Vector2 Force;
        public Color Color;
    }
}

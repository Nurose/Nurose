﻿using Nurose.Core;
using Nurose.OpenTK;

namespace FlowField
{
    public class Game
    {
        public void Start()
        {
            NuroseMain.SetGraphicsFactory<OpenTKGraphicsFactory>();
            NuroseMain.SetWindowFactory<OpenTKWindowFactory>();
            NuroseMain.Create("Flow Field", 1080, 720, 60);
            NuroseMain.Window.VSync = false;

            new WorldLoader().Load(NuroseMain.World);
            NuroseMain.Start();
        }
    }
}

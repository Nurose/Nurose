﻿using Nurose.Core;

namespace FlowField
{
    public class ParticlePhysicsSystem : WorldSystem
    {
        private float BorderOffset = -50;

        public void Draw()
        {
            foreach (var particle in World.Components.GetAll<Particle>())
            {
                particle.Velocity = Utils.Lerp(particle.Velocity, particle.Force, .01f);
                particle.Position += particle.Velocity;
                particle.Force = Vector2.Zero;
                BorderSolver(particle);
            }
        }

        private void BorderSolver(Particle p)
        {
            Vector2 pos = p.Position;

            if (pos.X < -BorderOffset)
            {
                p.Position = (NuroseMain.Window.Width + BorderOffset, pos.Y);
            }

            if (pos.Y < -BorderOffset)
            {
                p.Position = (pos.X, NuroseMain.Window.Height + BorderOffset);
            }

            if (pos.X > NuroseMain.Window.Width + BorderOffset)
            {
                p.Position = (-BorderOffset, pos.Y);
            }

            if (pos.Y > NuroseMain.Window.Height + BorderOffset)
            {
                p.Position = (pos.X, -BorderOffset);
            }
        }

    }
}

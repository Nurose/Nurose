﻿using Nurose.Core;

namespace FlowField
{
    public class WorldLoader
    {
        private const float A = .008f;

        public void Load(World world)
        {
            world.Systems.Add<ParticlePhysicsSystem>();
            world.Systems.Add<ParticleNoiseSystem>();
            world.Systems.Add<ParticleDrawerSystem>();

            for (int i = 0; i < 6000; i++)
                AddRandomParticle(world);
        }

        private void AddRandomParticle(World world)
        {
            var id = EntityID.Generate();
            Vector2 pos = Utils.RandomVector2(Vector2.Zero, NuroseMain.Window.Size);
            Color col = new Color(0, 1, 0, A);




            if (pos.X > NuroseMain.Window.Width/2)
            {
                col = new Color(1, .1f, 1, A);
            }
            else
            {
                col = new Color(0, 1, 1, A);

            }



            world.Components.Add(new Particle()
            {
                EntityID = id,
                Position = pos,
                Color = col,
            });
        }
    }
}

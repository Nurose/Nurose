﻿using System.Linq;
using Nurose.Core;

namespace Boids
{
    class BoidLogicSystem : WorldSystem
    {
        private int borderMargin = 60;
        private float borderDirectionImpact = 5;
        
        private float velocity = 7;
        
        private float separation = 10f;
        private float cohesion = 20f;
        private float alignment = 2f;
        
        private float viewDistance = 30;
        private float distanceToSeparate = 20;
        
        public void FixedUpdate()
        {
            foreach (var bird in World.Components.GetAll<Bird>())
            {
                bird.Position += bird.Direction * velocity;
                var nearby = World.Components.GetAll<Bird>().Where(n => Utils.Distance(bird.Position, n.Position) < viewDistance).ToArray();


                var averageNeighborPos = Vector2.Zero;
                var averageNeighborDirection = Vector2.Zero;
                var averageNeighborCenterOfMass = Vector2.Zero;

                var totDis = 0f;
                var centerOfMass = bird.Position;


                foreach (var n in nearby)
                {
                    var distance = Utils.Distance(bird.Position, n.Position);

                    if (distance < distanceToSeparate)
                    {
                        averageNeighborPos += n.Position * (distanceToSeparate - distance);
                        totDis += (distanceToSeparate - distance);
                    }

                    averageNeighborDirection += n.Direction;
                    averageNeighborCenterOfMass += n.Position;
                }
    
                averageNeighborPos /= totDis;
                averageNeighborDirection /= nearby.Length;
                averageNeighborCenterOfMass /= nearby.Length;

                bird.Direction = Utils.Lerp(bird.Direction, (bird.Position - averageNeighborPos).Normalized, Time.FixedDeltaTime * separation);
                bird.Direction = Utils.Lerp(bird.Direction, averageNeighborDirection.Normalized, Time.FixedDeltaTime * cohesion);
                bird.Direction = Utils.Lerp(bird.Direction, (bird.Position - averageNeighborCenterOfMass).Normalized, Time.FixedDeltaTime * alignment);
                
                BorderCheck(bird);
            }
        }

        private void BorderCheck(Bird bird)
        {
            var c = borderDirectionImpact * Time.FixedDeltaTime;
            if (bird.Position.X > NuroseMain.Window.Size.X - borderMargin)
                bird.Direction = Utils.Lerp(bird.Direction, Vector2.Left, c);

            if (bird.Position.X < borderMargin)
                bird.Direction = Utils.Lerp(bird.Direction, Vector2.Right, c);

            if (bird.Position.Y > NuroseMain.Window.Size.Y - borderMargin)
                bird.Direction = Utils.Lerp(bird.Direction, Vector2.Down, c);

            if (bird.Position.Y < borderMargin)
                bird.Direction = Utils.Lerp(bird.Direction, Vector2.Up, c);
        }
    }
}
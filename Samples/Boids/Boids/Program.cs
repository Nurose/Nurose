﻿using System;
using Nurose.Core;
using Nurose.OpenTK;

namespace Boids
{
    class Program
    {
        static void Main(string[] args)
        {
            NuroseMain.SetGraphicsFactory<OpenTKGraphicsFactory>();
            NuroseMain.SetWindowFactory<OpenTKWindowFactory>();
            
            NuroseMain.Create(new WindowConstructorArgs
            {
                Title = "Boids",
                Size = (1020, 720),
                WindowBorder = WindowBorder.Resizable,
                StartCentered = true,
                NumberOfAntiAliasSamples = 8,
            });
            
            NuroseMain.Window.ClearColor = Color.Grey(0.15f);

            var world = NuroseMain.AddLogic<ECSWorld>();
            world.ResourceManager.Hold("arrowTexture", new Texture("Resources/arrow.png"));
            
            world.Systems.Add<BoidLogicSystem>();
            world.Systems.Add<BoidRenderSystem>();
            
            SpawnBirds(world);
            
            NuroseMain.Start();
        }

        private static void SpawnBirds(ECSWorld world)
        {
            for (int i = 0; i < 300; i++)
            {
                var e = new Entity(world.Components);
                e.AddComponent(new Bird
                {
                    Position = Utils.RandomVector2(Vector2.Zero, NuroseMain.Window.Size),
                    Direction = Utils.RandomPointInUnitCircle().Normalized
                });
            }
        }
    }
}
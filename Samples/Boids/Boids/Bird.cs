﻿using Nurose.Core;

namespace Boids
{
    class Bird : Component
    {
        public Vector2 Position;
        public Vector2 Direction;
    }
}
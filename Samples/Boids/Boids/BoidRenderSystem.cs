﻿using Nurose.Core;

namespace Boids
{
    class BoidRenderSystem : WorldSystem
    {
        private readonly UnitRectangleDrawTask unitRectangleDrawTask = new UnitRectangleDrawTask(Color.Orange, 1, false);
        
        public void Draw()
        {
            Drawer.StartGroup(0);
            
            Drawer.StartPixelSpace();
            Drawer.SetTexture(ResourceManager.Get<Texture>("arrowTexture"));

            var size = Vector2.One * 20;
            var halfSize = size / 2;
            
            foreach (var bird in World.Components.GetAll<Bird>())
            {
                Drawer.SetModelMatrix(new TransformData(bird.Position - halfSize, size, -Utils.VectorToDegree(bird.Direction), halfSize).CalcModelMatrix());
                Drawer.Add(unitRectangleDrawTask);
            }

            Drawer.EndPixelSpace();
            Drawer.EndGroup();
        }
    }
}
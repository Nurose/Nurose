﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nurose.Core;

namespace Nurose.Tests.Visual
{
    class MovingComponentTest : VisualTest
    {
        Entity entity;

        public override void Setup()
        {
            World.Systems.Add<WorldLogicSystem>();
            World.Systems.Add<RenderSystem>();

            World.RenderTarget.SetShader(new ShaderProgram(FragmentShader.SingleColor, VertexShader.Default));
             entity = RectanglePrefab.Instantiate(World, Color.Green, (-1.5f, -.5f), Vector2.One);
            entity.AddComponent<MoveComponent>().Direction = Vector2.Right;

            SimulationTime = 1;
        }

        public override void Test()
        {
            Assert.IsTrue(Utils.Difference(entity.GetComponent<Transform>().LocalPosition.X, -.5f) < .06f);
            Assert.AreEqual(Color.Green, World.RenderTarget.GetPixel(World.RenderTarget.Size / 2));
        }
    }

}

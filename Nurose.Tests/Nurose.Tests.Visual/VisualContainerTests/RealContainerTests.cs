﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Nurose.Tests.Visual.VisualContainerTests
{
    [TestClass]
    public class RealContainerTests
    {
        [TestMethod]
        public void MovingComponentTest()
        {
            var runner = new VisualTestRunner();
           runner.Run<MovingComponentTest>();
        }

        [TestMethod]
        public void RemoveEntityTest()
        {
            var runner = new VisualTestRunner();
            runner.Run<RemoveEntityTest>();

        }
    }
}

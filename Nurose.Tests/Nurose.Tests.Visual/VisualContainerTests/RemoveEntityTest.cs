﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nurose.Core;
using System;

namespace Nurose.Tests.Visual.VisualContainerTests
{
    class RemoveEntityTest : VisualTest
    {
        Entity entity;

        public override void Setup()
        {
            World.Systems.Add<WorldLogicSystem>();
            World.Systems.Add<RenderSystem>();

            World.RenderTarget.SetShader(new ShaderProgram(FragmentShader.SingleColor, VertexShader.Default));

            entity = RectanglePrefab.Instantiate(World, Color.Green, (-1.5f, -.5f),Vector2.One);
            entity.AddComponent<MoveComponent>().Direction = Vector2.Right;
            var suicideComponent = entity.AddComponent<TimedSuicideComponent>();
            suicideComponent.LifeTime = 1;
            SimulationTime = 1.5f;
        }

        public override void Test()
        {
            Assert.AreNotEqual(Color.Green, World.RenderTarget.GetPixel(World.RenderTarget.Size / 2));
        }
    }
}

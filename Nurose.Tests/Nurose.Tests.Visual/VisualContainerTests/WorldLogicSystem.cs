﻿using Nurose.Core;

namespace Nurose.Tests.Visual
{
    public class WorldLogicSystem : WorldSystem
    {
        public struct MoveTransformFam
        {
            public Transform Transform;
            public MoveComponent MoveComponent;
        }


        public void FixedUpdate()
        {
            MoveLogic();
            SuicideLogic();
        }

        private void SuicideLogic()
        {
            foreach (var comp in World.Components.GetAll<TimedSuicideComponent>())
            {
                comp.TimeAlive += Time.FixedDeltaTime;
                if (comp.TimeAlive > comp.LifeTime)
                {
                    World.Components.RemoveAllOf(comp.EntityID);
                }
            }
        }

        private void MoveLogic()
        {
            foreach (var fam in World.Components.GetByFamily<MoveTransformFam>())
            {
                fam.MoveComponent.i++;
                fam.Transform.LocalPosition += fam.MoveComponent.Direction * Time.FixedDeltaTime;
            }
        }
    }

}

﻿using Nurose.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nurose.Tests.Visual.VisualContainerTests;

namespace Nurose.Tests.Visual
{
    public class RedBehindBlueTest : VisualTest
    {
        public override void Setup()
        {
            World.Systems.Add<WorldLogicSystem>();
            World.Systems.Add<RenderSystem>();
            World.RenderTarget.SetShader(new ShaderProgram(FragmentShader.SingleColor, VertexShader.Default));

            var blue = RectanglePrefab.Instantiate(World, Color.Blue, (-.75f, -.75f), Vector2.One * 1.5f, 2);
            var red = RectanglePrefab.Instantiate(World, Color.Red, (-.5f, -.5f), Vector2.One, 8);

            SimulationTime = 1;
        }

        public override void Test()
        {
            VisualAssert.AreSimilear(Color.Blue, World.RenderTarget.GetPixel(World.RenderTarget.Size / 4));
            VisualAssert.AreSimilear(Color.Blue, World.RenderTarget.GetPixel(World.RenderTarget.Size / 2));
        }
    }
}

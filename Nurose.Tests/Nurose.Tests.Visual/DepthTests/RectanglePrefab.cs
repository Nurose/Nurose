﻿using Nurose.Core;
using Nurose.Tests.Visual.VisualContainerTests;

namespace Nurose.Tests.Visual
{
    public static class RectanglePrefab
    {
        public static Entity Instantiate(World world, Color color, Vector2 pos, Vector2 size, float depth = 5)
        {
            var entity = new Entity(world.Components);
            var transform= entity.AddComponent<Transform>();
            transform.LocalPosition = pos;
            transform.LocalSize = size;
            var renderer = entity.AddComponent<RectangleRenderer>();
            renderer.Color = color;
            renderer.Depth = depth;
            return entity;
        }
    }
}

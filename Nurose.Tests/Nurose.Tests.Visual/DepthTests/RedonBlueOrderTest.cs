﻿using Nurose.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nurose.Tests.Visual.VisualContainerTests;

namespace Nurose.Tests.Visual
{
    public class RedOnBlueTest : VisualTest
    {
        public override void Setup()
        {
            World.Systems.Add<WorldLogicSystem>();
            World.Systems.Add<RenderSystem>();
            World.RenderTarget.SetShader(new ShaderProgram(FragmentShader.SingleColor, VertexShader.Default));

            var blue = RectanglePrefab.Instantiate(World, Color.Blue, (-.75f, -.75f), Vector2.One * 1.5f, 8);
            var red = RectanglePrefab.Instantiate(World, Color.Red, (-.5f, -.5f), Vector2.One, 2);
            //To check order
            var blue2 = RectanglePrefab.Instantiate(World, Color.Blue, (-.75f, -.75f), Vector2.One * 1.5f, 8);

            SimulationTime = 2;
        }

        public override void Test()
        {
            VisualAssert.AreSimilear(Color.Blue, World.RenderTarget.GetPixel(World.RenderTarget.Size / 4));
            VisualAssert.AreSimilear(Color.Red, World.RenderTarget.GetPixel(World.RenderTarget.Size / 2));
            VisualAssert.AreSimilear(Color.Blue, World.RenderTarget.GetPixel(World.RenderTarget.Size / 4));
        }
    }
}

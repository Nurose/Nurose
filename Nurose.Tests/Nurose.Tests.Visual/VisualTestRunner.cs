﻿using System;
using Nurose.OpenTK;
using Nurose.Core;
using System.Reflection;
using System.Threading;

namespace Nurose.Tests.Visual
{
    public class VisualTestRunner
    {
        public void Run<T>() where T : VisualTest
        {
            ReflectionSaver.InitCheck(typeof(Transform).Assembly);
            ReflectionSaver.InitCheck(Assembly.GetExecutingAssembly());
            NuroseMain.SetGraphicsFactory<OpenTKGraphicsFactory>();
            NuroseMain.SetWindowFactory<OpenTKWindowFactory>();

            NuroseMain.Create(new WindowConstructorArgs()
            {
                Title = "VisualTest " + typeof(T).Name,
                Size = (800, 800),
            });

            var cam = new Entity(NuroseMain.World.Components);
            cam.AddComponent<Camera>();
            var t = cam.AddComponent<Transform>();
            t.LocalSize = new Vector2(400, 400);

            var test = Activator.CreateInstance<T>();
            test.World = NuroseMain.World;
            test.Setup();
            VisualTestSystem system = NuroseMain.World.Systems.Add<VisualTestSystem>();
            system.TimeToSimulate = test.SimulationTime;
            system.OnSimulationFinished += (obj, e) => test.Test();
            NuroseMain.Start();
        }
    }
}

﻿using Nurose.Core;
using System.Reflection;

namespace Nurose.Tests.Visual
{
    public abstract class VisualTest
    {
        public VisualTest()
        {

        }
        public World World { get; set; }
        public float SimulationTime { get; set; } = 0;

        public static World GetWorld()
        {
            return NuroseMain.World;
        }

        public abstract void Setup();
        public abstract void Test();

    }
}

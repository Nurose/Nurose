﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nurose.Core;

namespace Nurose.Tests.Visual
{
    public static class VisualAssert
    {
        private const float colorFieldAllowedDiff = 1/255f;

        public static void AreSimilear(Color expected, Color actual)
        {
            if (Utils.Difference(expected.R, actual.R) > colorFieldAllowedDiff) { throw new AssertFailedException(); }
            if (Utils.Difference(expected.G, actual.G) > colorFieldAllowedDiff) { throw new AssertFailedException(); }
            if (Utils.Difference(expected.B, actual.B) > colorFieldAllowedDiff) { throw new AssertFailedException(); }
            if (Utils.Difference(expected.A, actual.A) > colorFieldAllowedDiff) { throw new AssertFailedException(); }
        }
    }
}

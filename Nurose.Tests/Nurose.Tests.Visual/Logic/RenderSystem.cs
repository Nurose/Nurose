﻿using Nurose.Core;
using OpenTK.Graphics.OpenGL4;

namespace Nurose.Tests.Visual

{

    public struct DrawRectTask : IRendererTask
    {
        public UnitRectangleDrawTask rectTask;
        public Color Color;
        public Matrix3x2 Matrix;
        public bool flipX;

        public DrawRectTask(UnitRectangleDrawTask rectTask, Color color, Matrix3x2 model, bool flipX = false)
        {
            this.rectTask = rectTask;
            Color = color;
            Matrix = model;
            this.flipX = flipX;
        }

        public void Execute(IRenderTarget renderTarget)
        {
            rectTask.Color = Color;
            rectTask.Depth = 1;
            renderTarget.ModelMatrix = Matrix;

            if (rectTask.InvertedXUV != flipX)
                rectTask.InvertedXUV = flipX;

            // if (rectTask.InvertedYUV != flipY)
            //      rectTask.InvertedYUV = flipY;

            rectTask.Execute(renderTarget);
        }
    }
    public class RenderSystem : WorldSystem
    {
        UnitRectangleDrawTask task = new UnitRectangleDrawTask(Color.White);
        public struct RenderFam
        {
            public Transform Transform;
            public RectangleRenderer RectangleRenderer;
        }

        public void Start()
        {
        }

        public void Draw()
        {
            var camId = World.Components.Get<Camera>().EntityID;
            var camTransform = World.Components.Get<Transform>(camId);
            World.Drawer.StartGroup(float.MaxValue);
            World.Drawer.SetCameraMatrix(camTransform.TransformData.CalcViewMatrix());
            World.Drawer.EndGroup();

            foreach (var fam in World.Components.GetByFamily<RenderFam>())
            {
                Drawer.StartGroup(fam.RectangleRenderer.Depth);
                Drawer.Add(new DrawRectTask()
                {
                    Color = fam.RectangleRenderer.Color,
                    rectTask = task,
                    Matrix = fam.Transform.GetModelMatrix(),
                });
                Drawer.EndGroup();
            }
        }
    }
    /*
    class MovingComponentTest : VisualTest
    {
        public override void Setup()
        {
            World.RenderTarget.SetShader(new ShaderProgram(FragmentShader.SingleColor, VertexShader.Default));
            var entity = EntityID.Generate();
            World.Components.Add(new RectangleRenderer() { Color = Color.Green });
            entity.AddComponent<RectangleRenderer>().Color = Color.Green;
            entity.AddComponent<MoveComponent>().Direction = Vector2.Right;
            World.AddEntity(entity);
            entity.Transform.LocalPosition = new Vector2(-1.5f, -.5f);
            entity.Transform.Depth = 1;
            SimulationTime = 1;
        }

        public override void Test()
        {
            foreach (var comp in World.GetComponentsOfType<Component>())
            {
                comp.Destroy();
            } 
            Assert.IsTrue(Utils.Differance(entity.Transform.LocalPosition.X,-.5f) < .06f);
            Assert.AreEqual(Color.Green, World.RenderTarget.GetPixel(World.RenderTarget.Size / 2));
        }
    }
    */
}

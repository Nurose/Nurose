﻿using Nurose.Core;

namespace Nurose.Tests.Visual
{
    public class TimedSuicideComponent: Component
    {
        public float LifeTime { get; set; }
        public float TimeAlive { get; set; } = 0;
    }
}

﻿using Nurose.Core;

namespace Nurose.Tests.Visual
{
    public class RectangleRenderer : Component
    {
        public Color Color;
        public float Depth;
    }
    /*
    class MovingComponentTest : VisualTest
    {
        public override void Setup()
        {
            World.RenderTarget.SetShader(new ShaderProgram(FragmentShader.SingleColor, VertexShader.Default));
            var entity = EntityID.Generate();
            World.Components.Add(new RectangleRenderer() { Color = Color.Green });
            entity.AddComponent<RectangleRenderer>().Color = Color.Green;
            entity.AddComponent<MoveComponent>().Direction = Vector2.Right;
            World.AddEntity(entity);
            entity.Transform.LocalPosition = new Vector2(-1.5f, -.5f);
            entity.Transform.Depth = 1;
            SimulationTime = 1;
        }

        public override void Test()
        {
            foreach (var comp in World.GetComponentsOfType<Component>())
            {
                comp.Destroy();
            } 
            Assert.IsTrue(Utils.Differance(entity.Transform.LocalPosition.X,-.5f) < .06f);
            Assert.AreEqual(Color.Green, World.RenderTarget.GetPixel(World.RenderTarget.Size / 2));
        }
    }
    */
}

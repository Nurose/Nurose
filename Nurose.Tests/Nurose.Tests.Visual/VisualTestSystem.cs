﻿using Nurose.Core;
using System;
using System.Diagnostics;
using System.Threading;

namespace Nurose.Tests.Visual
{
    public class VisualTestSystem : WorldSystem
    {
        public Stopwatch stopwatch = new Stopwatch();

        public VisualTestSystem()
        {
        }
        public void Start()
        {
            stopwatch.Restart();
        }

        public float TimeToSimulate { get; set; }

        public event EventHandler OnSimulationFinished;

        public void FixedUpdate()
        {
            if (stopwatch.Elapsed.TotalSeconds >= TimeToSimulate)
            {
               // NuroseMain.CatchUp();
                OnSimulationFinished.Invoke(this, new EventArgs());
                NuroseMain.Stop();
            }
        }

    }
}

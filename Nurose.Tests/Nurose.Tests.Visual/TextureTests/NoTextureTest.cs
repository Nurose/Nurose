﻿using Nurose.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nurose.Tests.Visual.TextureTests
{
    
    class NoTextureTest : VisualTest
    {
        public override void Setup()
        {
            World.Systems.Add<RenderSystem>();
            World.RenderTarget.SetShader(new ShaderProgram(FragmentShader.SingleColor, VertexShader.Default));
            var e = new Entity(World.Components);

            var t = e.AddComponent<Transform>().LocalPosition = new Vector2(-.5f, -.5f);
            e.AddComponent<RectangleRenderer>().Color = Color.Orange;
            SimulationTime = 1;
        }

        public override void Test()
        {
            VisualAssert.AreSimilear(Color.Orange, World.RenderTarget.GetPixel(World.RenderTarget.Size / 2));
        }
    }
    
}

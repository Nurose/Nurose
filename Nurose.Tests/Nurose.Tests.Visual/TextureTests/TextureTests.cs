﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nurose.Tests.Visual.TextureTests
{
    [TestClass]
    public class TextureTests
    {
        [TestMethod]
        public void NoTextureTest()
        {
            var runner = new VisualTestRunner();
            runner.Run<NoTextureTest>();
        }
    }
}

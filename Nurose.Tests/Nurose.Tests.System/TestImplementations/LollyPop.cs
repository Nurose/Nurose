﻿using System;
namespace Nurose.Tests.System
{
    public class TextEventArgs : EventArgs
    {
        public string Value { get; private set; }

        public TextEventArgs(string value)
        {
            Value = value;
        }
    }

    public class LollyPop : Candy
    {
        public event EventHandler OnEaten;
        public event EventHandler<TextEventArgs> OnEcho;
        public int i = 0;

        public void Eat()
        {
            OnEaten.Invoke(this, new EventArgs());
        }

        public void EmptyMethod()
        {
            i++;
        }

        public void Echo(string text)
        {
            OnEcho.Invoke(this, new TextEventArgs(text));
        }
    }
}

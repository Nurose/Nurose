﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nurose.Core;
using System;
using System.Linq;
using System.Reflection;

namespace Nurose.Tests.System
{
    public struct Fam
    {
        public SimpleAComponent SimpleAComponent;
        public Transform Transform;
    }
    
    
    [TestClass]
    public class ComponentContainerTests
    {
        public ComponentContainerTests()
        {
            ReflectionSaver.InitCheck(Assembly.GetExecutingAssembly());
        }

        [TestMethod]
        public void MemoryTest()
        {
            var container = new ComponentContainer();
            GC.Collect();
            long before = GC.GetTotalMemory(true);
            container.GetByFamily<(SimpleAComponent, SimpleBComponent)>();

            for (int k = 0; k < 100; k++)
            {
                for (int i = 0; i < 500; i++)
                {
                    var entity = new Entity(container);
                    entity.AddComponent<SimpleAComponent>();
                    if (i % 5 == 0)
                        entity.AddComponent<SimpleBComponent>();
                }

                container.UpdateContent();


                container.Clear();


                container.UpdateContent();
            }

            GC.Collect();
            GC.Collect();
            GC.Collect();
            long after = GC.GetTotalMemory(true);
            double diff = ((double) (after) / before);
            Assert.IsTrue(diff < 1.4);
        }


        [TestMethod]
        public void DeepCopyTest()
        {
            var original = new ComponentContainer();
            for (int i = 0; i < 10; i++)
            {
                var e = new Entity(original);
                e.AddComponent<SimpleAComponent>();
            }
            
            for (int i = 0; i < 10; i++)
            {
                var e = new Entity(original);
                e.AddComponent<SimpleAComponent>();
                e.AddComponent<Transform>();
            }
            
            original.UpdateContent();
            original.GetByFamily<Fam>();

            var clone = original.DeepCopy();
            Assert.AreEqual(original.Count, clone.Count);
            Assert.AreEqual(original.GetByFamily<Fam>().Count(), clone.GetByFamily<Fam>().Count());

            foreach (var fam in clone.GetByFamily<Fam>())
            {
                Assert.IsTrue(clone.GetEntityIDs().Contains(fam.SimpleAComponent.EntityID));
            }
            
            Assert.IsTrue(original.GetEntityIDs().SequenceEqual(clone.GetEntityIDs()));
            
        }
    }
}
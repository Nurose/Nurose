﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nurose.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

namespace Nurose.Tests.System
{
    [TestClass]
    public class MessageCallerTests
    {
        private int lollypopEatenAmount = 0;
        private string lastEchoTextRevieved;



        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        public MessageCallerTests()
        {
            ReflectionSaver.InitCheck(Assembly.GetExecutingAssembly());
        }

        [TestInitialize]
        public void BeforeTest()
        {
            lollypopEatenAmount = 0;
        }


        [TestMethod]
        public void SendMessageTest()
        {
            CandyBox candyBox = GetLollyBox();
            Assert.AreEqual(0, lollypopEatenAmount);
            candyBox.SendMessage("Eat");
            Assert.AreEqual(1, lollypopEatenAmount);
        }

        /*
        [TestMethod]
        public void SendMessageParameterTest()
        {
            CandyBox candyBox = GetLollyBox();
            candyBox.SendMessage("Echo");
            Assert.AreEqual("test", lastEchoTextRevieved);

        }
        */

        [TestMethod]
        public void RemoveContainableTest()
        {
            CandyBox candyBox = GetLollyBox();
            Assert.AreEqual(0, lollypopEatenAmount);
            candyBox.UpdateContent();
            candyBox.Clear();
            candyBox.UpdateContent();
            candyBox.SendMessage("Eat");
            Assert.AreEqual(0, lollypopEatenAmount);
        }

        [TestMethod]
        public void ChildMessageTest()
        {
            GenericBox<object> box = new GenericBox<object>();
            box.UpdateContent();
            box.Add(GetLollyBox());
            box.UpdateContent();
            box.SendMessage("Eat");
            Assert.AreEqual(1, lollypopEatenAmount);
        }

        private CandyBox GetLollyBox()
        {
            CandyBox candyBox = new CandyBox();
            LollyPop lollypop = new LollyPop();
            lollypop.OnEaten += Lollypop_OnEaten;
            lollypop.OnEcho += Lollypop_OnEcho;
            candyBox.Add(lollypop);
            candyBox.UpdateContent();
            return candyBox;
        }


        [TestMethod]
        public void SendMessageSpeedTest()
        {
            CandyBox candyBox = new CandyBox();
            int pop_amount = 10000;
            int call_amount = 1000;
            List<LollyPop> lollyPops = new List<LollyPop>();
            for (int i = 0; i < pop_amount; i++)
            {
                LollyPop pop = new LollyPop();
                lollyPops.Add(pop);
                candyBox.Add(pop);
            }
            candyBox.UpdateContent();
            Stopwatch stopwatch = new Stopwatch();


            GC.Collect();
            stopwatch.Start();
            for (int k = 0; k < call_amount; k++)
            {
                for (int i = 0; i < pop_amount; i++)
                {
                    lollyPops[i].EmptyMethod();
                }
            }
            long normalCallTicks = stopwatch.ElapsedTicks;

            stopwatch.Restart();
            for (int k = 0; k < call_amount; k++)
            {
                candyBox.SendMessage("EmptyMethod");
            }



            long sendMessageTicks = stopwatch.ElapsedTicks;

            for (int i = 0; i < pop_amount; i++)
            {
                Assert.AreEqual(call_amount * 2, lollyPops[i].i);
            }


            double ratio =
                (double)sendMessageTicks / normalCallTicks;
            testContextInstance.WriteLine("SendMessage call speed compared to direct call = " + ratio.ToString());
        }




        private void Lollypop_OnEcho(object sender, TextEventArgs e)
        {
            lastEchoTextRevieved = e.Value;
        }

        private void Lollypop_OnEaten(object sender, global::System.EventArgs e)
        {
            lollypopEatenAmount++;
        }
    }
}

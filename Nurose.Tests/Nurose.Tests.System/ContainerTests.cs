using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nurose.Core;
using System;
using System.Reflection;

namespace Nurose.Tests.System
{

    [TestClass]
    public class ContainerTests
    {
        public ContainerTests()
        {
            ReflectionSaver.InitCheck(Assembly.GetExecutingAssembly());
        }

        [TestMethod]
        public void AddTest()
        {
            CandyBox candyBox = new CandyBox();
            Assert.AreEqual(0, candyBox.GetAll().Count);
            candyBox.Add(new JellyBean());
            candyBox.UpdateContent();
            Assert.AreEqual(1, candyBox.GetAll().Count);

        }

        [TestMethod]
        public void RemoveTest()
        {
            CandyBox candyBox = new CandyBox();
            Assert.AreEqual(0, candyBox.GetAll().Count);
            JellyBean jellybean = new JellyBean();
            candyBox.Add(jellybean);
            candyBox.UpdateContent();
            Assert.AreEqual(1, candyBox.GetAll().Count);
            candyBox.Remove(jellybean);
            Assert.AreEqual(1, candyBox.GetAll().Count);
            candyBox.UpdateContent();
            Assert.AreEqual(0, candyBox.GetAll().Count);
        }

        [TestMethod]
        public void UpdateContentTest()
        {
            CandyBox candyBox = new CandyBox();
            Assert.AreEqual(0, candyBox.GetAll().Count);
            candyBox.Add(new JellyBean());
            Assert.AreEqual(0, candyBox.GetAll().Count);
            candyBox.UpdateContent();
            Assert.AreEqual(1, candyBox.GetAll().Count);
        }



        /// <summary>
        /// Note: <see cref="JellyBean"/> has 0 methods so  <see cref="MessageCaller"/> should have no influence on this test.
        /// </summary>
        [TestMethod]
        public void MemoryTestJellyBeans()
        {

            CandyBox candyBox = new CandyBox();
            long before = GC.GetTotalMemory(true);
            for (int k = 0; k < 100; k++)
            {
                for (int i = 0; i < 1000; i++)
                {
                    candyBox.Add(new JellyBean());
                }
                candyBox.UpdateContent();
                candyBox.Clear();
                candyBox.UpdateContent();
            }
            long after = GC.GetTotalMemory(true);
            double diff = ((double)(after) / before);
            Console.WriteLine("Memory test (without methods) = " + diff);
            Assert.IsTrue(diff < 1.05);
        }

        /// <summary>
        /// Note:  <see cref="LollyPop"/> has methods so <see cref="MessageCaller"/> should have influence on this test.
        /// </summary>
        [TestMethod]
        public void MemoryTestLollypop()
        {
            CandyBox candyBox = new CandyBox();
            GC.Collect();
            long before = GC.GetTotalMemory(true);
            for (int k = 0; k < 100; k++)
            {
                for (int i = 0; i < 500; i++)
                {
                    candyBox.Add(new LollyPop());
                }
                candyBox.UpdateContent();
                candyBox.Clear();
                candyBox.UpdateContent();
            }
            GC.Collect();
            long after = GC.GetTotalMemory(true);
            double diff = ((double)(after) / before);
            Assert.IsTrue(diff < 1.4);
        }
    }
}

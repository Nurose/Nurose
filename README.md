# Nurose 

Nurose is a minimal game engine, using OpenTK for windows, input, and rendering.



## The goal of this engine
Nurose is a C# 2D engine with the goal to be able to quickly prototype a game or create a visual program. It is structured in a way to allow extension and provide the necessities of a game / visual . In addition to the basics we added components like a versatile UI system for easy interaction with the game. 
### What Nurose provides


- Entity-component system 
- Message system
- Window creation and management
- Low level rendering 
- Input handeling
- Sound system



### What it will not provide

- Collision
- UI system
- Physics
- Particle system

# Empty project
For Nurose to run you need to inherit of NuroseMain and put your code before the Start call

```csharp
    public class Game : NuroseMain
    {
        public Game()
        {
            Create("A simple game", 1280, 720, 60);
            SetWindow<Nurose.OpenTK.Window>();
            Start();
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            var game = new Game();
        }
    }
    
```

##  Example projects 
These projects can be found in the Examples folder when they are done.
 
- [x] Flow field
- [ ] Asteroids
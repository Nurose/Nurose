using Nurose.Core;
using Nurose.Text;

namespace Nurose
{
    public struct DrawTextTask : IRenderTask
    {
        public TextDrawer textRenderQueue;
        private readonly BMFont font;
        private readonly string text;
        private readonly bool centered;
        private readonly Color color;
        private readonly float depth;

        public DrawTextTask(TextDrawer textRenderQueue, BMFont font, string text, bool centered, Color color, float depth)
        {
            this.textRenderQueue = textRenderQueue;
            this.font = font;
            this.text = text;
            this.centered = centered;
            this.color = color;
            this.depth = depth;
        }

        public void Execute(IRenderTarget renderTarget)
        {
            textRenderQueue.BMFont = font;
            textRenderQueue.Text = text;
            textRenderQueue.Centered = centered;
            textRenderQueue.Color = color;
            textRenderQueue.InvertedY = false;
            textRenderQueue.ScaleStyle = ScaleStyle.ForceLineHeight;
            textRenderQueue.Depth = depth;
            textRenderQueue.Text = text;
            textRenderQueue.Execute(renderTarget);
        }
    }
}
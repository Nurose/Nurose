﻿using Nurose.Core;
using System.Linq;

namespace Nurose.Text
{
    public enum ScaleStyle
    {
        ForceLineHeight,
        ForceSize
    }

    public class TextDrawer : IRenderTask
    {
        public BMFont BMFont
        {
            get => bMFont;
            set
            {
                bMFont = value;
                needUpdate = true;
                UpdateFontInfo();
            }
        }

        public string Text
        {
            get => text;

            set
            {
                if (text == value)
                    return;

                text = value;
                needUpdate = true;
            }
        }

        public bool InvertedY
        {
            get => invertY;
            set
            {
                invertY = value;
                needUpdate = true;
            }
        }
        public bool Centered
        {
            get => centered;
            set
            {
                centered = value;
                needUpdate = true;
            }
        }
        public Color Color
        {
            get => color;
            set
            {
                color = value;
                needUpdate = true;
            }
        }
        public ScaleStyle ScaleStyle
        {
            get => scaleStyle;
            set
            {
                scaleStyle = value;
                needUpdate = true;
            }
        }
        public float Depth { get; set; } = 5;


        private Color color = Color.White;
        private bool needUpdate = true;
        private bool centered = false;
        private bool invertY = true;
        private string text;

        private Vector2 imageSize;
        private Vector2 pixelSize;
        private ScaleStyle scaleStyle;
        private BMFont bMFont;
        private readonly VertexBuffer vertexBuffer;

        public TextDrawer(BMFont fNTFont)
        {
            bMFont = fNTFont;
            UpdateFontInfo();

            vertexBuffer = new VertexBuffer(null);
        }

        public TextDrawer()
        {
            vertexBuffer = new VertexBuffer(null);
        }


        private void UpdateFontInfo()
        {
            imageSize = new Vector2(BMFont.FontFile.Common.ScaleW, BMFont.FontFile.Common.ScaleH);
            pixelSize = 1 / imageSize;
        }

        public int CalcCharWidth(int lineHeight, char c)
        {
            int width = 0;
            FontChar fontChar = BMFont.FindChar(c);
            if (fontChar == null)
                fontChar = BMFont.FindChar('?');

            if (c != '\n' && c != '\r')
                width = fontChar.XAdvance;

            return (int)(width / (float)BMFont.FontFile.Info.Size * lineHeight);
        }

        public int CalcTextWidth(int lineheight, string text)
        {
            int width = 0;
            for (int i = 0; i < text.Length; i++)
            {
                char c = text[i];
                FontChar fontChar = BMFont.FindChar(c);
                if (fontChar == null)
                    fontChar = BMFont.FindChar('?');

                if (c != '\n' && c != '\r')
                {
                    width += fontChar.XAdvance;
                }

                if (i == text.Length - 1)
                    width -= fontChar.XOffset;
            }
            return (int)(width / (float)BMFont.FontFile.Info.Size * lineheight);
        }


        public Vertex[] GetVertices()
        {
            if (needUpdate)
                UpdateBuffer();

            return vertexBuffer.GetVertices();
        }

        private void UpdateBuffer()
        {
            Vertex[] vertices = new Vertex[Text.Length * 6];
            int currentX = 0;
            float maxX = 0;
            float maxHeight = 0;
            for (int i = 0; i < Text.Length; i++)
            {
                char c = Text[i];
                FontChar fontChar = BMFont.FindChar(c) ?? BMFont.FindChar('?');

                if (fontChar.Height > maxHeight)
                    maxHeight = fontChar.Height;

                Vector2 startPos = new Vector2(fontChar.X, fontChar.Y);
                Vector2 size = new Vector2(fontChar.Width, fontChar.Height);
                Vector2 uvStartPos = Utils.Multiply(pixelSize, startPos);
                Vector2 uvSize = Utils.Multiply(pixelSize, size);
                Vector2 Offset = new Vector2(currentX + fontChar.XOffset, fontChar.YOffset);

                vertices[i * 6 + 0] = new Vertex(Offset + new Vector2(0, 0) * size, Color, uvStartPos + new Vector2(0, 0) * uvSize);
                vertices[i * 6 + 1] = new Vertex(Offset + new Vector2(0, 1) * size, Color, uvStartPos + new Vector2(0, 1) * uvSize);
                vertices[i * 6 + 2] = new Vertex(Offset + new Vector2(1, 0) * size, Color, uvStartPos + new Vector2(1, 0) * uvSize);

                vertices[i * 6 + 3] = new Vertex(Offset + new Vector2(1, 1) * size, Color, uvStartPos + new Vector2(1, 1) * uvSize);
                vertices[i * 6 + 4] = new Vertex(Offset + new Vector2(0, 1) * size, Color, uvStartPos + new Vector2(0, 1) * uvSize);
                vertices[i * 6 + 5] = new Vertex(Offset + new Vector2(1, 0) * size, Color, uvStartPos + new Vector2(1, 0) * uvSize);

                if (invertY)
                {
                    Offset = new Vector2(currentX + fontChar.XOffset, BMFont.FontFile.Common.Base - fontChar.Height - fontChar.YOffset);
                    vertices[i * 6 + 0] = new Vertex(Offset + new Vector2(0, 1) * size, Color, uvStartPos + new Vector2(0, 0) * uvSize);
                    vertices[i * 6 + 1] = new Vertex(Offset + new Vector2(0, 0) * size, Color, uvStartPos + new Vector2(0, 1) * uvSize);
                    vertices[i * 6 + 2] = new Vertex(Offset + new Vector2(1, 1) * size, Color, uvStartPos + new Vector2(1, 0) * uvSize);
                    vertices[i * 6 + 3] = new Vertex(Offset + new Vector2(1, 0) * size, Color, uvStartPos + new Vector2(1, 1) * uvSize);
                    vertices[i * 6 + 4] = new Vertex(Offset + new Vector2(0, 0) * size, Color, uvStartPos + new Vector2(0, 1) * uvSize);
                    vertices[i * 6 + 5] = new Vertex(Offset + new Vector2(1, 1) * size, Color, uvStartPos + new Vector2(1, 0) * uvSize);
                }


                if (currentX + size.X > maxX)
                    maxX = currentX + size.X;

                currentX += fontChar.XAdvance;
            }


            if (centered)
            {
                for (int i = 0; i < vertices.Length; i++)
                {
                    vertices[i].Position -= new Vector3(maxX / 2f, 0, 0);
                }
            }

            for (int i = 0; i < vertices.Length; i++)
            {
                switch (ScaleStyle)
                {
                    case ScaleStyle.ForceLineHeight:
                        vertices[i].Position = new Vector3(vertices[i].Position.X / BMFont.FontFile.Info.Size, vertices[i].Position.Y / BMFont.FontFile.Info.Size, Depth);
                        break;
                    case ScaleStyle.ForceSize:
                        vertices[i].Position = new Vector3(vertices[i].Position.X / maxX, vertices[i].Position.Y / maxX, Depth);
                        break;
                }
            }

            vertexBuffer.SetVertices(vertices);
        }

        public void Execute(IRenderTarget RenderTarget)
        {
            if (needUpdate)
            {
                UpdateBuffer();
                needUpdate = false;
            }
            RenderTarget.Texture = BMFont.Texture;
            RenderTarget.RenderVertexBuffer(vertexBuffer, PrimitiveType.Triangles);
        }
    }
}
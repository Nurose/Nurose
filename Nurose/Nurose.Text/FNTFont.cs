﻿using Nurose.Core;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Nurose.Text
{
    //TEST
    public class BMFontGroup
    {
        public Dictionary<int, BMFont> Fonts = new();
        public int MaxSize { get; private set; }
        public BMFontGroup(string folder)
        {
            foreach (var fontPath in Directory.GetFiles(folder, "*.fnt"))
            {
                var fnt = new BMFont(fontPath);
                int size = fnt.FontFile.Info.Size;
                Fonts.Add(size, fnt);

                if (size > MaxSize)
                    MaxSize = size;
            }
        }

        public BMFont GetFont(int lineHeight)
        {
            lineHeight = GetNearestValidSize(lineHeight);
            return Fonts[lineHeight];
        }

        public int GetNearestValidSize(int lineHeight)
        {
            while (!Fonts.ContainsKey(lineHeight))
            {
                lineHeight++;
                if (lineHeight >= MaxSize)
                {
                    lineHeight = MaxSize;
                    break;
                }
            }

            return lineHeight;
        }
    }
    public class BMFont
    {
        public Texture Texture { get; private set; }
        internal FontFile FontFile;
        public Dictionary<char, FontChar> fontchars = new();

        internal FontChar FindChar(char c)
        {
            fontchars.TryGetValue(c, out FontChar val);
            return val;
        }

        public BMFont(string path)
        {
            FontFile = FontLoader.Load(path);
            if (FontFile is null)
            {
                return;
            }
            Texture = new Texture(Path.GetDirectoryName(path) + "\\" + FontFile.Pages[0].File) { MagFilter = TextureFilter.Linear, MinFilter = TextureFilter.Linear };


            foreach (var c in FontFile.Chars)
            {
                fontchars.Add((char)c.ID, c);
            }

        }
    }
}

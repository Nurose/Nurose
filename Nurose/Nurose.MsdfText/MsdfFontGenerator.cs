﻿using Newtonsoft.Json;
using Nurose.Core;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text;

namespace Nurose.MsdfText
{
    public static class MsdfFontGenerator
    {

        public static MsdfFont Generate(MsdfFontGenerateArgs parameters)
        {
            var folderPath = parameters.FolderPath;
            var fontName = new DirectoryInfo(Path.GetDirectoryName(folderPath + "/")).Name;
            var genFolderPath = $"{parameters.FolderPath}/generated";
            var ttfFilePath = $"{folderPath}/{fontName}.ttf";
            var charsetFilePath = $"{folderPath}/charset.txt";
            var genCharsetFilePath = $"{genFolderPath}/charset.txt";
            var genImagePath = $"{genFolderPath}/texture.png";
            var genInfoPath = $"{genFolderPath}/info.json";
            var md5Path = $"{genFolderPath}/.md5";
            var needsGen = true;


            var md5String = CreateMd5ForFolder(folderPath);
            if (Path.Exists($"{genFolderPath}") && Path.Exists($"{md5Path}") && File.ReadAllText(md5Path) == md5String)
            {
                needsGen = false;
            }


            /*            
                        string imagePath = $"Resources/Fonts/Generated/{Path.GetFileNameWithoutExtension(parameters.FontPath)}.png";
                        string fontInfoPath = $"Resources/Fonts/Generated/{Path.GetFileNameWithoutExtension(parameters.FontPath)}.json";
                        string cacheFolderName = $"Resources/Fonts/{Path.GetFileNameWithoutExtension(parameters.FontPath)}-generated";
                        string cachedCharsetPath = $"{cacheFolderName}/charset.txt";
                        var gen = true;

                        if (File.Exists(imagePath) && File.Exists(fontInfoPath))
                        {
                            gen = false;
                            if (!File.Exists(cachedCharsetPath) || File.ReadAllText(cachedCharsetPath) != File.ReadAllText(parameters.CharsetPath))
                                gen = true;
                        }*/
            if (needsGen)
            {
                if (Directory.Exists(genFolderPath))
                    Directory.Delete(genFolderPath, true);

                var call = new StringBuilder();
                call.Append(" -font ");
                call.Append($"\"{ttfFilePath}\"");
                call.Append(" -charset ");
                call.Append($"\"{charsetFilePath}\"");
                call.Append(" -imageout \"");
                call.Append(genImagePath + "\"");
                call.Append(" -json \"" + genInfoPath + "\"");
                call.Append(" -size 32");
                Directory.CreateDirectory(Path.GetRelativePath(Directory.GetCurrentDirectory(), genFolderPath));
                File.WriteAllText(md5Path, md5String);
                ProcessStartInfo psi = new()
                {
                    WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden,
                    Arguments = call.ToString(),
                    FileName = "msdf-atlas-gen",
                    CreateNoWindow = true
                };
                var process = Process.Start(psi);
                process.WaitForExit();
                File.Copy(charsetFilePath, genCharsetFilePath);
            }
            return new MsdfFont(JsonConvert.DeserializeObject<MsdfFontInfo>(File.ReadAllText(genInfoPath)))
            {
                Texture = new Texture(genImagePath) { MagFilter = TextureFilter.Linear, MinFilter = TextureFilter.Linear },
            };
        }

        public static string CreateMd5ForFolder(string path)
        {
            // assuming you want to include nested folders
            var files = Directory.GetFiles(path, "*.*", SearchOption.TopDirectoryOnly)
                                 .OrderBy(p => p).ToList();

            MD5 md5 = MD5.Create();

            for (int i = 0; i < files.Count; i++)
            {
                string file = files[i];

                // hash path
                string relativePath = file.Substring(path.Length + 1);
                byte[] pathBytes = Encoding.UTF8.GetBytes(relativePath.ToLower());
                md5.TransformBlock(pathBytes, 0, pathBytes.Length, pathBytes, 0);

                // hash contents
                byte[] contentBytes = File.ReadAllBytes(file);
                if (i == files.Count - 1)
                    md5.TransformFinalBlock(contentBytes, 0, contentBytes.Length);
                else
                    md5.TransformBlock(contentBytes, 0, contentBytes.Length, contentBytes, 0);
            }

            return BitConverter.ToString(md5.Hash).Replace("-", "").ToLower();
        }
    }
}
﻿using Nurose.Core;

namespace Nurose.MsdfText
{
    public class MsdfShaderProgram
    {
        public static ShaderProgram Instance { get; } = new ShaderProgram(
            Shader.LoadFromCode(
@"
            #version 330 core
            uniform sampler2D mainTex;

            in vec2 uv;
            uniform float screenPxRange;
            in vec2 screenPos;
            in vec4 vertexColor;
            out vec4 color;

            float median(float r, float g, float b) 
            {
                return max(min(r, g), min(max(r, g), b));
            }

            void main()
            {
                vec3 msd = texture(mainTex, uv).rgb;
                float sd = median(msd.r, msd.g, msd.b);
                float screenPxDistance = screenPxRange*(sd - 0.5);
                float opacity = clamp(screenPxDistance + 0.5, 0.0, 1.0);
                color = mix(vec4(vertexColor.rgb,0), vertexColor, opacity);
            }
"),
            VertexShaders.Default
        );
    }
}
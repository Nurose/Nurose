﻿using Nurose.Core;

namespace Nurose.MsdfText
{
    public enum ScaleStyle
    {
        ForceLineHeight,
        ForceSize
    }

    public class MsdfTextDrawer : IRenderTask
    {
        public MsdfFont MsdfFont
        {
            get => font;
            set
            {
                if (value == font)
                    return;

                font = value;
                needUpdate = true;
                UpdateFontInfo();
            }
        }

        public string Text
        {
            get => text;

            set
            {
                if (text == value)
                    return;

                text = value;
                needUpdate = true;
            }
        }

        public bool InvertedY
        {
            get => invertY;
            set
            {
                if (invertY == value)
                    return;

                invertY = value;
                needUpdate = true;
            }
        }
        public bool Centered
        {
            get => centered;
            set
            {
                if (centered == value)
                    return;

                centered = value;
                needUpdate = true;
            }
        }
        public Color Color
        {
            get => color;
            set
            {
                if (color == value)
                    return;

                color = value;
                needUpdate = true;
            }
        }
        public ScaleStyle ScaleStyle
        {
            get => scaleStyle;
            set
            {
                if (scaleStyle == value)
                    return;

                scaleStyle = value;
                needUpdate = true;
            }
        }
        public float Depth { get; set; } = 1;
        public float LineHeight;

        private Color color = Color.White;
        private bool needUpdate = true;
        private bool centered = false;
        private bool invertY = true;
        private string text;

        private Vector2 imageSize;
        private Vector2 pixelSize;
        private ScaleStyle scaleStyle;
        private MsdfFont font;
        private readonly VertexBuffer vertexBuffer;

        public MsdfTextDrawer(MsdfFont fNTFont)
        {
            font = fNTFont;
            UpdateFontInfo();

            vertexBuffer = new VertexBuffer(null);
        }

        public MsdfTextDrawer()
        {
            vertexBuffer = new VertexBuffer(null);
        }


        private void UpdateFontInfo()
        {
            imageSize = new Vector2(MsdfFont.MsdfFontInfo.Atlas.width, MsdfFont.MsdfFontInfo.Atlas.height);
            pixelSize = 1 / imageSize;
        }
        public float CalcCharWidth(int lineheight, char c)
        {
            float width = 0;
            var fontChar = MsdfFont.GetGlyphInfo(c);
            if (fontChar == null)
                fontChar = MsdfFont.GetGlyphInfo('?');

            if (c != '\n' && c != '\r')
                width = fontChar.advance;

            return (width / MsdfFont.MsdfFontInfo.Metrics.lineHeight) * lineheight;
        }
        public float CalcTextWidth(int lineheight, string text)
        {
            float width = 0;
            for (int i = 0; i < text.Length; i++)
            {
                char c = text[i];
                var fontChar = MsdfFont.GetGlyphInfo(c);
                if (fontChar == null)
                    fontChar = MsdfFont.GetGlyphInfo('?');


                if (c != '\n' && c != '\r')
                {
                    width += fontChar.advance;
                }

                // if (i == text.Length - 1)
                //     width -= fontChar.XOffset;
            }
            return (width / MsdfFont.MsdfFontInfo.Metrics.lineHeight) * lineheight;
        }

        public Vertex[] GetVertices()
        {
            if (needUpdate)
                UpdateBuffer();

            return vertexBuffer.GetVertices();
        }

        private void UpdateBuffer()
        {
            if (Text == null)
                return;

            Vertex[] vertices = new Vertex[Text.Length * 6];
            float currentX = 0;
            for (int i = 0; i < Text.Length; i++)
            {
                char c = Text[i];
                var fontChar = MsdfFont.GetGlyphInfo(c);
                if (fontChar == null)
                    fontChar = MsdfFont.GetGlyphInfo('?');
                if (fontChar.atlasBounds != null)
                {
                    float lh = font.MsdfFontInfo.Metrics.lineHeight;
                    float baseHeight = font.MsdfFontInfo.Metrics.descender;

                    Vector2 uvSize = new(1f / font.Texture.Size.X, 1f / font.Texture.Size.Y);

                    var ab = fontChar.atlasBounds;
                    var pb = fontChar.planeBounds;
                    var maxY = font.MsdfFontInfo.Atlas.height;
                    var uvLeftTop = new Vector2(ab.left, maxY - ab.bottom) * uvSize;
                    var uvLeftBot = new Vector2(ab.left, maxY - ab.top) * uvSize;
                    var uvRightTop = new Vector2(ab.right, maxY - ab.bottom) * uvSize;
                    var uvRightBot = new Vector2(ab.right, maxY - ab.top) * uvSize;

                    vertices[i * 6 + 0] = new Vertex(new(currentX + pb.left, baseHeight + lh - pb.top), Color, uvLeftBot);
                    vertices[i * 6 + 1] = new Vertex(new(currentX + pb.left, baseHeight + lh - pb.bottom), Color, uvLeftTop);
                    vertices[i * 6 + 2] = new Vertex(new(currentX + pb.right, baseHeight + lh - pb.top), Color, uvRightBot);
                    vertices[i * 6 + 3] = new Vertex(new(currentX + pb.right, baseHeight + lh - pb.bottom), Color, uvRightTop);
                    vertices[i * 6 + 4] = new Vertex(new(currentX + pb.left, baseHeight + lh - pb.bottom), Color, uvLeftTop);
                    vertices[i * 6 + 5] = new Vertex(new(currentX + pb.right, baseHeight + lh - pb.top), Color, uvRightBot);

                    if (invertY)
                    {
                        var planeLeftTop = new Vector2(pb.left, pb.top);
                        var planeLeftBot = new Vector2(pb.left, pb.bottom);
                        var planeRightTop = new Vector2(pb.right, pb.top);
                        var planeRightBot = new Vector2(pb.right, pb.bottom);

                        vertices[i * 6 + 0] = new Vertex(new Vector2(currentX, baseHeight) + planeLeftTop, Color, uvLeftBot);
                        vertices[i * 6 + 1] = new Vertex(new Vector2(currentX, baseHeight) + planeLeftBot, Color, uvLeftTop);
                        vertices[i * 6 + 2] = new Vertex(new Vector2(currentX, baseHeight) + planeRightTop, Color, uvRightBot);
                        vertices[i * 6 + 3] = new Vertex(new Vector2(currentX, baseHeight) + planeRightBot, Color, uvRightTop);
                        vertices[i * 6 + 4] = new Vertex(new Vector2(currentX, baseHeight) + planeLeftBot, Color, uvLeftTop);
                        vertices[i * 6 + 5] = new Vertex(new Vector2(currentX, baseHeight) + planeRightTop, Color, uvRightBot);
                    }
                }


                currentX += fontChar.advance;
            }


            if (centered)
            {
                for (int i = 0; i < vertices.Length; i++)
                {
                    vertices[i].Position -= new Vector2(currentX / 2f, 0);
                }
            }

            for (int i = 0; i < vertices.Length; i++)
            {
                float width = font.MsdfFontInfo.Atlas.width;
                switch (ScaleStyle)
                {
                    case ScaleStyle.ForceLineHeight:
                        float m = 1f / font.MsdfFontInfo.Metrics.lineHeight;
                        vertices[i].Position = new Vector2(vertices[i].Position.X * m, vertices[i].Position.Y * m);
                        break;
                    case ScaleStyle.ForceSize:
                        vertices[i].Position = new Vector2(vertices[i].Position.X / currentX, vertices[i].Position.Y / currentX);
                        break;
                }
            }

            vertexBuffer.SetVertices(vertices);
        }

        public void SetupContext(IRenderTarget RenderTarget)
        {
            RenderTarget.SetShader(MsdfShaderProgram.Instance);
            RenderTarget.SetCurrentShaderUniform("screenPxRange", Utils.Max(1, LineHeight / 14));
            RenderTarget.SetCurrentShaderUniform("depth", Depth);
            RenderTarget.Texture = font.Texture;
        }

        public void ExecuteAssumeContextValid(IRenderTarget RenderTarget)
        {
            if (needUpdate)
            {
                UpdateBuffer();
                needUpdate = false;
            }
            RenderTarget.RenderVertexBuffer(vertexBuffer, PrimitiveType.Triangles);
        }

        public void Execute(IRenderTarget RenderTarget)
        {
            if (needUpdate)
            {
                UpdateBuffer();
                needUpdate = false;
            }
            RenderTarget.SetShader(MsdfShaderProgram.Instance);
            RenderTarget.SetCurrentShaderUniform("screenPxRange", Utils.Max(1, LineHeight / 14));
            RenderTarget.SetCurrentShaderUniform("depth", Depth);
            RenderTarget.Texture = font.Texture;
            RenderTarget.RenderVertexBuffer(vertexBuffer, PrimitiveType.Triangles);
            RenderTarget.SetShader(ShaderProgram.Default);
        }
    }
}
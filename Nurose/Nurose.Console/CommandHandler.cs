﻿using Nurose.Core;
using Nurose.ECS;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nurose.Console
{
    public class CommandHandler
    {
        private Dictionary<string, Command> CommandsByName { get; set; } = new Dictionary<string, Command>();
        public ECSWorld World { get; set; }

        public List<KeyValuePair<string, Command>> GetCommands()
        {
            return CommandsByName.ToList();
        }

        public void InitilizeCommands()
        {
            AddCommandsInReflectionSaver();
        }

        private void AddCommandsInReflectionSaver()
        {
            ReflectionSaver.InitCheck(typeof(ConsoleSystem));
            foreach (var method in ReflectionSaver.CustomMethods)
            {
                if (method.GetCustomAttributes(typeof(CommandAttribute), true).Any())
                {
                    Add(new Command()
                    {
                        MethodInfo = method,
                        Attribute = method.GetCustomAttributes(typeof(CommandAttribute), true)[0] as CommandAttribute,
                    });
                }
            }
        }


        public Command FindCommand(string name)
        {
            CommandsByName.TryGetValue(name.ToUpperInvariant(), out Command command);
            return command;
        }

        public void Add(Command command)
        {
            CommandsByName.Add(command.MethodInfo.Name.ToUpperInvariant(), command);
        }

        public void Execute(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return;

            var splitted = input.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            var commandName = splitted[0];
            var command = FindCommand(commandName);

            if (command == null)
            {
                Logger.LogAnnouncement($"No command with name {commandName}");
                return;
            }

            var args = new object[] { input };
            object target = null;
            Type declaringType = command.MethodInfo.DeclaringType;
            if (declaringType.IsSubclassOf(typeof(ECSSystem)))
            {
                target = World.Systems.GetOrDefault(declaringType);

                if (target == null)
                {
                    Logger.LogAnnouncement($"No instance of { declaringType.Name} in world.");
                    return;
                }
            }
            if (!command.Attribute.TakesRawInput)
            {
                args = new object[splitted.Length - 1];
                System.Reflection.ParameterInfo[] parameters = command.MethodInfo.GetParameters();
                if (parameters.Length != args.Length)
                {
                    throw new ConsoleException("Wrong number of arguments");
                }
                for (int i = 0; i < parameters.Length; i++)
                {
                    Type parameterType = parameters[i].ParameterType;
                    if (parameterType == typeof(string))
                    {
                        args[i] = splitted[i + 1];
                    }
                    else
                    {
                        try
                        {
                            args[i] = parameterType.GetMethods().Where(m => m.Name == "Parse").First().Invoke(null, new object[] { splitted[i + 1] });
                        }
                        catch (Exception)
                        {

                            throw new ConsoleException($"Can't parse argument {splitted[i]}");
                        }
                    }
                }
            }
            command.MethodInfo.Invoke(target, args);
        }

    }
}
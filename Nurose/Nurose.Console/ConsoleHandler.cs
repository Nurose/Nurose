﻿using Nurose.Core;
using Nurose.ECS;
using Nurose.MsdfText;
using System;
using System.Linq;

namespace Nurose.Console
{
    public class ConsoleHandler
    {
        public readonly CommandHandler CommandHandler = new();
        private VertexBuffer Outline { get; set; }
        private UnitRectangleDrawTask BackgroundRect { get; set; }
        private MsdfTextDrawer TextDrawer { get; set; }

        private readonly MsdfFont font;

        public ConsoleHandler(ECSWorld world, MsdfFont customFont = null)
        {
            CommandHandler.World = world;
            CommandHandler.InitilizeCommands();
            font = customFont;
            if (font == null)
                font = MsdfFontGenerator.Generate(new MsdfFontGenerateArgs
                {
                    FolderPath = "Resources/Fonts/Consolas",
                });
            TextDrawer = new MsdfTextDrawer(font);
            Outline = new VertexBuffer(Array.Empty<Vertex>());
            BackgroundRect = new UnitRectangleDrawTask(Color.Grey(.1f));
            TextDrawer.Text = "";
            TextDrawer.Depth = .02f;
            TextDrawer.InvertedY = false;
        }

        public void RegisterWithLogger(ConsoleData Data)
        {
            Data.LogMessages.AddRange(Logger.GetMessages());
            Logger.OnNewMessage += new EventHandler<LogMessageEventArgs>(
                (sender, eventArgs) =>
                {
                    Data.LogMessages.Add(eventArgs.Message);
                    ScrollToEnd(Data);
                });
        }


        private void UpdateOutlineVertices(ConsoleData Data)
        {
            float depth = .01f;
            Vertex[] vertices = new Vertex[]
            {
                new Vertex(new Vector2(Data.Width - 1, 0), Color.White, Vector2.Zero),
                new Vertex(new Vector2(Data.Width - 1, Data.Height), Color.White, Vector2.Zero),
                new Vertex(new Vector2(1, Data.Height), Color.White, Vector2.Zero),
                new Vertex(new Vector2(1, 0), Color.White, Vector2.Zero),

                new Vertex(new Vector2(1, Data.Height - Data.LineHeight), Color.White, Vector2.Zero),
                new Vertex(new Vector2(Data.Width, Data.Height - Data.LineHeight), Color.White, Vector2.Zero),
            };
            Outline.SetVertices(vertices);
        }

        public void UpdateInput(ConsoleData Data, InputState Input)
        {
            Data.Width = Input.WindowSize.X;

            if (Input.IsKeyPressed(Key.GraveAccent))
            {
                Data.HasFocus = !Data.HasFocus;
                Data.CapturesInput = Data.HasFocus;

                if (Input.IsKeyHeld(Key.LeftShift))
                    Data.CapturesInput = false;

                Input.CharInputs = Input.CharInputs.Replace("`", "", StringComparison.InvariantCulture);
            }

            if (!Data.CapturesInput)
                return;

            if (Input.IsKeyPressed(Key.Backspace) && Data.CurrentInput.Length > 0)
            {
                if (Input.IsKeyHeld(Key.LeftControl))
                {
                    Data.CurrentInput = RemoveLastWord(Data.CurrentInput);
                }
                else
                {
                    Data.CurrentInput = RemoveLastChar(Data.CurrentInput);
                }
            }

            Data.CurrentInput += Input.CharInputs;


            if (Input.IsKeyPressed(Key.Enter))
            {
                try
                {
                    CommandHandler.Execute(Data.CurrentInput);
                }
                catch (ConsoleException e)
                {
                    Logger.LogAnnouncement(e.Message);
                }
                Data.CurrentInput = "";
            }
            ScrollTo(Data.ScrollY - Input.MouseWheelDelta * Data.LineHeight, Data);
            Input = new InputState()
            {
                WindowSize = Input.WindowSize
            };
        }


        public void Draw(ConsoleData data, RenderQueue renderQueue, ResourceManager ResourceManager)
        {
            if (!data.HasFocus)
                return;


            renderQueue.StartGroup(1);
            renderQueue.SetFloatUniform("depth", 0.1f);
            renderQueue.Add(new StoreMatrixes(ResourceManager, "world"));
            renderQueue.Add(new SetPixelCoordinates());
            renderQueue.SetTexture(Texture.White1x1);
            DrawBackground(data, renderQueue);
            DrawInputText(data, renderQueue);
            DrawLogs(data, renderQueue);
            DrawOutline(data, renderQueue);

            renderQueue.Add(new LoadStoredMatricies(ResourceManager, "world"));
            renderQueue.EndGroup();
        }

        private void DrawLogs(ConsoleData data, RenderQueue RenderQueue)
        {
            RenderQueue.Add(new ConsoleRenderTask(data, TextDrawer));
        }

        private void DrawInputText(ConsoleData data, RenderQueue RenderQueue)
        {
            TextDrawer.Text = data.CurrentInput + "|";
            RenderQueue.SetModelMatrix(new TransformData(new Vector2(5, data.Height - data.LineHeight), Vector2.One * (data.LineHeight + 4)).CalcModelMatrix());
            RenderQueue.Add(TextDrawer);
        }

        private static string RemoveLastChar(string text)
        {
            text = text[0..^1];
            return text;
        }

        private static string RemoveLastWord(string text)
        {
            return string.Join(' ', text.Split(' ').Reverse().Skip(1).Reverse());
        }

        private void DrawBackground(ConsoleData data, RenderQueue RenderQueue)
        {
            RenderQueue.SetModelMatrix(new TransformData(Vector2.Zero, new Vector2(data.Width, data.Height)).CalcModelMatrix());
            RenderQueue.Add(BackgroundRect);
        }

        private void DrawOutline(ConsoleData consoleData, RenderQueue RenderQueue)
        {
            UpdateOutlineVertices(consoleData);
            RenderQueue.SetTexture(Texture.White1x1);
            RenderQueue.SetModelMatrix(Matrix3x2.Identity);
            RenderQueue.DrawVertexBuffer(Outline, PrimitiveType.LineLoop);
        }

        private void ScrollToEnd(ConsoleData data)
        {
            int lineAmountVisible = (data.Height / data.LineHeight) - 1;
            if (data.LogMessages.Count > lineAmountVisible)
            {
                ScrollTo((data.LogMessages.Count - lineAmountVisible) * data.LineHeight, data);
            }
        }

        private void ScrollTo(int y, ConsoleData Data)
        {
            int lineAmountVisible = (Data.Height / Data.LineHeight) - 1;

            Data.ScrollY = Utils.MinMax(0, (Data.LogMessages.Count - lineAmountVisible) * Data.LineHeight, y);
        }
    }
}
﻿using Nurose.Core;
using Nurose.ECS;
using System.Collections.Generic;

namespace Nurose.Console
{
    public class ConsoleData : Component
    {
        public List<LogMessage> LogMessages = new();
        public string CurrentInput = "";
        public int ScrollY;
        public bool HasFocus;
        public bool CapturesInput;
        public int Height = (int)(300 * NuroseMain.Monitor.ContentScale);
        public int LineHeight = (int)(21f * NuroseMain.Monitor.ContentScale);
        public int Width = NuroseMain.Window.Width;
        public float ScrollVelocity = 0;

        public override Component Clone()
        {
            var clone = base.Clone() as ConsoleData;
            clone.LogMessages = new List<LogMessage>(LogMessages);
            return clone;
        }
    }
}

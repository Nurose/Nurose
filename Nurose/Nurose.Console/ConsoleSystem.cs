﻿using Nurose.Core;
using Nurose.ECS;
using Nurose.MsdfText;
using System;

namespace Nurose.Console
{
    public class ConsoleSystem : ECSSystem
    {
        public ConsoleHandler ConsoleHandler;
        public ConsoleSystem()
        {
        }

        public void Start()
        {
            var Entity = new Entity(World);
            var Data = Entity.AddComponent<ConsoleData>();
            ConsoleHandler = new ConsoleHandler(World,null);
            ConsoleHandler.RegisterWithLogger(Data);
        }

        [Command]
        public void Vsync(bool v)
        {
            NuroseMain.Window.VSync = v;
            Logger.LogAnnouncement("Vsync=" + v.ToString());
        }


        [Command]
        public void Crash()
        {
            throw new Exception("You asked for a crash. Here you go.");
        }

        [Command]
        public void Echo(string v)
        {
            Logger.LogAnnouncement(v);
        }

        [Command]
        public void Add(int first, int second)
        {
            Logger.LogAnnouncement(first + second);
        }

        [Command]
        public void Devs()
        {
            Logger.LogAnnouncement("98% Besm 2% Mestiez en 100% Somi");
        }

        [Command]
        public void gpu()
        {
            Logger.LogAnnouncement(NuroseMain.Window.GPUVendor);
        }

        [Command]
        public void help()
        {
            foreach (var command in ConsoleHandler.CommandHandler.GetCommands())
            {
                Logger.LogAnnouncement(command.Key.ToLowerInvariant());
            }
        }

        [Command]
        public void Cls()
        {
            var Data = World.Components.Get<ConsoleData>();
            Data.LogMessages.Clear();
        }


        public void FixedUpdate()
        {
            if (NuroseMain.Window.InputRefreshOption == InputRefreshOption.FixedUpdate)
                UpdateInput();
        }

        public void Draw()
        {
            if (NuroseMain.Window.InputRefreshOption == InputRefreshOption.Draw)
                UpdateInput();

            ConsoleHandler.Draw(GetConsoleData(), RenderQueue, ResourceManager);
        }

        private void UpdateInput()
        {
            ConsoleHandler.UpdateInput(GetConsoleData(), Input);
        }

        private ConsoleData GetConsoleData()
        {
            return World.Components.Get<ConsoleData>();
        }
    }
}
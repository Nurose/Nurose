﻿using Nurose.Core;
using Nurose.MsdfText;

namespace Nurose.Console
{
    internal struct ConsoleRenderTask : IRenderTask
    {
        public ConsoleData data;
        public MsdfTextDrawer TextDrawer;

        public ConsoleRenderTask(ConsoleData data, MsdfTextDrawer textDrawer)
        {
            this.data = data;
            TextDrawer = textDrawer;
        }

        public void Execute(IRenderTarget target)
        {
            target.DrawBoundsEnabled = true;
            target.SetDrawBounds(new Vector2Int(0, 0), new Vector2Int(data.Width, data.Height - data.LineHeight));
            int view_start = data.ScrollY - data.LineHeight;
            int view_end = data.ScrollY + data.Height - (data.LineHeight * 2);

            for (int i = 0; i < data.LogMessages.Count; i++)
            {
                if (i * data.LineHeight > view_start && i * data.LineHeight < view_end)
                {

                    LogMessage logMessage = data.LogMessages[i];
                    TextDrawer.Text = logMessage.ToString();
                    target.ModelMatrix = new TransformData(new Vector2(5, data.LineHeight * i - data.ScrollY + 2), Vector2.One * data.LineHeight).CalcModelMatrix();
                    TextDrawer.Execute(target);
                }
            }

            target.DrawBoundsEnabled = false;
        }
    }
}
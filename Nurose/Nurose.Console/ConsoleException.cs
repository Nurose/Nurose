﻿using System;

namespace Nurose.Console
{
    public class ConsoleException : Exception
    {
        public ConsoleException(string message) : base(message)
        {
        }

        public ConsoleException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public ConsoleException()
        {
        }
    }
}
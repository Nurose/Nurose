﻿using System.Reflection;

namespace Nurose.Console
{
    public class Command
    {
        public CommandAttribute Attribute { get; set; }
        public MethodInfo MethodInfo { get; set; }
    }
}


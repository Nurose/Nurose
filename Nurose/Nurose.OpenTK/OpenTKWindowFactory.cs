﻿using Nurose.Core;
using OpenTK.Windowing.Desktop;
using System;

namespace Nurose.OpenTK
{
    public class OpenTKWindowFactory : IWindowFactory
    {

        [System.Runtime.InteropServices.DllImport("nvapi64.dll", EntryPoint = "fake")]
        static extern int LoadNvApi64();

        [System.Runtime.InteropServices.DllImport("nvapi.dll", EntryPoint = "fake")]
        static extern int LoadNvApi32();

        private void InitializeDedicatedGraphics()
        {
            try
            {
                if (Environment.Is64BitProcess)
                    LoadNvApi64();
                else
                    LoadNvApi32();
            }
            catch { } // will always fail since 'fake' entry point doesn't exists
        }

        public IWindow CreateWindow(WindowConstructorArgs args)
        {

            InitializeDedicatedGraphics();

            //needs to be here because of an OpenTK bug.

            Vector2Int pos = GetCenterPos(args.Size);
            NativeWindowSettings settings = new()
            {
                Title = args.Title,
                StartVisible = false,
                Location = new global::OpenTK.Mathematics.Vector2i(pos.X, pos.Y),
                Size = new global::OpenTK.Mathematics.Vector2i(args.Size.X, args.Size.Y),
                WindowBorder = (global::OpenTK.Windowing.Common.WindowBorder)(int)args.WindowBorder,
                NumberOfSamples = args.NumberOfAntiAliasSamples,
                StencilBits = 8,
            };
            return new OpenTKWindow(settings);
        }
        public IMonitor CreateMonitorInfo()
        {
            return new OpenTkMonitor();
        }

        private static Vector2Int GetCenterPos(Vector2Int windowSize)
        {
            IntPtr primary = NativeMethods.GetDC(IntPtr.Zero);
            int DESKTOPVERTRES = 117;
            int DESKTOPHORZRES = 118;
            int actualPixelsX = NativeMethods.GetDeviceCaps(primary, DESKTOPHORZRES);
            int actualPixelsY = NativeMethods.GetDeviceCaps(primary, DESKTOPVERTRES);

            Vector2Int pos = new(actualPixelsX / 2 - windowSize.X / 2, actualPixelsY / 2 - windowSize.Y / 2);
            return pos;
        }
    }
}


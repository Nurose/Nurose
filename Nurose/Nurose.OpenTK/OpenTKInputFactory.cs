﻿using Nurose.Core;

namespace Nurose.OpenTK
{
    public class OpenTKInputFactory : IInputFactory
    {
        public InputHandler CreateInput()
        {
            return new OpenTKInput((OpenTKWindow)NuroseMain.Window, () => NuroseMain.Window.Input);
        }
    }
}


﻿using Nurose.Core;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.IO;

namespace Nurose.OpenTK
{
    internal class ShaderManager
    {
        public int ActiveProgramIndex { get; set; }
        public ShaderProgram ActiveShaderProgram { get; set; }

        private readonly Dictionary<ShaderProgram, int> LoadedShaderPrograms = new();
        private readonly Dictionary<Shader, int> LoadedShaders = new();

        public int GetIndex(ShaderProgram shaderProgram)
        {
            bool loaded = LoadedShaderPrograms.TryGetValue(shaderProgram, out int index);
            if (loaded && shaderProgram.Initialised)
            {
                return index;
            }

            return RegisterShaderProgram(shaderProgram);
        }

        private int RegisterShaderProgram(ShaderProgram shaderProgram)
        {
            OpenGLError.CheckWarn();
            LoadedShaders.Remove(shaderProgram.FragmentShader);
            LoadedShaders.Remove(shaderProgram.VertexShader);
            
            int vertexShader = LoadShader(shaderProgram.VertexShader, ShaderType.VertexShader);
            int fragmentShader = LoadShader(shaderProgram.FragmentShader, ShaderType.FragmentShader);
            int programId;

            if(!LoadedShaderPrograms.ContainsKey(shaderProgram))
            {
                programId = GL.CreateProgram();
                LoadedShaderPrograms.Add(shaderProgram, programId);
            }
            else
            {
                programId = LoadedShaderPrograms[shaderProgram];
            }
            
            GL.AttachShader(programId, vertexShader);
            GL.AttachShader(programId, fragmentShader);
            GL.LinkProgram(programId);

            Logger.LogAnnouncement("ShaderProgram at " + programId + " created and linked");
            GL.GetProgram(programId, GetProgramParameterName.LinkStatus, out int linkstatus);
            Logger.LogAnnouncement("ShaderProgram at " + programId + " linkstatus: " + linkstatus);
            GL.GetProgramInfoLog(programId, out string infoLog);
            if (!string.IsNullOrEmpty(infoLog))
                Logger.LogAnnouncement(programId + " info log: " + infoLog);

            GL.DetachShader(programId, vertexShader);
            GL.DetachShader(programId, fragmentShader);
            GL.DeleteShader(vertexShader);
            GL.DeleteShader(fragmentShader);
            OpenGLError.CheckWarn();
            return programId;
        }

        private int LoadShader(Shader shader, ShaderType type)
        {
            bool loaded = LoadedShaders.TryGetValue(shader, out int index);
            if (loaded)
            {
                return index;
            }
            index = GL.CreateShader(type);
            InitialiseAndCheckShader(shader, index, type);
            Logger.LogAnnouncement($"New {type} created at {index}");
            LoadedShaders.Add(shader, index);
            return index;
        }

        private static void InitialiseAndCheckShader(Shader shader, int index, ShaderType type)
        {
            if (shader.LoadedFromFile)
            {
                Logger.LogDebug($"Loading shader from path '{shader.FilePath}'.");
                GL.ShaderSource(index, File.ReadAllText(shader.FilePath));
            }
            else
            {
                Logger.LogDebug($"Loading shader from source.");
                GL.ShaderSource(index, shader.Code);
            }

            GL.CompileShader(index);

            AssertCompilationSuccess(index, type);
        }

        private static bool AssertCompilationSuccess(int index, ShaderType type)
        {
            GL.GetShaderInfoLog(index, out string info);
            GL.GetShader(index, ShaderParameter.CompileStatus, out int code);
            if (code != 1)
            {
                Logger.LogError($"{type} at {index} error:{Environment.NewLine}{info}", null);
                return false;
            }

            return true;
        }
    }
}
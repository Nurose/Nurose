﻿using Nurose.Core;
using OpenTK.Graphics.OpenGL;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using System;
using System.Buffers;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Color = Nurose.Core.Color;
using PrimitiveType = OpenTK.Graphics.OpenGL.PrimitiveType;

namespace Nurose.OpenTK
{
    public class OpenTKRenderTarget : IRenderTarget
    {
        internal int FrameBufferIndex { get; }
        internal RenderTexture CorrespondingRenderTexture;

        private Blending blending;
        public Blending Blending
        {
            get => blending;
            set
            {
                if (blending != value)
                {
                    blending = value;
                    UpdateBlending();
                }
            }
        }
        public Vector2Int Size { get; set; }

        public int Width => Size.X;
        public int Height => Size.Y;

        internal Matrix3x2 modelMatrix = Matrix3x2.Identity;
        internal Matrix4x4 viewMatrix = Matrix4x4.Identity;
        internal Matrix4x4 projectionMatrix = Matrix4x4.Identity;

        private Color clearColor = Color.White;
        private int shaderProgramID;
        private ShaderProgram currentShaderProgram = null;
        public float near = .1f;
        public float far = 20;



        public OpenTKRenderTarget(Vector2Int size, int FrameBufferIndex)
        {
            OpenGLError.CheckCrashIfDebug();
            GPULoadedData.CheckInitialize();
            this.Size = size;
            this.FrameBufferIndex = FrameBufferIndex;
#if (Debug)
            Logger.LogDebug($"Rendertarget created with size = {size} at Framebuffer={FrameBufferIndex}", this);
#endif
        }


        private bool drawboundEnabled;

        public bool DrawBoundsEnabled
        {
            get => drawboundEnabled;
            set
            {
                drawboundEnabled = value;
                if (CurrentOpenGLContext.CurrentDrawBoundsEnabled != value)
                {
                    if (value)
                        GL.Enable(EnableCap.ScissorTest);
                    else
                        GL.Disable(EnableCap.ScissorTest);

                    CurrentOpenGLContext.CurrentDrawBoundsEnabled = value;
                }

            }
        }

        public Matrix3x2 ModelMatrix
        {
            get => modelMatrix;
            set
            {
                if (modelMatrix != value)
                {
                    modelMatrix = value;
                    PushMatrix(ShaderConstants.ModelMatrix, new Matrix4x4(modelMatrix));
                }
            }
        }

        public Matrix4x4 ViewMatrix
        {
            get => viewMatrix;
            set
            {
                if (viewMatrix != value)
                {
                    viewMatrix = value;
                    PushMatrix(ShaderConstants.ViewMatrix, viewMatrix);
                }
            }
        }

        public Matrix4x4 ProjectionMatrix
        {
            get => projectionMatrix;
            set
            {
                if (projectionMatrix != value)
                {
                    projectionMatrix = value;
                    PushMatrix(ShaderConstants.ProjectionMatrix, projectionMatrix);
                }
            }
        }


        public Color ClearColor
        {
            get => clearColor;
            set
            {
                clearColor = value;
                GL.ClearColor(clearColor.R, clearColor.G, clearColor.B, clearColor.A);
            }
        }

        public void Clear()
        {

            StartTargeting();
            OpenGLError.CheckCrashIfDebug();
            GL.ClearDepth(1);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
        }


        public void SetDrawBounds(Vector2Int pos, Vector2Int size)
        {
            OpenGLError.CheckCrashIfDebug();

            if (size.X < 0 || size.Y < 0)
                return;

            if (pos == CurrentOpenGLContext.DrawBoundsPos && size == CurrentOpenGLContext.DrawBoundsSize)
                return;

            CurrentOpenGLContext.DrawBoundsPos = pos;
            CurrentOpenGLContext.DrawBoundsSize = size;
            GL.Scissor(pos.X, Size.Y - pos.Y - size.Y, size.X, size.Y);
            OpenGLError.CheckCrashIfDebug();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private unsafe void PushMatrix(string name, Matrix4x4 matrix)
        {
            OpenGLError.CheckCrashIfDebug();

            if (currentShaderProgram == null)
                SetShader(ShaderProgram.Default);

            if (currentShaderProgram.VertexShader != null && modelMatrix != default)
            {
                GL.UniformMatrix4(currentShaderProgram.GetUniformLocation(name).Value, 1, false, matrix.values);
            }

            OpenGLError.CheckCrashIfDebug();
        }


        private bool textureEnabled;

        private ITexture texture;

        public ITexture Texture
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            get => texture;
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            set
            {
                if (value == null)
                {
                    if (textureEnabled)
                    {
                        GL.Disable(EnableCap.Texture2D);
                        textureEnabled = false;
                    }

                    return;
                }
                else
                {
                    if (CurrentOpenGLContext.LoadedTexture == value)
                        return;

                    // if (value != null && texture != null && texture == value) return;
                    OpenGLError.CheckCrashIfDebug();

                    int curId = GPULoadedData.TextureManager[value];
                    OpenGLError.CheckCrashIfDebug();
                    if (!textureEnabled)
                    {
                        //GL.Enable(EnableCap.Texture2D);
                        textureEnabled = true;
                    }

                    OpenGLError.CheckCrashIfDebug();
                    GL.BindTexture(TextureTarget.Texture2D, curId);
                    CurrentOpenGLContext.LoadedTexture = value;
                    OpenGLError.CheckCrashIfDebug();
                }

                texture = value;
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void RenderVertexBuffer(VertexBuffer vertexBuffer, Core.PrimitiveType primitiveType)
        {
            LoadedVertexBuffer data = BindVertexBuffer(vertexBuffer);
            GL.DrawArrays((PrimitiveType)(int)primitiveType, 0, data.verticesAmount);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void RenderVertexBufferInstanced(VertexBuffer vertexBuffer, Core.PrimitiveType primitiveType, int amount)
        {
#if DEBUG
            if (amount < 1)
                throw new Exception("Amount has to be bigger than 0");
#endif
            LoadedVertexBuffer data = BindVertexBuffer(vertexBuffer);
            GL.DrawArraysInstanced((PrimitiveType)(int)primitiveType, 0, data.verticesAmount, amount);
            OpenGLError.CheckCrashIfDebug();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private LoadedVertexBuffer BindVertexBuffer(VertexBuffer vertexBuffer)
        {
            var data = GPULoadedData.VertexBuffers.Get(vertexBuffer);

            if (CurrentOpenGLContext.LoadedVertexBuffer != vertexBuffer || CurrentOpenGLContext.LoadedVertexBufferUpdates != data.update)
            {
                GL.BindBuffer(BufferTarget.ArrayBuffer, data.index);
                GL.BindVertexArray(data.index);

                CurrentOpenGLContext.LoadedVertexBuffer = vertexBuffer;
                CurrentOpenGLContext.LoadedVertexBufferUpdates = data.update;
            }

            return data;
        }


        public void StartTargeting()
        {
            GL.GetInteger(GetPName.FramebufferBinding, out int index);
            if (index == FrameBufferIndex)
                return;

            CurrentOpenGLContext.Reset();
            if (CorrespondingRenderTexture != null)
                _ = GPULoadedData.RenderTextureManager[CorrespondingRenderTexture];

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, FrameBufferIndex);

            UpdateBlending();

            GL.Viewport(0, 0, Size.X, Size.Y);

            if (currentShaderProgram != null)
            {
                SetShader(currentShaderProgram);
                ProjectionMatrix = Matrix4x4.CreateOrthographic(Size.X, Size.Y, near, far);
            }
            CurrentOpenGLContext.LoadedFrameBuffer = FrameBufferIndex;
        }

        private void UpdateBlending()
        {
            switch (Blending)
            {
                case Blending.Default:
                    GL.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);
                    break;
                case Blending.DefaultWithAlphaAdd:
                    GL.BlendFuncSeparate(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha, BlendingFactorSrc.SrcAlpha, BlendingFactorDest.DstAlpha);
                    break;
                case Blending.Additive:
                    GL.BlendFunc(BlendingFactor.One, BlendingFactor.One);
                    break;
                default: throw new NotImplementedException();
            }
        }

        public Color GetPixel(Vector2Int pos)
        {
            var pixel = new Color();
            GL.ReadPixels(pos.X, Height - pos.Y, 1, 1, PixelFormat.Rgba, PixelType.Float, ref pixel);
            return pixel;
        }

        //Not tested could be changed tyo Color[]
        public Color[] GetPixels()
        {
            OpenGLError.CheckCrashIfDebug();

            var originalpixels = new RGBAPixel[Width * Height];
            GL.ReadPixels(0, 0, Width, Height, PixelFormat.Rgba, PixelType.UnsignedByte, originalpixels);
            var colors = new Color[originalpixels.Length];
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    int i = y * Width + x;
                    int i0 = (Height - y - 1) * Width + x;
                    colors[i] = Color.FromBytes(originalpixels[i0].R, originalpixels[i0].G, originalpixels[i0].B, originalpixels[i0].A);
                }
            }

            OpenGLError.CheckCrashIfDebug();
            return colors;
        }

        public void SaveToFile(string path)
        {
            OpenGLError.CheckCrashIfDebug();
            GL.GetInteger(GetPName.FramebufferBinding, out int currentframebuffer);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, FrameBufferIndex);
            StartTargeting();
            var bytes = ArrayPool<Bgra32>.Shared.Rent(Width * Height);
            GL.ReadPixels(0, 0, Width, Height, PixelFormat.Bgra, PixelType.UnsignedByte, bytes);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, currentframebuffer);
            OpenGLError.CheckCrashIfDebug();
            Task.Run(() =>
            {
                for (int i = 0; i < bytes.Length; i++)
                {
                    bytes[i].A = 255;
                }
                var image = Image.LoadPixelData<Bgra32>(bytes, Width, Height);
                image.Mutate(x => x.Flip(FlipMode.Vertical));
                image.SaveAsync(path).Wait();
                image.Dispose();
                ArrayPool<Bgra32>.Shared.Return(bytes);
            });
        }

        public void SetShader(ShaderProgram shaderProgram)
        {
            OpenGLError.CheckCrashIfDebug();

            Contract.Requires(shaderProgram != null);

            if (currentShaderProgram != null)
            {
                currentShaderProgram.RenderTarget = null;
            }

            currentShaderProgram = shaderProgram;
            currentShaderProgram.RenderTarget = this;

            shaderProgramID = GPULoadedData.ShaderManager.GetIndex(shaderProgram);

            if (GPULoadedData.ShaderManager.ActiveProgramIndex != shaderProgramID)
                UseShaderProgram();

            GPULoadedData.ShaderManager.ActiveShaderProgram = shaderProgram;
            GPULoadedData.ShaderManager.ActiveProgramIndex = shaderProgramID;

            CurrentOpenGLContext.LoadedShaderProgram = shaderProgram;
            OpenGLError.CheckCrashIfDebug();
        }

        private void UseShaderProgram()
        {
            OpenGLError.CheckCrashIfDebug();

            GL.UseProgram(shaderProgramID);

            if (currentShaderProgram != null)
            {
                if (!currentShaderProgram.Initialised)
                {
                    foreach (string constant in ShaderConstants.All)
                    {
                        currentShaderProgram.SetUniformLocation(constant, GL.GetUniformLocation(shaderProgramID, constant));
                    }

                    currentShaderProgram.Initialised = true;
                }

                currentShaderProgram.RenderTarget = this;
            }

            SetCurrentShaderUniform(ShaderConstants.ColorTint, Color.White);
            PushMatrix(ShaderConstants.ViewMatrix, viewMatrix);
            PushMatrix(ShaderConstants.ProjectionMatrix, projectionMatrix);
            PushMatrix(ShaderConstants.ModelMatrix, new Matrix4x4(modelMatrix));
        }

        private int GetUniformLocation(string name)
        {
            // return GL.GetUniformLocation(shaderProgramID, name);
            var location = currentShaderProgram.GetUniformLocation(name);
            if (!location.HasValue)
            {
                int loc = GL.GetUniformLocation(shaderProgramID, name);
                currentShaderProgram.SetUniformLocation(name, loc);
                return loc;
            }

            return location.Value;
        }

        public void SetCurrentShaderUniform(string name, uint value)
        {
            GL.Uniform1(GetUniformLocation(name), value);
        }

        public void SetCurrentShaderUniform(string name, int value)
        {
            GL.Uniform1(GetUniformLocation(name), value);
        }

        public void SetCurrentShaderUniform(string name, float value)
        {
            GL.Uniform1(GetUniformLocation(name), value);
        }

        public void SetCurrentShaderUniform(string name, double value)
        {
            GL.Uniform1(GetUniformLocation(name), value);
        }

        public void SetCurrentShaderUniform(string name, Vector2 value)
        {
            GL.Uniform2(GetUniformLocation(name), value.X, value.Y);
        }

        public void SetCurrentShaderUniform(string name, Vector3 value)
        {
            GL.Uniform3(GetUniformLocation(name), value.X, value.Y, value.Z);
        }

        public void SetCurrentShaderUniform(string name, Color value)
        {
            GL.Uniform4(GetUniformLocation(name), value.R, value.G, value.B, value.A);
        }

        public void SetCurrentShaderUniform<T>(string name, T[] data, int stride) where T : struct
        {
            if (!currentShaderProgram.HasUniformBuffer(name))
            {
                int index = GL.GenBuffer();
                currentShaderProgram.AddUniformBuffer(name, index);
            }

            int buffer = currentShaderProgram.GetUniformBuffer(name);
            if (CurrentOpenGLContext.LoadedUniformBuffer != buffer)
            {
                GL.BindBuffer(BufferTarget.UniformBuffer, buffer);
                GL.BufferData(BufferTarget.UniformBuffer, stride, data, BufferUsageHint.DynamicDraw);
                GL.BindBufferRange(BufferRangeTarget.UniformBuffer, 0, buffer, (IntPtr)0, stride);
            }
        }

        public void SetCurrentShaderUniform<T>(string name, T data, int stride) where T : struct
        {
            if (!currentShaderProgram.HasUniformBuffer(name))
            {
                int index = GL.GenBuffer();
                currentShaderProgram.AddUniformBuffer(name, index);
            }

            int buffer = currentShaderProgram.GetUniformBuffer(name);
            if (CurrentOpenGLContext.LoadedUniformBuffer != buffer)
            {
                GL.BindBuffer(BufferTarget.UniformBuffer, buffer);
                GL.BufferData(BufferTarget.UniformBuffer, stride, ref data, BufferUsageHint.DynamicDraw);
                GL.BindBufferRange(BufferRangeTarget.UniformBuffer, 0, buffer, (IntPtr)0, stride);
            }
            //   GL.BindBuffer(BufferTarget.UniformBuffer, 0);
        }


        public void SetPixelsAs(IRenderTarget target)
        {
#if DEBUG
            if (target.Size != Size)
                throw new Exception("The rendertargets are not the same size.");
#endif
            var size = target.Size;
            var x0 = 0;
            var x1 = size.X;
            var y0 = 0;
            var y1 = size.Y;

            var src = ((OpenTKRenderTarget)target).FrameBufferIndex;
            var dest = FrameBufferIndex;

            Logger.LogAnnouncement(GL.CheckNamedFramebufferStatus(src, FramebufferTarget.Framebuffer));
            Logger.LogAnnouncement(GL.CheckNamedFramebufferStatus(dest, FramebufferTarget.Framebuffer) + " " + dest.ToString());

            GL.BlitNamedFramebuffer(
                src,
                dest,
                x0, y0, x1, y1, x0, y0, x1, y1,
                ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit, BlitFramebufferFilter.Nearest);
        }

        public void SetCurrentShaderUniform(string name, Matrix4x4 value)
        {
            unsafe
            {
                GL.UniformMatrix4(GetUniformLocation(name), 1, false, value.values);
            }
        }

        public void SetCurrentShaderUniform(string name, uint[] value)
        {
            Contract.Requires(value != null);
            GL.Uniform1(GetUniformLocation(name), value.Length, value);
        }

        public void SetCurrentShaderUniform(string name, int[] value)
        {
            Contract.Requires(value != null);
            GL.Uniform1(GetUniformLocation(name), value.Length, value);
        }

        public void SetCurrentShaderUniform(string name, double[] value)
        {
            Contract.Requires(value != null);
            GL.Uniform1(GetUniformLocation(name), value.Length, value);
        }

        public void SetCurrentShaderUniform(string name, float[] value)
        {
            Contract.Requires(value != null);

            GL.Uniform1(GetUniformLocation(name), value.Length, value);
        }

        public void SetCurrentShaderUniform(string name, Vector3[] value)
        {
            Contract.Requires(value != null);
            GL.Uniform3(GetUniformLocation(name), value.Length, ref value[0].X);
        }


        public void SetCurrentShaderUniform(string name, ITexture value)
        {
            // Dit project is verloren <-- Laatste woorden van Mestiez voordat hij Nurose verliet. Ssssh ik ben er nog Nurose ik laat je nooit gaan me and you 4ever <3

            OpenGLError.CheckCrashIfDebug();

            int textureUnit = currentShaderProgram.GetTextureUnit(name);
            TextureUnit textureUnitEnum = (TextureUnit)((int)TextureUnit.Texture0 + textureUnit);
            int textureId = GPULoadedData.TextureManager[value];
            GL.ActiveTexture(textureUnitEnum);
            GL.Uniform1(GetUniformLocation(name), textureUnit);
            GL.BindTexture(TextureTarget.Texture2D, textureId);
            GL.ActiveTexture(TextureUnit.Texture0);

            OpenGLError.CheckCrashIfDebug();

        }

        public void SetShaderStorageBuffer<T>(int location, ShaderStorageBuffer<T> buffer) where T : struct
        {
            var id = GPULoadedData.ShaderBufferStorager.GetIdAndUpdateIfNeeded(buffer);
            GL.BindBufferBase(BufferRangeTarget.ShaderStorageBuffer, location, id);
        }


        public Vector2 WorldToScreen(Vector2 position)
        {
            //transform world to clipping coordinates
            var point3D = (viewMatrix * projectionMatrix).MultiplyPoint(position);
            float winX = ((point3D.X + 1) / 2.0f) * Width;
            //we calculate -point3D.getY() because the screen Y axis is
            //oriented top->down 
            float winY = ((1 - point3D.Y) / 2.0f) * Height;
            return new Vector2(winX, winY);
        }

        public Vector2 ScreenToWorld(Vector2 position)
        {
            position.X = 2.0f * position.X / Width - 1;
            position.Y = -2.0f * position.Y / Height + 1;

            Matrix4x4 invert = Matrix4x4.Invert(viewMatrix * projectionMatrix);
            var vec2 = invert.MultiplyPoint(position);

            return vec2;
        }

        public void Render(RenderQueue queue, bool autoClear)
        {
            if (queue.Groups.Count == 0)
                return;


            StartTargeting();
            //foreach (var group in queue.Groups.OrderBy(o => Utils.RandomFloat()))
            foreach (var group in queue.Groups.OrderByDescending(o => o.Depth))
            // foreach (var group in queue.Groups)
            {
                if (currentShaderProgram != null)
                    SetCurrentShaderUniform("depth", group.Depth);
                foreach (var task in group.Tasks)
                {
                    task.Execute(this);
                }
            }

            if (autoClear)
            {
                queue.Groups.Clear();
            }
            //GL.Flush();
        }

        public void ForceLoad(ITexture texture)
        {
            var _ = GPULoadedData.TextureManager[texture];
        }
        public void ForceLoad(RenderTexture texture)
        {
            var _ = GPULoadedData.RenderTextureManager[texture];
        }

        public void CopyDepthBufferFrom(IRenderTarget renderTarget)
        {
            var otherFrameBufferId = ((OpenTKRenderTarget)renderTarget).FrameBufferIndex;
            var frameBufferId = this.FrameBufferIndex;
            GL.BindFramebuffer(FramebufferTarget.ReadFramebuffer, otherFrameBufferId);
            GL.BindFramebuffer(FramebufferTarget.DrawFramebuffer, frameBufferId);
            GL.BlitFramebuffer(0, 0, Width, Height, 0, 0, Width, Height, ClearBufferMask.DepthBufferBit, BlitFramebufferFilter.Nearest);
        }
    }
}
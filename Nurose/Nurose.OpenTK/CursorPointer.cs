﻿using OpenTK.Windowing.GraphicsLibraryFramework;

namespace Nurose.OpenTK
{
    public unsafe class CursorPointer
    {
        public CursorPointer(Cursor* pointer)
        {
            Pointer = pointer;
        }
        public Cursor* Pointer;
    }
}
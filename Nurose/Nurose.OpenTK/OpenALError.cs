﻿using System.Runtime.CompilerServices;
using Nurose.Core;
using OpenTK.Audio.OpenAL;

namespace Nurose.OpenTK
{
    public static class OpenALError
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Check()
        {
#if (DEBUG)
            while (NuroseMain.Window == null || NuroseMain.Window.IsOpen)
            {
                var error = AL.GetError();
                if (error == ALError.NoError)
                {
                    break;
                }
                // Logger.LogError("OpenGL Error" + error.ToString());
            }
        
#endif
        }
    }
}
﻿using Nurose.Core;

namespace Nurose.OpenTK
{
    internal static class CurrentOpenGLContext
    {
        public static ITexture LoadedTexture;
        public static VertexBuffer LoadedVertexBuffer;
        public static int LoadedVertexBufferUpdates = -1;
        public static ShaderProgram LoadedShaderProgram;
        public static int LoadedUniformBuffer = -1;
        public static int LoadedFrameBuffer = -1;
        public static bool CurrentDrawBoundsEnabled;
        public static Vector2Int DrawBoundsPos;
        public static Vector2Int DrawBoundsSize;
            
        public static void Reset()
        {
            LoadedTexture = null;
            LoadedFrameBuffer = -1;
            //LoadedVertexBuffer = null;
            // LoadedShaderProgram = null;
        }
    }
}
﻿using Nurose.Core;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Nurose.OpenTK
{
    internal class ShaderBufferStorager
    {
        private readonly Dictionary<IShaderStorageBuffer, int> LoadedBuffers = new();


        public int GetId<T>(ShaderStorageBuffer<T> buffer) where T : struct
        {
            if (!LoadedBuffers.TryGetValue(buffer, out int key))
            {
                key = GL.GenBuffer();
                GL.BindBuffer(BufferTarget.ShaderStorageBuffer, key);
                GL.BufferData(BufferTarget.ShaderStorageBuffer, buffer.Data.Length * Marshal.SizeOf<T>(), buffer.Data, BufferUsageHint.DynamicDraw);
                LoadedBuffers.Add(buffer, key);
            }
            return key;
        }

        public int GetIdAndUpdateIfNeeded<T>(ShaderStorageBuffer<T> buffer) where T : struct
        {
            int Id = GetId(buffer);
            OpenGLError.CheckCrashIfDebug();
            if (buffer.NeedsUpdate)
            {
                GL.BindBuffer(BufferTarget.ShaderStorageBuffer, Id);
                OpenGLError.CheckCrashIfDebug();
                GL.BufferSubData(BufferTarget.ShaderStorageBuffer, 0, buffer.Data.Length * Marshal.SizeOf<T>(), buffer.Data);
                OpenGLError.CheckCrashIfDebug();
                buffer.NeedsUpdate = false;
            }
            OpenGLError.CheckCrashIfDebug();
            return Id;
        }
    }
}
﻿using System;
using System.Runtime.InteropServices;

namespace Nurose.OpenTK
{
    public static class NativeMethods
    {
        [DllImport("USER32.DLL")]
        internal static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("User32.dll")]
        internal static extern IntPtr GetDC(IntPtr hwnd);

        [DllImport("User32.dll")]
        internal static extern int ReleaseDC(IntPtr hwnd, IntPtr dc);

        [DllImport("gdi32.dll")]
        internal static extern int GetDeviceCaps(IntPtr hdc, int nIndex);


    }
}

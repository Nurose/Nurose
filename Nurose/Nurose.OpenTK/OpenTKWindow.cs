﻿using Nurose.Core;
using OpenTK.Graphics.OpenGL;
using OpenTK.Windowing.Common;
using OpenTK.Windowing.Desktop;
using OpenTK.Windowing.GraphicsLibraryFramework;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats;
using SixLabors.ImageSharp.PixelFormats;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using Color = Nurose.Core.Color;
using Cursor = Nurose.Core.Cursor;
using Image = SixLabors.ImageSharp.Image;
using Monitor = OpenTK.Windowing.GraphicsLibraryFramework.Monitor;
using OpenTKCursor = global::OpenTK.Windowing.GraphicsLibraryFramework.Cursor;

namespace Nurose.OpenTK
{
    public class OpenTKWindow : NativeWindow, IWindow
    {
        private bool resizable;
        private readonly Dictionary<Cursor, CursorPointer> Cursors = new();
        public InputHandler InputHandler { get; set; }
        public InputState Input { get; set; }
        public InputRefreshOption InputRefreshOption { get; set; }
        public IRenderTarget RenderTarget
        {
            get => OpenTKRenderTarget;
        }

        public OpenTKRenderTarget OpenTKRenderTarget { get; }

        public Color ClearColor { get; set; } = Color.Black;
        public bool ClearEnabled { get; set; } = true;

        public string GPUVendor { get; }
        public event EventHandler<string[]> OnFileDropped;


        public unsafe IMonitor Monitor => new OpenTkMonitor(base.WindowPtr);

        public bool IsOpen { get; private set; } = true;

        public int Width => base.Size.X;

        public int Height => base.ClientSize.Y;

        private Core.Cursor cursorShape;

        public Core.Cursor CursorShape
        {
            get => cursorShape;
            set
            {
                if (value != cursorShape)
                {
                    cursorShape = value;
                    unsafe
                    {
                        if (!IsOpen)
                            return;

                        if (!Cursors.ContainsKey(value))
                        {
                            OpenTKCursor* standardCursor = GLFW.CreateStandardCursor((CursorShape)((int)cursorShape));
                            Cursors.Add(value, new CursorPointer(standardCursor));
                        }
                        GLFW.SetCursor(GLFW.GetCurrentContext(), Cursors[value].Pointer);
                    }
                }
            }
        }
        public string Clipboard
        {
            get => ClipboardString;
            set => ClipboardString = value;
        }

        Core.WindowState IWindow.WindowState
        {
            get => (Core.WindowState)(int)base.WindowState;
            set
            {
                base.WindowState = (global::OpenTK.Windowing.Common.WindowState)(int)value;

/*                unsafe
                {
                    switch (value)
                    {
                        case Core.WindowState.Normal:


                            IntPtr primary = NativeMethods.GetDC(IntPtr.Zero);
                            int DESKTOPVERTRES = 117;
                            int DESKTOPHORZRES = 118;
                            int actualPixelsX = NativeMethods.GetDeviceCaps(primary, DESKTOPHORZRES);
                            int actualPixelsY = NativeMethods.GetDeviceCaps(primary, DESKTOPVERTRES);

                            var monitorSize = Monitor.MonitorSizeInPixels;
                            var size = Size;
                            var Location = new Vector2Int(actualPixelsX / 2 - size.X / 2, actualPixelsY / 2 - size.Y / 2);


                            GLFW.RestoreWindow(WindowPtr);
                            GLFW.SetWindowMonitor(WindowPtr, null, 0, 0, size.X, size.Y, GLFW.DontCare);
                            break;
                        case Core.WindowState.Minimized:
                            GLFW.IconifyWindow(WindowPtr);
                            break;
                        case Core.WindowState.Maximized:
                            GLFW.MaximizeWindow(WindowPtr);
                            break;
                        case Core.WindowState.Fullscreen:
                            {
                                Monitor* monitor = CurrentMonitor.ToUnsafePtr<Monitor>();
                                VideoMode* videoMode = GLFW.GetVideoMode(monitor);
                                GLFW.SetWindowMonitor(WindowPtr, monitor, 0, 0, videoMode->Width, videoMode->Height, videoMode->RefreshRate);
                                break;
                            }
                    }
                }*/
                UpdateSwapInterval();
            }
        }

        Core.WindowBorder IWindow.WindowBorder
        {
            get => (Core.WindowBorder)(int)base.WindowBorder;
            set => base.WindowBorder = (global::OpenTK.Windowing.Common.WindowBorder)(int)value;
        }

        public OpenTKWindow(NativeWindowSettings settings) : base(settings)
        {
            //var mode = new GraphicsMode(8, 8, 0, 8);
            //glContext = new GraphicsContext(mode, WindowInfo);
            //glContext.MakeCurrent(WindowInfo);
            //(glContext as IGraphicsContextInternal).LoadAll();
            OpenGLError.CheckCrashIfDebug();
            GL.Enable(EnableCap.Multisample);
            GL.Enable(EnableCap.DepthTest);
            GL.DepthFunc(DepthFunction.Lequal);

            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);
            unsafe
            {
                GLFW.SetInputMode(base.WindowPtr, CursorStateAttribute.Cursor, CursorModeValue.CursorNormal);
            }
            // GL.DepthMask(true);
            var openTKRenderTarget = new OpenTKRenderTarget((settings.Size.X, settings.Size.Y), 0);
            //InputHandler = new OpenTKInput(this, () => Input);
            OpenTKRenderTarget = openTKRenderTarget;
            Input = new();
            Title = settings.Title;
            //GL.AlphaFunc(AlphaFunction.Greater, 0.0f);
            // GL.Enable(EnableCap.AlphaTest);
            OpenGLError.CheckCrashIfDebug();
            // GL.Enable(EnableCap.VertexArray);
            // GL.Enable(EnableCap.TextureCoordArray);
            // GL.Enable(EnableCap.ColorArray);
            Render();

            if (File.Exists("resources/icon.png"))
                LoadIcon();

            GPUVendor = GL.GetString(StringName.Vendor) + ", " + GL.GetString(StringName.Renderer) + ", " + GL.GetString(StringName.ShadingLanguageVersion);
        }

        private void LoadIcon()
        {
            Configuration.Default.PreferContiguousImageBuffers = true;
            using var image = Image.Load("resources/icon.png").CloneAs<Rgba32>();
            image.DangerousTryGetSinglePixelMemory(out Memory<Rgba32> span);
            var openTKImage = new global::OpenTK.Windowing.Common.Input.Image(image.Width, image.Height, MemoryMarshal.AsBytes(span.Span).ToArray());
            Icon = new global::OpenTK.Windowing.Common.Input.WindowIcon(openTKImage);
        }

        /*
        public override void ProcessEvents()
        {
            unsafe
            {

                if (GLFW.WindowShouldClose(WindowPtr))
                {
                    IsOpen = false;
                }

            }
            base.ProcessEvents();
        }
        */

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
        }

        public Vector2Int Position
        {
            get { return new Vector2Int(base.Location.X, base.Location.Y); }
            set { base.Location = (value.X, value.Y); }
        }

        public new Vector2Int Size
        {
            get { return new Vector2Int(base.ClientSize.X, base.ClientSize.Y); }
            set
            {
                base.ClientSize = ((int)value.X, (int)value.Y);
                RecalculateView();
            }
        }

        public uint TargetFps { get; set; }

        public bool Resizable
        {
            get => resizable;
            set
            {
                resizable = value;

                // WindowBorder = resizable ? WindowBorder.Resizable : WindowBorder.Fixed;
                // Size = new Vector2Int(base.Size.Width, base.Size.Height);
            }
        }

        private bool vsync;

        public new bool VSync
        {
            get => vsync;
            set
            {
                vsync = value;
                UpdateSwapInterval();
            }
        }


        private void UpdateSwapInterval()
        {
            if (vsync)
                GLFW.SwapInterval(1);
            else
                GLFW.SwapInterval(0);
        }


        //public bool VSync { get; set; }


        protected override void OnKeyDown(KeyboardKeyEventArgs e)
        {
            base.OnKeyDown(e);
        }

        public void Start()
        {
            //IsVisible = true;
            RenderTarget.ProjectionMatrix = Matrix4x4.CreateOrthographic(Size.X, Size.Y, ((OpenTKRenderTarget)RenderTarget).near, ((OpenTKRenderTarget)RenderTarget).far);
        }

        public void Render()
        {
            unsafe
            {
                if (GLFW.WindowShouldClose(WindowPtr))
                {
                    IsOpen = false;
                }

                GLFW.SwapBuffers(base.WindowPtr);
            }
        }

        public new void CenterWindow()
        {
            unsafe
            {
                IntPtr primary = NativeMethods.GetDC(IntPtr.Zero);
                int DESKTOPVERTRES = 117;
                int DESKTOPHORZRES = 118;
                int actualPixelsX = NativeMethods.GetDeviceCaps(primary, DESKTOPHORZRES);
                int actualPixelsY = NativeMethods.GetDeviceCaps(primary, DESKTOPVERTRES);

                Location = (actualPixelsX / 2 - base.Size.X / 2, actualPixelsY / 2 - base.Size.Y / 2);
            }
        }


        public new Vector2Int MousePosition()
        {
            return Input.MousePosition;
        }

        protected override void OnFileDrop(FileDropEventArgs e)
        {
            OnFileDropped?.Invoke(this, e.FileNames);
            base.OnFileDrop(e);
        }
        protected override void OnResize(ResizeEventArgs e)
        {
            if (RenderTarget != null)
            {
                base.OnResize(e);
                RecalculateView();
            }
        }

        private void RecalculateView()
        {
            RenderTarget.Size = new Vector2Int(base.ClientSize.X, base.ClientSize.Y);
            RenderTarget.ProjectionMatrix = Matrix4x4.CreateOrthographic(Size.X, Size.Y, ((OpenTKRenderTarget)RenderTarget).near, ((OpenTKRenderTarget)RenderTarget).far);
            GL.Viewport(0, 0, base.ClientSize.X, base.ClientSize.Y);
        }

        public new void Close()
        {
            IsOpen = false;
            base.Close();
        }

        public void ProcessEvents()
        {
            base.ProcessEvents(.001f);
        }
    }
}
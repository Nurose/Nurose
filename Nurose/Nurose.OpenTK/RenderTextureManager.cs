﻿using Nurose.Core;
using System;
using OpenTK.Graphics.OpenGL;
using System.Collections.Generic;

namespace Nurose.OpenTK
{
    internal class RenderTextureManager
    {
        private Dictionary<RenderTexture, int> FrameBuffersIds { get; } = new Dictionary<RenderTexture, int>();

        public int this[RenderTexture texture]
        {
            get
            {
                if (!FrameBuffersIds.ContainsKey(texture) || texture.ColorTexture.Initilized == false)
                    LoadRenderTexture(texture);

                return FrameBuffersIds[texture];
            }
        }

        private void LoadRenderTexture(RenderTexture rt)
        {
            OpenGLError.CheckCrashIfDebug();
            GL.GetInteger(GetPName.FramebufferBinding, out int previouslyBoundBuffer);

            bool updateOnly = FrameBuffersIds.ContainsKey(rt);
            var renderTargetSize = rt.Size;
            if (renderTargetSize == default)
                renderTargetSize = Vector2Int.One;

            int framebufferIndex;
            if (updateOnly)
            {
                framebufferIndex = FrameBuffersIds[rt];
            }
            else
            {
                framebufferIndex = GL.GenFramebuffer();
            }
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, framebufferIndex);


            ((Texture)rt.ColorTexture).Size = renderTargetSize;
            ((Texture)rt.ColorTexture).Initilized = false;
            var colorTextureIndex = GPULoadedData.TextureManager[rt.ColorTexture];

            GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, colorTextureIndex, 0);
            OpenGLError.CheckCrashIfDebug();

            if (rt.DepthTexture != null)
            {
                ((Texture)rt.DepthTexture).Size = renderTargetSize;
                ((Texture)rt.DepthTexture).Initilized = false;
                var depthTextureIndex = GPULoadedData.TextureManager[rt.DepthTexture];
                if (rt.hasStencilBuffer)
                    GL.FramebufferTexture2D(FramebufferTarget.DrawFramebuffer, FramebufferAttachment.DepthStencilAttachment, TextureTarget.Texture2D, depthTextureIndex, 0);
                else
                    GL.FramebufferTexture2D(FramebufferTarget.DrawFramebuffer, FramebufferAttachment.DepthAttachment, TextureTarget.Texture2D, depthTextureIndex, 0);
            }



            if (updateOnly)
            {
                rt.RenderTarget.Size = renderTargetSize;
            }
            else
            {
                FrameBuffersIds.Add(rt, framebufferIndex);
                rt.RenderTarget = new OpenTKRenderTarget(renderTargetSize, framebufferIndex) { CorrespondingRenderTexture = rt };
            }

            FramebufferErrorCode status = GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer);
            Logger.LogAnnouncement(status);
            if (status != FramebufferErrorCode.FramebufferComplete &&
                status != FramebufferErrorCode.FramebufferCompleteExt)
            {
                throw new Exception($"Error creating framebuffer:{status}");
            }

            //GL.BindFramebuffer(FramebufferTarget.Framebuffer, previouslyBoundBuffer);
            OpenGLError.CheckCrashIfDebug();
        }
    }
}

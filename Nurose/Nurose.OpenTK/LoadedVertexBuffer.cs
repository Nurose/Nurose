﻿namespace Nurose.OpenTK
{
    internal class LoadedVertexBuffer
    {
        public int index;
        public int verticesAmount;
        public int update;

        public LoadedVertexBuffer(int index, int size, int update)
        {
            this.index = index;
            verticesAmount = size;
            this.update = update;
        }
    }
}


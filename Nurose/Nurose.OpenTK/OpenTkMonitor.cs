using Nurose.Core;
using OpenTK.Windowing.GraphicsLibraryFramework;
using System.Linq;

namespace Nurose.OpenTK
{
    public unsafe class OpenTkMonitor : IMonitor
    {
        private Window* WindowPtr;
        private float? cachedContentScale;
        private Vector2Int? cachedRealMonitorSize;

        public OpenTkMonitor()
        {
        }

        public OpenTkMonitor(Window* windowPtr)
        {
            WindowPtr = windowPtr;
        }

        public Vector2Int PhysicalSizeInMilimeters
        {
            get
            {
                GLFW.GetMonitorPhysicalSize(GLFW.GetWindowMonitor(WindowPtr), out int width, out int height);
                return new Vector2Int(width, height);
            }
        }
        public float ContentScale
        {
            get
            {
                if (cachedContentScale == null)
                {
                    GLFW.Init();
                    GLFW.GetMonitorContentScale(GLFW.GetPrimaryMonitor(), out float x, out float y);
                    if (x != y)
                        Logger.LogWarning("Non uniform scaling not supported");

                    cachedContentScale = x;
                }
                return cachedContentScale.Value;
            }
        }



        public Vector2Int MonitorSizeInPixels
        {
            get
            {
                if (cachedRealMonitorSize == null)
                {
                    //try to take the window monitor (if fullscreen only). otherwise take the primary monitor.
                    var monitor = GLFW.GetWindowMonitor(WindowPtr);
                    if (monitor == default)
                        monitor = GLFW.GetPrimaryMonitor();

                    var vms = GLFW.GetVideoModes(monitor);
                    var largest = vms.MaxBy(v => v.Width * v.Height);
                    cachedRealMonitorSize = new Vector2Int(largest.Width, largest.Height);
                }
                return cachedRealMonitorSize.Value;

            }
        }
    }
}
﻿using Nurose.Core;

namespace Nurose.OpenTK
{
    public class OpenTKAudioFactory : IAudioFactory
    {
        public IAudioPlayer CreateAudioPlayer()
        {
            return new OpenTKAudioPlayer();
        }
    }
}
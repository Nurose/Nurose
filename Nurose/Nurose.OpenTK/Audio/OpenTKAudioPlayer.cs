﻿using Nurose.Core;
using OpenTK.Audio.OpenAL;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace Nurose.OpenTK
{
    public class OpenTKAudioPlayer : IAudioPlayer
    {
        const int MaxSources = 32;
        public Dictionary<AudioSource, int> AudioSourceIndex = new();
        public static Dictionary<AudioFile, int> AudioBuffersIds = new();
        public AudioSource[] Sources = new AudioSource[MaxSources];
        public int[] SourceIds = new int[MaxSources];
        public static int GlobalPlayId;

        public OpenTKAudioPlayer()
        {
            SetupALContext();
            SetupSources();
        }

        private void SetupSources()
        {
            SourceIds = AL.GenSources(Sources.Length);
            for (int i = 0; i < Sources.Length; i++)
            {
                Sources[i] = new AudioSource();
                Sources[i].Reset();
                AudioSourceIndex.Add(Sources[i], i);
            }
        }

        private static void SetupALContext()
        {
            string deviceName = ALC.GetString(ALDevice.Null, AlcGetString.DefaultDeviceSpecifier);
            var devices = ALC.GetStringList(GetEnumerationStringList.DeviceSpecifier);
            foreach (var d in devices)
            {
                if (d.Contains("OpenAL Soft"))
                {
                    deviceName = d;
                }
            }

            var device = ALC.OpenDevice(deviceName);
            var context = ALC.CreateContext(device, (int[])null);
            ALC.MakeContextCurrent(context);

            OpenGLError.CheckCrashIfDebug();

            ALC.GetInteger(device, AlcGetInteger.MajorVersion, 1, out int alcMajorVersion);
            ALC.GetInteger(device, AlcGetInteger.MinorVersion, 1, out int alcMinorVersion);
            string alcExts = ALC.GetString(device, AlcGetString.Extensions);

            var attrs = ALC.GetContextAttributes(device);
            Logger.LogDebug($"OpenAL Attributes: {attrs}");
            Logger.LogDebug($"OpenAL Extentions: {alcExts}");
            Logger.LogDebug($"OpenAL Version: major={alcMajorVersion} minor:{alcMinorVersion}");

            AL.DistanceModel(ALDistanceModel.LinearDistance);
            AL.Listener(ALListenerf.Gain, 1f);
            OpenGLError.CheckCrashIfDebug();
        }

        public void ForceLoad(AudioFile file)
        {
            LoadAudioFileIfloaded(file);
        }

        public AudioSource Play(AudioPlayArgs args)
        {

            //if something free
            //pick first non-active
            //else stop random non-long living (music).

            bool IsAnyBufferAvailable = false;
            int firstAvailable = -1;
            for (int i = 0; i < Sources.Length; i++)
            {
                if (!Sources[i].Alive)
                {
                    IsAnyBufferAvailable = true;
                    firstAvailable = i;
                    break;
                }
            }

            var audioFile = args.AudioFile;
            LoadAudioFileIfloaded(audioFile);

            var indexToClaim = -1;
            if (IsAnyBufferAvailable)
                indexToClaim = firstAvailable;
            else
            {
                float curMax = float.MinValue;
                int indexMax = -1;
                for (int i = 0; i < Sources.Length; i++)
                {
                    var s = Sources[i];

                    var t = s.TimePlaying / s.AudioFile.TimeInSeconds - s.Priority*100;
                    if (t > curMax)
                    {
                        curMax = t;
                        indexMax = i;
                    }

                }
                indexToClaim = indexMax;

            }
            var sid = SourceIds[indexToClaim];

            var audioSource = Sources[indexToClaim];
            audioSource.Volume = args.Volume;
            AL.SourceRewind(sid);
            AL.Source(sid, ALSourcei.Buffer, AudioBuffersIds[audioFile]);
            AL.Source(sid, ALSourceb.Looping, args.PlayLooped);
            UpdateALSource(audioSource);
            AL.SourcePlay(sid);
            audioSource.Alive = true;
            audioSource.Manual = args.Manual;
            audioSource.TimePlaying = 0;
            audioSource.TimeInactive = 0;
            audioSource.AudioFile = audioFile;
            audioSource.PlayId = ++GlobalPlayId;
            audioSource.Priority = args.AudioPriority;
            audioSource.Pitch = args.Pitch;
            audioSource.IsLooping = args.PlayLooped;
            if(audioSource == null)
            {
                throw new Exception("audiosource = null after play");
            }
            return audioSource;
        }

        private static void LoadAudioFileIfloaded(AudioFile audioFile)
        {
            if (!AudioBuffersIds.ContainsKey(audioFile))
                AudioBuffersIds.Add(audioFile, AudioFileLoader.Load(audioFile));

            //Reinitilize for hot reloading.
            if (!audioFile.Initilized)
                AudioBuffersIds[audioFile] = AudioFileLoader.Load(audioFile);
        }

        public void SetListenerPos(Vector2 pos)
        {
            AL.Listener(ALListener3f.Position, 0, 0, 0);
        }

        public void StopSource(AudioSource audioSource)
        {
            audioSource.Reset();
            AL.SourceStop(SourceIds[AudioSourceIndex[audioSource]]);

        }

        public void Update(float dt)
        {
            UpdateSources(dt);
        }

        private void UpdateSources(float dt)
        {
            for (int i = 0; i < Sources.Length; i++)
            {
                AudioSource source = Sources[i];
                if (source.Alive)
                {
                    UpdateALSource(source);
                }

                if (source.Alive && source.TimePlaying > source.AudioFile.TimeInSeconds && !source.Manual)
                {
                    StopSource(source);
                }

                if (source.Alive)
                    source.TimePlaying += dt;
                else
                    source.TimeInactive += dt;
            }
        }

        public void UpdateALSource(AudioSource source)
        {
            var sid = SourceIds[AudioSourceIndex[source]];
            AL.Source(sid, ALSourcef.Gain, Utils.Max(0, source.Volume));
            AL.Source(sid, ALSourcef.Pitch, source.Pitch);
            AL.Source(sid, ALSource3f.Position, source.Position.X, source.Position.Y, 0);
        }
    }
}
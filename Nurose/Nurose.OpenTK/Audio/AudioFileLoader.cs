﻿using System;
using System.IO;
using System.Text;
using Nurose.Core;
using Nurose.OpenTK;
using OpenTK.Audio.OpenAL;

namespace Nurose.OpenTK
{
    /// <summary>
    /// The encoding format of a wave file
    /// </summary>
    public enum WaveFileFormat
    {
        Unknown = 0, PCM = 1, ADPCM = 2, IEEEFloat = 3, MPEG = 5, ALaw = 6, MuLaw = 7, Extensible = 0xFFFE
    }

    /// <summary>
    /// A full representation of a wave file
    /// </summary>
    /// <remarks>
    /// <para>Basic implementation; allows you to load a WAV file from disk or byte array and look at it.</para>
    /// <para>Really just enough here to support loading files for Unity, though it could easily be extended.</para>
    /// </remarks>
    public class WaveFile
    {
        /// <summary>
        /// The format of this wave file
        /// </summary>
        public WaveFileFormat Format { get; private set; }

        /// <summary>
        /// The sub-format of this wave file (if it is an extensible wave file)
        /// </summary>
        public WaveFileFormat SubFormat { get; private set; }

        /// <summary>
        /// The number of channels in this wave file
        /// </summary>
        public int Channels { get; private set; }

        /// <summary>
        /// The sample rate of this wave file
        /// </summary>
        public int SampleRate { get; private set; }

        /// <summary>
        /// The number of bits per sample of this wave file
        /// </summary>
        public int BitsPerSample { get; private set; }

        /// <summary>
        /// Whether this wave file has signed or unsigned data
        /// </summary>
        public bool Signed { get; private set; }

        /// <summary>
        /// The raw wave data of this wave file
        /// </summary>
        /// <remarks>
        /// Caution: this references the actual backing field
        /// </remarks>
        public ArraySegment<byte> Data { get; private set; }

        public int Samples { get => Data.Count / (BitsPerSample / 8 * Channels); }

        private WaveFile()
        {

        }

        /// <summary>
        /// Loads a WaveFile from disk
        /// </summary>
        public static WaveFile Load(string path)
        {
            return Load(File.ReadAllBytes(path), false);
        }

        /// <summary>
        /// Loads a WaveFile from a byte array
        /// </summary>
        public static WaveFile Load(byte[] data)
        {
            return Load(data, true);
        }

        /// <summary>
        /// Loads a WaveFile from a byte array
        /// </summary>
        public static WaveFile Load(byte[] data, bool copyData)
        {
            WaveFile waveFile = new();

            //handle the first WAVE/RIFF chunk
            string fileChunkID = Encoding.ASCII.GetString(data, 0, 4);
            int fileSize = (int)BitConverter.ToUInt32(data, 4) + 8; //this size does not include the first chunk ID or size bytes
            string fileFormatID = Encoding.ASCII.GetString(data, 8, 4);

            if (!(fileChunkID.Equals("RIFF", StringComparison.Ordinal) && fileFormatID.Equals("WAVE", StringComparison.Ordinal)))
                throw new FormatException();

            //start reading other chunks
            for (int i = 12; i < fileSize;)
            {
                //read the chunk header
                string currentChunkID = Encoding.ASCII.GetString(data, i, 4);
                int currentChunkLength = (int)BitConverter.ToUInt32(data, i + 4);

                //parse chunks we know, ignore ones we don't
                if (currentChunkID.Equals("fmt ", StringComparison.Ordinal))
                {
                    //fmt chunk describes the format of our wave file
                    waveFile.Format = (WaveFileFormat)BitConverter.ToUInt16(data, i + 8);
                    waveFile.Channels = (int)BitConverter.ToUInt16(data, i + 10);
                    waveFile.SampleRate = (int)BitConverter.ToUInt32(data, i + 12);
                    waveFile.BitsPerSample = (int)BitConverter.ToUInt16(data, i + 22);

                    //handle "extensible" format codes
                    if (waveFile.Format == WaveFileFormat.Extensible && currentChunkLength > 16)
                    {
                        int extensionChunkSize = (int)BitConverter.ToUInt16(data, i + 24);
                        waveFile.BitsPerSample = (int)BitConverter.ToUInt16(data, i + 26);
                        //we are lazy and only read the first two bytes of the extensible format GUID
                        waveFile.SubFormat = (WaveFileFormat)BitConverter.ToUInt16(data, i + 32);
                    }
                }
                else if (currentChunkID.Equals("data", StringComparison.Ordinal))
                {
                    //data chunk holds the actual wave data

                    if (copyData) //copying is slower and wastes memory but is safer
                    {
                        byte[] newData = new byte[currentChunkLength];
                        Array.Copy(data, i + 8, newData, 0, currentChunkLength);
                        waveFile.Data = new ArraySegment<byte>(newData);
                    }
                    else
                    {
                        waveFile.Data = new ArraySegment<byte>(data, i + 8, currentChunkLength);
                    }

                }
                else
                {
                    //ignore chunks we don't understand
                    //Console.WriteLine("found unidentified chunk " + currentChunkID);
                }

                i += (currentChunkLength + 8); //don't forget the chunk ID and size bytes!
            }

            //assuming standards compliance: PCM 16/24/32 bit is signed, 8-bit is not, float is always signed as well
            //note that it *is* possible to create WAV files that break from the standard
            waveFile.Signed = waveFile.BitsPerSample > 8;

            return waveFile;
        }

        /// <summary>
        /// Gets the audio data as an array of floats.
        /// </summary>
        /// <remarks>
        /// Really meant for Unity's AudioClip
        /// </remarks>
        public float[] GetDataFloat()
        {
            if (Format == WaveFileFormat.IEEEFloat || (Format == WaveFileFormat.Extensible && SubFormat == WaveFileFormat.IEEEFloat))
                return GetDataFloatFromFloat();
            else if (Format == WaveFileFormat.PCM || (Format == WaveFileFormat.Extensible && SubFormat == WaveFileFormat.PCM))
                return GetDataFloatFromPCM();
            else
                throw new NotSupportedException();
        }

        /// <summary>
        /// Gets a float array of audio data, assuming the source data is float data
        /// </summary>
        private float[] GetDataFloatFromFloat()
        {
            if (BitsPerSample != 32)
                throw new NotSupportedException(); //we don't support DP (yet)

            int stride = 4; //assume single-precision float

            float[] floatData = new float[Data.Count / stride];
            for (int i = 0; i < floatData.Length; i++)
            {
                floatData[i] = BitConverter.ToSingle(Data.Array, i * stride + Data.Offset);
            }

            return floatData;
        }

        /// <summary>
        /// Gets a float array of audio data, assuming the source data is PCM data
        /// </summary>
        private float[] GetDataFloatFromPCM()
        {
            int stride = (BitsPerSample / 8);

            //this is hopefully faster than doing an if/then every iteration
            Func<byte[], int, float> ConvertSample;
            switch (BitsPerSample)
            {
                case 8:
                    if (Signed)
                        throw new NotSupportedException();
                    ConvertSample = (data, index) => (data[index] - 128) / 127f; //offset binary
                    break;
                case 16:
                    if (!Signed)
                        throw new NotSupportedException();
                    ConvertSample = (data, index) => BitConverter.ToInt16(data, index) / (float)short.MaxValue;
                    break;
                case 24:
                    if (!Signed)
                        throw new NotSupportedException();
                    ConvertSample = (data, index) =>
                    {
                        int sample = data[index] | data[index + 1] << 8 | (sbyte)data[index + 2] << 16;
                        return sample / (float)8388607;
                    };
                    break;
                case 32:
                    if (!Signed)
                        throw new NotSupportedException();
                    ConvertSample = (data, index) => BitConverter.ToInt32(data, index) / (float)int.MaxValue;
                    break;
                default:
                    throw new NotSupportedException();
            }

            float[] floatData = new float[Data.Count / stride];
            for (int i = 0; i < floatData.Length; i++)
            {
                floatData[i] = ConvertSample(Data.Array, i * stride + Data.Offset);
            }

            return floatData;
        }


    }

    public static class AudioFileLoader
    {
        //source= https://github.com/mono/opentk/blob/main/Source/Examples/OpenAL/1.1/Playback.cs
        public static byte[] LoadWave(Stream stream, out int channels, out int bits, out int rate)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");

            using (BinaryReader reader = new(stream))
            {
                // RIFF header
                string signature = new(reader.ReadChars(4));
                if (signature != "RIFF")
                    throw new NotSupportedException("Specified stream is not a wave file.");

                int riff_chunck_size = reader.ReadInt32();

                string format = new(reader.ReadChars(4));
                if (format != "WAVE")
                    throw new NotSupportedException("Specified stream is not a wave file.");

                // WAVE header
                string format_signature = new(reader.ReadChars(4));
                if (format_signature != "fmt ")
                    throw new NotSupportedException("Specified wave file is not supported.");

                int format_chunk_size = reader.ReadInt32();
                int audio_format = reader.ReadInt16();
                int num_channels = reader.ReadInt16();
                int sample_rate = reader.ReadInt32();
                int byte_rate = reader.ReadInt32();
                int block_align = reader.ReadInt16();
                int bits_per_sample = reader.ReadInt16();

                string data_signature = new(reader.ReadChars(4));
                if (data_signature != "data")
                    throw new NotSupportedException("Specified wave file is not supported.");

                int data_chunk_size = reader.ReadInt32();

                channels = num_channels;
                bits = bits_per_sample;
                rate = sample_rate;
                return reader.ReadBytes((int)reader.BaseStream.Length);
            }
        }

        public static ALFormat GetSoundFormat(int channels, int bits)
        {
            switch (channels)
            {
                case 1: return bits == 8 ? ALFormat.Mono8 : ALFormat.Mono16;
                case 2: return bits == 8 ? ALFormat.Stereo8 : ALFormat.Stereo16;
                default: throw new NotSupportedException("The specified sound format is not supported.");
            }
        }

        public static int Load(AudioFile audioFile)
        {
            var fileStream = new FileStream(audioFile.FilePath, FileMode.Open);
            var bytes = LoadWave(fileStream, out int channels, out int bits, out int rate);
            audioFile.TimeInSeconds = ((float)bytes.Length / rate) / 2 / channels;
            // Playback the recorded data
            // short[] sine = new short[44100 * 1];
            // FillSine(sine, 4400, 44100);
            // FillSine(recording, 440, 44100);
            var soundFormat = GetSoundFormat(channels, bits);
            var b2 = WaveFile.Load(audioFile.FilePath);
            
            int buffer = AL.GenBuffer();
            AL.BufferData<byte>(buffer, soundFormat, b2.Data, rate);
            audioFile.Initilized = true;
            //   AL.BufferData<byte>(buffer, GetSoundFormat(channels, bits), bytes, rate);
            return buffer;
        }
    }
}
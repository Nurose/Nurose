﻿using System.Collections.Generic;
using System.Linq;
using Nurose.Core;
using Nurose.ECS;
using OpenTK.Audio.OpenAL;

namespace Nurose.OpenTK
{
    public class OldOpenTKAudioPlayer : IAudioPlayer
    {
        public List<AudioSource> PlayingAudioSources = new();
        public List<AudioSource> AvailableAudioSources = new();
        public static Dictionary<AudioFile, int> AudioBuffersIds = new();
        public static Dictionary<AudioSource, int> AudioSourceIds = new();
        public static int MaxSources = 64;
        public OldOpenTKAudioPlayer()
        {
            SetupALContext();
        }
        private static void SetupALContext()
        {
            string deviceName = ALC.GetString(ALDevice.Null, AlcGetString.DefaultDeviceSpecifier);
            var devices = ALC.GetStringList(GetEnumerationStringList.DeviceSpecifier);
            foreach (var d in devices)
            {
                if (d.Contains("OpenAL Soft"))
                {
                    deviceName = d;
                }
            }

            var device = ALC.OpenDevice(deviceName);
            var context = ALC.CreateContext(device, (int[])null);
            ALC.MakeContextCurrent(context);

            OpenGLError.CheckCrashIfDebug();

            ALC.GetInteger(device, AlcGetInteger.MajorVersion, 1, out int alcMajorVersion);
            ALC.GetInteger(device, AlcGetInteger.MinorVersion, 1, out int alcMinorVersion);
            string alcExts = ALC.GetString(device, AlcGetString.Extensions);

            var attrs = ALC.GetContextAttributes(device);
            Logger.LogDebug($"OpenAL Attributes: {attrs}");
            Logger.LogDebug($"OpenAL Extentions: {alcExts}");
            Logger.LogDebug($"OpenAL Version: major={alcMajorVersion} minor:{alcMinorVersion}");

            AL.DistanceModel(ALDistanceModel.LinearDistance);
            AL.Listener(ALListenerf.Gain, 1f);
            OpenGLError.CheckCrashIfDebug();
        }

        public void Update(float dt)
        {
            foreach (var pair in AudioSourceIds)
            {
                if (!pair.Key.IsLooping)
                    pair.Key.TimeInactive += dt;

            }

            foreach (var pair in AudioSourceIds.Where(t => t.Key.TimeInactive > 5))
            {
                if (!PlayingAudioSources.Contains(pair.Key))
                {
                    AvailableAudioSources.Remove(pair.Key);
                    AudioSourceIds.Remove(pair.Key);
                }
            }

            OpenGLError.CheckCrashIfDebug();
            for (int i = PlayingAudioSources.Count - 1; i >= 0; i--)
            {
                AudioSource audioSource = PlayingAudioSources[i];
                audioSource.TimePlaying += dt;
                audioSource.TimeInactive = 0;
                if ((!audioSource.IsLooping && audioSource.TimePlaying >= audioSource.AudioFile.TimeInSeconds))
                {
                    StopSource(audioSource);
                }
                else
                {
                    AL.Source(AudioSourceIds[audioSource], ALSourcef.Gain, Utils.Max(0, audioSource.Volume));
                    AL.Source(AudioSourceIds[audioSource], ALSourcef.Pitch, audioSource.Pitch);
                    AL.Source(AudioSourceIds[audioSource], ALSource3f.Position, audioSource.Position.X, audioSource.Position.Y, 0);
                }
                OpenGLError.CheckCrashIfDebug();
            }

        }
        public void SetListenerPos(Vector2 pos)
        {
            AL.Listener(ALListener3f.Position, pos.X, pos.Y, 0);
        }

        public AudioSource Play(AudioPlayArgs args)
        {
            var audioFile = args.AudioFile;
            if (!AudioBuffersIds.ContainsKey(audioFile))
                AudioBuffersIds.Add(audioFile, AudioFileLoader.Load(audioFile));

            //Reinitilize for hot reloading.
            if (!audioFile.Initilized)
                AudioBuffersIds[audioFile] = AudioFileLoader.Load(audioFile);

            var audioSource = AvailableAudioSources.FirstOrDefault(b => b.AudioFile == audioFile);
            if (audioSource == null)
            {
                audioSource = new AudioSource
                {
                    AudioFile = audioFile,
                    IsLooping = args.PlayLooped,
                    Volume = args.Volume,
                };


                if (AudioSourceIds.Count < MaxSources)
                {
                    AudioSourceIds.Add(audioSource, AL.GenSource());
                }
                else
                {
                    bool done = false;
                    foreach (var pair in AudioSourceIds.Reverse())
                    {
                        if (!pair.Key.IsLooping)
                        {
                            done = true;
                            var sourceToRemove = pair.Key;
                            var idToSteal = pair.Value;
                            StopSource(sourceToRemove);
                            AvailableAudioSources.Remove(sourceToRemove);
                            if (!AudioSourceIds.Remove(sourceToRemove))
                                throw new System.Exception();
                            AudioSourceIds.Add(audioSource, idToSteal);
                            Logger.LogDebug(audioSource.AudioFile.FilePath);
                            break;
                        }
                    }
                    if (!done)
                        throw new System.Exception("looping");

                }

                AvailableAudioSources.Add(audioSource);

                /*  if (AudioSourceIds.Keys.Count == MaxSources)
                  {
                      AudioSource sourceToStop = PlayingAudioSources[Utils.RandomInt(0, PlayingAudioSources.Count)];
                      foreach (var item in PlayingAudioSources)
                      {
                          if (!item.IsLooping)
                          {
                              sourceToStop = item;
                              break;
                          }
                      }
                      StopSource(sourceToStop);
                  }

                  if (AudioSourceIds.Keys.Count >= MaxSources)
                  {
                      //Max source count reached
                      if (AvailableAudioSources.Count == 0)
                      {
                          //no free buffers availabe. So stop a playing audio and move it to availabe buffer. (worst case)
                          AudioSource sourceToStop;
                          foreach (var item in PlayingAudioSources)
                          {
                              if (!item.IsLooping)
                                  sourceToStop = item;

                          }
                          StopSource(PlayingAudioSources[Utils.RandomInt(0, PlayingAudioSources.Count)]);

                      }

                      //take an available source buffer
                      int index = Utils.RandomInt(0, AvailableAudioSources.Count);
                      var sourceToRemove = AvailableAudioSources[index];
                      var sourceIndex = AudioSourceIds[sourceToRemove];
                      AvailableAudioSources.Remove(sourceToRemove);
                      AudioSourceIds.Remove(sourceToRemove);
                      AudioSourceIds.Add(audioSource, sourceIndex);

                  }
                  else
                  {
                      AudioSourceIds.Add(audioSource, AL.GenSource());
                  }
                  AvailableAudioSources.Add(audioSource);*/
            }
            audioSource.Volume = args.Volume;
            audioSource.Position = args.Position;
            var source = AudioSourceIds[audioSource];
            if (!AvailableAudioSources.Remove(audioSource))
                throw new System.Exception();
            PlayingAudioSources.Add(audioSource);
            audioSource.TimePlaying = 0;
            AL.SourceRewind(source);
            AL.Source(source, ALSourceb.SourceRelative, true);
            AL.Source(source, ALSourcef.Gain, args.Volume);
            AL.Source(source, ALSourcef.Pitch, args.Pitch);
            AL.Source(source, ALSourceb.Looping, args.PlayLooped);
            AL.Source(source, ALSource3f.Position, args.Position.X, args.Position.Y, 0);
            AL.Source(source, ALSource3f.Velocity, 0, 0, 0);
            AL.Source(source, ALSourcei.Buffer, AudioBuffersIds[audioFile]);
            AL.SourcePlay(source);
            OpenGLError.CheckCrashIfDebug();
            return audioSource;
        }

        public void StopSource(AudioSource audioSource)
        {
            if (PlayingAudioSources.Contains(audioSource))
            {
                var id = AudioSourceIds[audioSource];
                AL.SourceStop(id);
                AvailableAudioSources.Add(audioSource);
                if (!PlayingAudioSources.Remove(audioSource))
                    throw new System.Exception();
            }
        }

        public void ForceLoad(AudioFile file)
        {
            if (!AudioBuffersIds.ContainsKey(file))
                AudioBuffersIds.Add(file, AudioFileLoader.Load(file));
        }
    }
}
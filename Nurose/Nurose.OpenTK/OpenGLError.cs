﻿using Nurose.Core;
using OpenTK.Graphics.OpenGL;
using System.Runtime.CompilerServices;
using OpenTK.Audio.OpenAL;

namespace Nurose.OpenTK
{
    public static class OpenGLError
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void CheckCrashIfDebug()
        {
#if (DEBUG)
            while (NuroseMain.Window == null || NuroseMain.Window.IsOpen)
            {
                var error = GL.GetError();
                if (error == ErrorCode.NoError)
                {
                    break;
                }
                Logger.LogError("OpenGL Error" + error.ToString());
            }

            while (NuroseMain.AudioPlayer is OldOpenTKAudioPlayer)
            {
                var error = AL.GetError();
                if (error == ALError.NoError)
                    break;
                Logger.LogError(AL.GetErrorString(error));
            }
#endif
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void CheckWarn()
        {
            while (NuroseMain.Window == null || NuroseMain.Window.IsOpen)
            {
                var error = GL.GetError();
                if (error == ErrorCode.NoError)
                {
                    break;
                }
                Logger.LogWarning("OpenGL Error" + error.ToString());
            }

            while (NuroseMain.AudioPlayer is OldOpenTKAudioPlayer)
            {
                var error = AL.GetError();
                if (error == ALError.NoError)
                    break;
                Logger.LogWarning(AL.GetErrorString(error));
            }
        }
    }
}

﻿using Nurose.Core;
using System;
using OpenTK.Graphics.OpenGL;
using System.Collections.Generic;
using System.IO;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp;

namespace Nurose.OpenTK
{

    internal class TextureManager
    {
        private Dictionary<ITexture, int> TextureIds { get; } = new Dictionary<ITexture, int>();

        public int this[ITexture texture]
        {
            get
            {
                LoadIfUnloaded(texture);
                return TextureIds[texture];
            }
        }

        public void LoadIfUnloaded(ITexture texture)
        {
            if (!TextureIds.ContainsKey(texture) || texture.Initilized == false)
            {
                LoadITexture(texture);
            }
        }

        private void LoadITexture(ITexture texture)
        {
            switch (texture)
            {
                case Texture t:
                    LoadTexture(t);
                    break;
                default:
                    throw new NotImplementedException();
            }

            texture.Initilized = true;
        }

        private void LoadTexture(Texture texture)
        {
            OpenGLError.CheckCrashIfDebug();
            GL.GetInteger(GetPName.Texture2D, out int oldId);

            int id;
            bool updateOnly = TextureIds.ContainsKey(texture);
            if (updateOnly)
            {
                id = TextureIds[texture];
            }
            else
            {
                id = GL.GenTexture();
            }

            GL.BindTexture(TextureTarget.Texture2D, id);
            SetGLTextureParameters(texture);
            OpenGLError.CheckCrashIfDebug();
            switch (texture.LoadOption)
            {
                case TextureLoadOption.File:
                    LoadFileTexture(texture, id);
                    break;
                case TextureLoadOption.Bytes:
                    LoadPixelTexture(texture);
                    break;
                case TextureLoadOption.DepthComponent:
                    LoadDepthTexture(texture);
                    break;
                case TextureLoadOption.DepthStencilComponent:
                    LoadDepthStencilTexture(texture);
                    break;
                case TextureLoadOption.ColorAttachment:
                    LoadColorAttachmentTexture(texture);
                    break;
                default:
                    throw new NotImplementedException();
            }
            OpenGLError.CheckCrashIfDebug();

            if (!updateOnly)
                TextureIds.Add(texture, id);

            GL.BindTexture(TextureTarget.Texture2D, oldId);
            OpenGLError.CheckCrashIfDebug();
        }

        private static void LoadDepthTexture(Texture texture)
        {
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.DepthComponent32f, texture.Size.X, texture.Size.Y, 0, PixelFormat.DepthComponent, PixelType.Float, IntPtr.Zero);
        }

        private static void LoadDepthStencilTexture(Texture texture)
        {
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Depth32fStencil8, texture.Size.X, texture.Size.Y, 0, PixelFormat.DepthStencil, PixelType.Float32UnsignedInt248Rev, IntPtr.Zero);
            OpenGLError.CheckCrashIfDebug();
        }


        private static void LoadColorAttachmentTexture(Texture texture)
        {
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba32f, texture.Size.X, texture.Size.Y, 0, PixelFormat.Rgba, PixelType.Float, IntPtr.Zero);
        }

        private static void LoadPixelTexture(Texture texture)
        {
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, texture.Size.X, texture.Size.Y, 0,
                 PixelFormat.Rgba, PixelType.UnsignedByte, texture.Pixels);
        }

        private static void LoadFileTexture(Texture texture, int id)
        {
            try
            {
                SetGLTextureParameters(texture);

                using Image<Bgra32> image = Image.Load(texture.FilePath).CloneAs<Bgra32>();
                image.DangerousTryGetSinglePixelMemory(out Memory<Bgra32> span);

                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, image.Width, image.Height, 0,
                    PixelFormat.Bgra, PixelType.UnsignedByte, span.ToArray());

                texture.Size = new Vector2Int(image.Width, image.Height);
#if DEBUG
                if (texture.LoadedFromFile)
                {
                    string releative = Path.GetRelativePath(Path.GetFullPath("resources"), Path.GetFullPath(texture.FilePath));
                    Logger.LogDebug($"Loaded texture with id={id} from path=..\\{releative}", null);
                }
                else
                    Logger.LogDebug($"Loaded texture with id={id} from memory", null);
#endif
            }
            catch (ArgumentException)
            {
                Logger.LogError(texture.FilePath + " is not a valid path for a bitmap");
            }
        }

        private static void SetGLTextureParameters(Texture texture)
        {
            OpenGLError.CheckCrashIfDebug();
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)texture.MinFilter);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)texture.MagFilter);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)texture.WrapModeX);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)texture.WrapModeY);
            OpenGLError.CheckCrashIfDebug();
        }
    }
}

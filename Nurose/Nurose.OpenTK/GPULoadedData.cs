﻿namespace Nurose.OpenTK
{
    internal class GPULoadedData
    {
        internal static ShaderManager ShaderManager { get; private set; } = new ShaderManager();
        internal static VertexBuffersStorage VertexBuffers { get; private set; } = new VertexBuffersStorage();
        internal static ShaderBufferStorager ShaderBufferStorager { get; private set; } = new ShaderBufferStorager();
        internal static TextureManager TextureManager { get; private set; } = new TextureManager();
        internal static RenderTextureManager RenderTextureManager { get; private set; } = new RenderTextureManager();

        public static void CheckInitialize()
        {

        }
    }
}
﻿using Nurose.Core;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;

namespace Nurose.OpenTK
{
    internal class VertexBuffersStorage
    {
        private readonly Dictionary<VertexBuffer, LoadedVertexBuffer> LoadedVertices = new();
        private Queue<VertexBuffer> ClaimedBuffers = new();
        public Queue<int> UnclaimedBuffers = new();

        public VertexBuffersStorage()
        {
            Initilize();
        }

        public void Initilize()
        {
            //I hate this. Steam overlay messes up vertexbuffers id generating somehow.
            //So I claim certain amount of buffers at the start and then assign them use them when needed.    
            //This is "temporary" until I figuere out what steam overlay does with the openGL context.

            var bufferAmount = 256;
            int[] ids = new int[bufferAmount];
            // GL.GenBuffers(10, ids);
            GL.GenBuffers(bufferAmount, ids);

            for (var i = 0; i < ids.Length; i++)
            {
                var index = ids[i];
                UnclaimedBuffers.Enqueue(index);
                BindBufferToIndex(new VertexBuffer(new Vertex[]
                {
                    new Vertex(),
                    new Vertex(),
                    new Vertex(),
                    new Vertex()
                }), index);
            }
        }

        public LoadedVertexBuffer Get(VertexBuffer vertexBuffer)
        {
            LoadedVertices.TryGetValue(vertexBuffer, out LoadedVertexBuffer data);
            if (data == null)
            {
                data = RegisterVertices(vertexBuffer);
            }
            else if (vertexBuffer.Updates != data.update)
            {
                UpdateVertices(vertexBuffer);
            }

            return data;
        }

        public void UpdateVertices(VertexBuffer vertexBuffer)
        {
            LoadedVertices.TryGetValue(vertexBuffer, out LoadedVertexBuffer loaded);
            var verts = vertexBuffer.GetVertices();
            GL.BindBuffer(BufferTarget.ArrayBuffer, loaded.index);

            if (loaded.verticesAmount == verts.Length)
                GL.BufferSubData(BufferTarget.ArrayBuffer, 0, (IntPtr)(Vertex.SizeInBytes * verts.Length), verts);
            else
                GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Vertex.SizeInBytes * verts.Length), verts, BufferUsageHint.DynamicDraw);
            loaded.verticesAmount = verts.Length;
            loaded.update = vertexBuffer.Updates;
        }

        private LoadedVertexBuffer RegisterVertices(VertexBuffer vertexBuffer)
        {
            if (UnclaimedBuffers.Count == 0)
                UnlaimOldestEntry();

            int index = UnclaimedBuffers.Dequeue();
            ClaimedBuffers.Enqueue(vertexBuffer);
            LoadedVertexBuffer loadedData = new(index, 0, 0);
            LoadedVertices.Add(vertexBuffer, loadedData);

            UpdateVertices(vertexBuffer);

            Logger.LogDebug($"VertexBuffer id={index} claimed");
            return loadedData;
        }

        private void UnlaimOldestEntry()
        {
            var toUnclaim = ClaimedBuffers.Dequeue();
            var index = LoadedVertices[toUnclaim].index;
            UnclaimedBuffers.Enqueue(index);
            LoadedVertices.Remove(toUnclaim);
            Logger.LogDebug($"Unclaimed vertexbuffer with index={index}");
        }

        private static void BindBufferToIndex(VertexBuffer vertexBuffer, int index)
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, index);
            var vertices = vertexBuffer.GetVertices();
            GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(Vertex.SizeInBytes * vertices.Length), vertices, BufferUsageHint.DynamicDraw);

            GL.GenVertexArrays(1, out int arrayIndex);
            GL.BindVertexArray(arrayIndex);
            GL.BindBuffer(BufferTarget.ArrayBuffer, index);

            GL.EnableVertexAttribArray(0);
            GL.EnableVertexAttribArray(1);
            GL.EnableVertexAttribArray(2);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, Vertex.SizeInBytes, 0);
            GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, false, Vertex.SizeInBytes, 12);
            GL.VertexAttribPointer(2, 4, VertexAttribPointerType.Float, false, Vertex.SizeInBytes, 20);
        }
    }
}
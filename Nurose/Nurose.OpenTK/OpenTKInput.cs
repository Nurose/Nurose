﻿using Nurose.Core;
using OpenTK.Windowing.Common;
using System;

namespace Nurose.OpenTK
{
    public sealed class OpenTKInput : InputHandler
    {
        private readonly OpenTKWindow windowBase;

        public OpenTKInput(OpenTKWindow window, Func<InputState> func) : base(func)
        {
            windowBase = window;
            windowBase.FocusedChanged += WindowBase_FocusedChanged;
            windowBase.KeyDown += Keyboard_KeyDown;
            windowBase.TextInput += WindowBase_TextInput;
            windowBase.KeyUp += Keyboard_KeyUp;
            windowBase.MouseDown += Mouse_ButtonDown;
            windowBase.MouseUp += Mouse_ButtonUp;
            windowBase.MouseMove += Mouse_Move;
            windowBase.MouseWheel += Mouse_Wheel;

        }


        public override void Deregister()
        {
            windowBase.FocusedChanged -= WindowBase_FocusedChanged;
            windowBase.KeyDown -= Keyboard_KeyDown;
            windowBase.TextInput -= WindowBase_TextInput;
            windowBase.KeyUp -= Keyboard_KeyUp;
            windowBase.MouseDown -= Mouse_ButtonDown;
            windowBase.MouseUp -= Mouse_ButtonUp;
            windowBase.MouseMove -= Mouse_Move;
            windowBase.MouseWheel -= Mouse_Wheel; base.Deregister();
        }

        private void WindowBase_TextInput(TextInputEventArgs obj)
        {
            GetInputState().CharInputs += obj.AsString;
        }

        private void WindowBase_FocusedChanged(FocusedChangedEventArgs e)
        {
            if (!windowBase.IsFocused)
            {
                GetInputState().ButtonsHeld.Clear();
                GetInputState().KeysHeld.Clear();
            }
        }

        private void Mouse_Wheel(MouseWheelEventArgs e)
        {
            GetInputState().MouseWheelDelta = (int)e.OffsetY;
        }

        private Vector2Int nextMousePosition;
        private void Mouse_Move(MouseMoveEventArgs e)
        {
            nextMousePosition = new Vector2Int((int)e.X, (int)e.Y);
        }

        private void Mouse_ButtonUp(MouseButtonEventArgs e)
        {
            MouseButton button = (MouseButton)(int)e.Button;
            GetInputState().ButtonsHeld.Remove(button);
            GetInputState().ButtonsReleased.Add(button);
        }

        private void Mouse_ButtonDown(MouseButtonEventArgs e)
        {
            MouseButton button = (MouseButton)(int)e.Button;
            if (!GetInputState().ButtonsHeld.Contains(button))
            {
                GetInputState().ButtonsHeld.Add(button);
            }
            GetInputState().ButtonsPressed.Add(button);
        }

        private void Keyboard_KeyDown(KeyboardKeyEventArgs e)
        {
            Key key = (Key)(int)e.Key;
            if (!GetInputState().KeysHeld.Contains(key))
            {
                GetInputState().KeysHeld.Add(key);
                GetInputState().KeysPressed.Add(key);
            }
        }

        private void Keyboard_KeyUp(KeyboardKeyEventArgs e)
        {
            Key key = (Key)(int)e.Key;

            GetInputState().KeysHeld.Remove(key);
            GetInputState().KeysReleased.Add(key);
        }

        public override void RefreshInput()
        {
            GetInputState().WindowSize = windowBase.Size;
            GetInputState().MonitorSize = windowBase.Monitor.MonitorSizeInPixels;
            GetInputState().MousePosition = nextMousePosition;
            GetInputState().RefreshInput();
        }
    }
}

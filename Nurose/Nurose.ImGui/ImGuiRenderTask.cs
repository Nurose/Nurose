﻿using ImGuiNET;
using Nurose.Core;
using System;
using System.Collections.Generic;

namespace Nurose.ImGUI
{
    public struct ImGuiRenderTask : IRenderTask
    {
        private readonly float Depth;
        private static readonly List<VertexBuffer> buffers = new();
        private readonly ResourceManager ResourceManager;

        public ImGuiRenderTask(ResourceManager ResourceManager, float depth) : this()
        {
            this.ResourceManager = ResourceManager;
            Depth = depth;
        }

        public void Execute(IRenderTarget target)
        {
            ImGui.Render();
            target.SetShader(ShaderProgram.Default);
            new SetColorTintTask(Color.White).Execute(target);
            target.SetCurrentShaderUniform("depth", .3f);
            unsafe
            {
                var drawData = ImGui.GetDrawData();
                var oldview = target.ViewMatrix;
                var oldProjection = target.ProjectionMatrix;
                target.ProjectionMatrix = Matrix4x4.CreateOrthographicOffCenter(0, target.Size.X, target.Size.Y, 0, 1, 10);
                target.ViewMatrix = Matrix4x4.Identity;
                target.ModelMatrix = Matrix3x2.Identity;
                target.DrawBoundsEnabled = true;
                for (int n = 0; n < drawData.CmdListsCount; n++)
                {
                    var cmd_list = drawData.CmdLists[n];
                    var vtx_buffer = cmd_list.VtxBuffer;
                    var idx_buffer = cmd_list.IdxBuffer;

                    List<Vertex> vertices = new();
                    for (int i = 0; i < cmd_list.VtxBuffer.Size; i++)
                    {
                        var v = vtx_buffer[i];
                        Vertex vertex = new(v.pos.ToVector(), Color.FromUint(v.col), v.uv.ToVector());
                        vertices.Add(vertex);
                    }

                    while (buffers.Count < cmd_list.CmdBuffer.Size)
                    {
                        buffers.Add(new VertexBuffer());
                    }

                    int vtxOffset = 0;
                    int idxOffset = 0;

                    for (int cmd_i = 0; cmd_i < cmd_list.CmdBuffer.Size; cmd_i++)
                    {
                        var pcmd = cmd_list.CmdBuffer[cmd_i];
                        if (pcmd.UserCallback != IntPtr.Zero)
                        {
                            throw new NotImplementedException();
                        }
                        else
                        {
                            int txtr = pcmd.TextureId.ToInt32();
                            target.Texture = ResourceManager.Get<ITexture>(txtr);
                            // target.SetDrawBounds(new Vector2Int((int)pcmd->ClipRect.X, (int)pcmd->ClipRect.Y),
                            //   new Vector2Int((int)pcmd->ClipRect.Z - (int)pcmd->ClipRect.X, (int)pcmd->ClipRect.W - (int)pcmd->ClipRect.Y));

                            target.SetDrawBounds(new Vector2Int((int)pcmd.ClipRect.X, (int)pcmd.ClipRect.Y),
                                     new Vector2Int((int)pcmd.ClipRect.Z - (int)pcmd.ClipRect.X, (int)pcmd.ClipRect.W - (int)pcmd.ClipRect.Y));
                            ushort[] indices = new ushort[pcmd.ElemCount];
                            for (int i = vtxOffset; i < indices.Length; i++)
                            {
                                indices[i] = idx_buffer[i + (int)pcmd.IdxOffset];
                            }

                            Vertex[] finalVerts = new Vertex[indices.Length];
                            for (int i = idxOffset; i < indices.Length; i++)
                            {
                                finalVerts[i] = vertices[indices[i]];
                            }
                            buffers[cmd_i].SetVertices(finalVerts);
                            target.RenderVertexBuffer(buffers[cmd_i], PrimitiveType.Triangles);
                        }
                        //idx_buffer.Data += pcmd.ElemCount;
                        //idx_buffer += pcmd.ElemCount;
                    }



                }
                target.DrawBoundsEnabled = false;
                target.ViewMatrix = oldview;
                target.ProjectionMatrix = oldProjection;
                target.SetShader(ShaderProgram.Default);
                //new LoadStoredMatricies(ResourceManager, "pre-imgui").Execute(target);
            }
        }
    }
}

﻿using Nurose.Core;

namespace Nurose.ImGUI
{
    public static class ColorExtentions
    {
        public static System.Numerics.Vector4 ToNumeric(this Color color)
        {
            return new System.Numerics.Vector4(color.R, color.G, color.B, color.A);
        }
    }
}

﻿using Nurose.Core;

namespace Nurose.ImGUI
{
    public struct ImGuiDrawVert
    {
        public const int PosOffset = 0;
        public const int UVOffset = 8;
        public const int ColOffset = 16;
#pragma warning disable CA1051 // Do not declare visible instance fields
        public Vector2 pos;
        public Vector2 uv;
        public uint col;
#pragma warning restore CA1051 // Do not declare visible instance fields
    }
}

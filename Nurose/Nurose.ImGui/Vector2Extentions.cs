﻿using Nurose.Core;
using System.Runtime.CompilerServices;

namespace Nurose.ImGUI
{
    public static class Vector2Extentions
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static System.Numerics.Vector2 ToNumeric(this Vector2 v) => new(v.X, v.Y);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 ToVector(this System.Numerics.Vector2 v) => new(v.X, v.Y);
    }
}

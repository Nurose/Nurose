﻿using ImGuiNET;
using Nurose.Core;
using System.Collections.Generic;
using System.Linq;

namespace Nurose.ImGUI
{
    /*
    public class ImGuiSystem : ECSSystem
    {
        protected ImGuiContext ImGuiContext;

        public float Depth { get; private set; } = 1;

        public virtual void Start()
        {
            ImGuiContext = new ImGuiContext();
            ImGuiContext.SetupFontTexture(ResourceManager);
        }

        public virtual void Draw()
        {
            List<GUITask> tasks = GetSortedTasks();

            ImGuiContext.UpdateIO(World.Input, World.RenderTarget.Size);
            ImGui.NewFrame();
            ImGui.SetWindowFontScale(2);

            foreach (var task in tasks)
                task.Execute(World);

            RenderQueue.StartGroup(DepthLayer.Default.ToFloat());
            ImGuiRenderTask rendertask = ImGuiContext.GetRenderTask(ResourceManager, Depth);
            RenderQueue.Add(rendertask);
            RenderQueue.EndGroup();

        }

        private List<GUITask> GetSortedTasks()
        {
            List<ImGuiComponent> components = World.Components.GetAll<ImGuiComponent>().ToList();
            var dict = new Dictionary<ImGuiComponent, GUITask>();


            GUITask GetTask(ComponentRef<ImGuiComponent> component)
            {
                var key = component.Get(World.Components);
                if (!dict.TryGetValue(key, out GUITask task))
                {
                    task = new GUITask(key);
                    dict.Add(key, task);
                }
                return task;
            }


            for (int i = components.Count - 1; i >= 0; i--)
            {
                var comp = components[i];
                var parent = comp.Parent.Get(World.Components);
                GUITask task = GetTask(comp);
                if (parent != null)
                {
                    GetTask(parent).ChildTasks.Add(task);
                    components.RemoveAt(i);
                }
            }

            foreach (var task in dict.Values)
            {
                int count = task.ChildTasks.Count;
                if (count > 1)
                {
                    var first = task.ChildTasks.Single(t => t.Component.Before == default);
                    var sorted = new List<GUITask> { first };

                    while (sorted.Count < count)
                    {
                        EntityID entityID = sorted.Last().Component.EntityID;
                        var after = task.ChildTasks.Single(t => t.Component.Before.EntityID == entityID);
                        sorted.Add(after);
                    }
                    task.ChildTasks = sorted;
                }
            }
            List<GUITask> tasks = components.Select(t => dict[t]).ToList();
            return tasks;
        }
    }
    */
}

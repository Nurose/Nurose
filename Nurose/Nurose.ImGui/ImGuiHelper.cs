﻿using Nurose.Core;
using System;
using ImGui = System.Numerics;
namespace Nurose.ImGUI
{
    public static class ImGuiHelper
    {
        public static void DrawTexture(ResourceRef<Texture> textureRef, Vector2 size, Color color)
        {
            ImGuiNET.ImGui.Image((IntPtr)textureRef.ResourceId, size.ToNumeric(), new ImGui.Vector2(), ImGui.Vector2.One, color.ToNumeric());
        }

        public static void DrawTexture(ResourceRef<Texture> textureRef, Vector2 size)
        {
            ImGuiNET.ImGui.Image((IntPtr)textureRef.ResourceId, size.ToNumeric(), new ImGui.Vector2(), ImGui.Vector2.One, Color.White.ToNumeric());
        }

        public static void AddLine(this ImGuiNET.ImDrawListPtr drawList, Vector2 pos1, Vector2 pos2, Color col, float thickness)
        {
            drawList.AddLine(pos1.ToNumeric(), pos2.ToNumeric(), col.ToUint(), thickness);
        }

        public static void AddRect(this ImGuiNET.ImDrawListPtr drawList, Vector2 pos1, Vector2 pos2, Color col, float thickness, float rounding = 0)
        {
            drawList.AddRect(pos1.ToNumeric(), pos2.ToNumeric(), col.ToUint(), rounding, 0, thickness);
        }
    }
}

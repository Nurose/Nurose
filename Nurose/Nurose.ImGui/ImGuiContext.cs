﻿using ImGuiNET;
using Nurose.Core;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using System;
using System.IO;

namespace Nurose.ImGUI
{
    public class ImGuiContext
    {
        private Texture texture;
        private readonly VertexBuffer vertexBuffer;
        internal ImGuiIOPtr io;

        public bool WantCaptureMouse => io.WantCaptureMouse;
        public bool WantCaptureKeyboard => io.WantCaptureKeyboard;

        public ImGuiContext()
        {
            vertexBuffer = new VertexBuffer();
        }

        public unsafe void SetupFontTexture(ResourceManager resourceManager, int FontScaleMultiplier = 1)
        {
            IntPtr context = ImGui.CreateContext();
            ImGui.SetCurrentContext(context);
            io = ImGui.GetIO();
            ImFontConfig* nativeConfig = ImGuiNative.ImFontConfig_ImFontConfig();
            Logger.LogAnnouncement(nativeConfig->SizePixels);
            nativeConfig->SizePixels = 13 * FontScaleMultiplier;
            nativeConfig->GlyphMaxAdvanceX *= 1f * FontScaleMultiplier;
            nativeConfig->GlyphMinAdvanceX *= 1f * FontScaleMultiplier;
            ImGui.GetIO().Fonts.AddFontDefault(nativeConfig);
            ImGui.GetStyle().WindowPadding *= FontScaleMultiplier;
            ImGui.GetStyle().FramePadding *= FontScaleMultiplier;
            ImGui.GetStyle().DisplayWindowPadding *= FontScaleMultiplier;
            io.Fonts.GetTexDataAsRGBA32(out byte* pixels, out int width, out int height, out int bytesPerPixel);
            // Build texture atlas

            byte[] bytes = new byte[width * height * bytesPerPixel];
            for (int i = 0; i < (width * height * bytesPerPixel); i++)
            {
                bytes[i] = pixels[i];
            }
            var file = File.OpenWrite("ImGuiFont.png");
            var image = SixLabors.ImageSharp.Image.LoadPixelData<Rgba32>(bytes, width, height);
            image.SaveAsPng(file);
            file.Close();
            texture = new Texture("ImGuiFont.png")
            {
                MagFilter = TextureFilter.Nearest,
                MinFilter = TextureFilter.Nearest
            };
            resourceManager.TryHold("ImGuiFontTexture", texture);
            var id = resourceManager.GetResourceRef<Texture>("ImGuiFontTexture");

            io.Fonts.SetTexID(new IntPtr(id.ResourceId));

            io.Fonts.ClearInputData();
            io.Fonts.ClearTexData();
        }

        public void UpdateIO(InputState input, Vector2Int windowSize)
        {
            io.MouseClicked[0] = input.IsButtonPressed(MouseButton.Left);
            io.MouseClicked[1] = input.IsButtonPressed(MouseButton.Right);
            io.MouseClicked[2] = input.IsButtonPressed(MouseButton.Middle);
            io.MouseDown[0] = input.IsButtonHeld(MouseButton.Left);
            io.MouseDown[1] = input.IsButtonHeld(MouseButton.Right);
            io.MouseDown[2] = input.IsButtonHeld(MouseButton.Middle);

            io.MouseWheel += input.MouseWheelDelta;
            io.MousePos = new System.Numerics.Vector2(input.MousePosition.X, input.MousePosition.Y);
            io.DeltaTime = Time.DeltaTime;
            io.DisplaySize = new System.Numerics.Vector2(windowSize.X, windowSize.Y);
            //io.AddKeyEvent( [(int)ImGuiKey.Backspace] = (int)Key.Backspace;

            io.KeyMods = ImGuiKey.ModNone;
            if (input.IsKeyPressed(Key.LeftShift) || input.IsKeyPressed(Key.RightShift))
                io.KeyMods |= ImGuiKey.ModShift;
            if (input.IsKeyPressed(Key.LeftControl) || input.IsKeyPressed(Key.RightControl))
                io.KeyMods |= ImGuiKey.ModCtrl;
            if (input.IsKeyPressed(Key.LeftAlt) || input.IsKeyPressed(Key.RightAlt))
                io.KeyMods |= ImGuiKey.ModAlt;
            //io.KeysDown[(int)Key.Backspace] = input.IsKeyHeld(Key.Backspace);

            if (!string.IsNullOrEmpty(input.CharInputs))
                io.AddInputCharactersUTF8(input.CharInputs);
        }

        public ImGuiRenderTask GetRenderTask(ResourceManager resourceManager, float Depth)
        {
            ImGui.Render();
            return new ImGuiRenderTask(resourceManager, Depth);
        }
    }
}
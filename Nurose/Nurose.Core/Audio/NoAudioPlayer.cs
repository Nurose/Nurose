namespace Nurose.Core
{
    public class NoAudioPlayer : IAudioPlayer
    {
        public void Update(float dt)
        {
        }
        public AudioSource Play(AudioPlayArgs args)
        {
            return null;
        }
        public void StopSource(AudioSource audioSource)
        {
        }

        public void SetListenerPos(Vector2 pos)
        {
            throw new System.NotImplementedException();
        }

        public void ForceLoad(AudioFile file)
        {
            throw new System.NotImplementedException();
        }
    }
}
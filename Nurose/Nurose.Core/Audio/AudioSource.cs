using System;

namespace Nurose.Core
{
    public class AudioSource
    {
        public int Priority;
        public bool Alive;
        public bool Manual;
        public AudioFile AudioFile;
        public float Volume;
        public float Pitch;
        public float TimePlaying;
        public bool IsLooping;
        public float TimeInactive;
        public Vector2 Position;
        public int PlayId;

        public void Reset()
        {
            Priority = 0;
            Alive = false;
            Manual = false;
            Volume = 0;
            Pitch = 1f;
            TimePlaying = 0;
            IsLooping = false;
            TimeInactive = 0;
            Position = default;
            PlayId = -1;
        }
    }
}
﻿namespace Nurose.Core
{
    public interface IAudioPlayer
    {
        void Update(float dt);
        //void SetListenerPos(Vector2 pos);
        AudioSource Play(AudioPlayArgs args);
        void ForceLoad(AudioFile file);
        void StopSource(AudioSource audioSource);
        void SetListenerPos(Vector2 pos);
    }
}
namespace Nurose.Core
{
    public class NoAudioFactory : IAudioFactory

    {
        public IAudioPlayer CreateAudioPlayer()
        {
            return new NoAudioPlayer();
        }
    }
}
namespace Nurose.Core
{
    public struct AudioPlayArgs
    {
        public AudioFile AudioFile;
        public bool PlayLooped;
        public bool Manual;
        public float Volume;
        public float Pitch;
        public Vector2 Position;
        public int AudioPriority;
    }
}
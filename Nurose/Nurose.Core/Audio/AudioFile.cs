﻿namespace Nurose.Core
{
    public class AudioFile
    {
        public string FilePath;
        public bool Initilized;
        public bool IsStereo;
        public float TimeInSeconds;


        public AudioFile(string path)
        {
            FilePath = path;
        }
    }
}
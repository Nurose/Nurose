namespace Nurose.Core
{
    public struct SetVector2UniformTask : IRenderTask
    {
        public string Name;
        public Vector2 Value;

        public SetVector2UniformTask(string name, Vector2 value)
        {
            Name = name;
            Value = value;
        }

        public void Execute(IRenderTarget renderTarget)
        {
            renderTarget.SetCurrentShaderUniform(Name, Value);
        }
    }
}
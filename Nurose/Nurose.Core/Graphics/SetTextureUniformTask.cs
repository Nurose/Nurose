﻿namespace Nurose.Core
{
    public struct SetTextureUniformTask : IRenderTask
    {
        public string Name;
        public ITexture Texture;

        public SetTextureUniformTask(string name, ITexture value)
        {
            Name = name;
            Texture = value;
        }

        public void Execute(IRenderTarget renderTarget)
        {
            renderTarget.SetCurrentShaderUniform(Name, Texture);
        }
    }
}
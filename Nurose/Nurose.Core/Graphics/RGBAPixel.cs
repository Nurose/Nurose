﻿using System.Runtime.InteropServices;

namespace Nurose.Core
{
    [StructLayout(LayoutKind.Sequential)]
    public struct RGBAPixel
    {
        public readonly byte R;
        public readonly byte G;
        public readonly byte B;
        public readonly byte A;

        public RGBAPixel(byte r, byte g, byte b, byte a)
        {
            R = r;
            G = g;
            B = b;
            A = a;
        }

        public RGBAPixel(Color color)
        {
            color.ToBytes(out byte r, out byte g, out byte b, out byte a);
            R = r;
            G = g;
            B = b;
            A = a;
        }

        public static implicit operator RGBAPixel(Color color)
        {
            return new RGBAPixel(color);
        }

        public override string ToString()
        {
            return $"({R},{G},{B},{A})";
        }
    }
}

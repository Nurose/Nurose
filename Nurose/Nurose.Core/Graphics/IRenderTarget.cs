﻿namespace Nurose.Core
{

    public interface IRenderTarget
    {
        Color ClearColor { get; set; }
        Vector2Int Size { get; set; }
        bool DrawBoundsEnabled { set; get; }
        Blending Blending { get; set; }

        Matrix3x2 ModelMatrix { get; set; }
        Matrix4x4 ViewMatrix { get; set; }
        Matrix4x4 ProjectionMatrix { get; set; }

        ITexture Texture { get; set; }
        void StartTargeting();
        void Clear();

        void RenderVertexBuffer(VertexBuffer vertexBuffer, PrimitiveType primitiveType);
        void Render(RenderQueue queue, bool clearQueue = true);

        /// <summary>
        /// Set the drawbounds in pixel space. Left-top corner is <see cref="Vector2.Zero"/>. Right-bottom corner is equal to <see cref="Size"/>. 
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="size"></param>
        void SetDrawBounds(Vector2Int pos, Vector2Int size);

        Color[] GetPixels();
        Color GetPixel(Vector2Int pos);
        void SaveToFile(string path);

        void SetShader(ShaderProgram shaderProgram);
        void SetCurrentShaderUniform(string name, int value);
        void SetCurrentShaderUniform(string name, uint value);
        void SetCurrentShaderUniform(string name, float value);
        void SetCurrentShaderUniform(string name, double value);
        void SetCurrentShaderUniform(string name, int[] value);
        void SetCurrentShaderUniform(string name, float[] value);
        void SetCurrentShaderUniform(string name, double[] value);
        void SetCurrentShaderUniform(string name, Vector2 value);
        void SetCurrentShaderUniform(string name, Vector3 value);
        void SetCurrentShaderUniform(string name, Matrix4x4 value);
        void SetCurrentShaderUniform(string name, ITexture value);
        void SetCurrentShaderUniform(string name, Color value);
        Vector2 ScreenToWorld(Vector2 position);
        Vector2 WorldToScreen(Vector2 position);
        void ForceLoad(ITexture texture);
        void ForceLoad(RenderTexture texture);
        void SetCurrentShaderUniform<T>(string name, T[] data, int stride) where T : struct;
        void RenderVertexBufferInstanced(VertexBuffer vertexBuffer, PrimitiveType primitiveType, int amount);
        void SetCurrentShaderUniform<T>(string name, T data, int stride) where T : struct;
        void SetPixelsAs(IRenderTarget renderTarget);
        void SetCurrentShaderUniform(string name, Vector3[] value);
        void CopyDepthBufferFrom(IRenderTarget renderTarget);
        void SetShaderStorageBuffer<T>(int location, ShaderStorageBuffer<T> buffer) where T : struct;
    }
}

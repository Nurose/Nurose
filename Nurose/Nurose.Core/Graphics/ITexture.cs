﻿namespace Nurose.Core
{
    public interface ITexture
    {
        Vector2Int Size { get; }
        bool Initilized { get; set; }
    }
}
﻿namespace Nurose.Core
{
    public enum WindowBorder
    {
        Resizable = 0,
        Fixed = 1,
        Hidden = 2
    }
}

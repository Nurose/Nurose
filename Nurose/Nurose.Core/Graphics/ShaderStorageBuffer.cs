﻿namespace Nurose.Core
{
    public class ShaderStorageBuffer<T> : IShaderStorageBuffer where T : struct
    {
        public T[] Data;
        public bool NeedsUpdate;
        
        public void MarkModified()
        {
            NeedsUpdate = true;
        }

        public ShaderStorageBuffer(int size)
        {
            Data = new T[size];
        }
    }
}

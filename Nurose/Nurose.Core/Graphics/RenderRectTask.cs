namespace Nurose.Core
{
    public struct RenderRectTask : IRenderTask
    {
        public UnitRectangleDrawTask rectTask;
        public Color Color;
        public Matrix3x2 Matrix;
        public bool flipX;

        public RenderRectTask(UnitRectangleDrawTask rectTask, Color color, Matrix3x2 model, bool flipX = false)
        {
            this.rectTask = rectTask;
            Color = color;
            Matrix = model;
            this.flipX = flipX;
        }

        public void Execute(IRenderTarget renderTarget)
        {
            new SetColorTintTask(Color).Execute(renderTarget);
            renderTarget.ModelMatrix = Matrix;

            if (rectTask.InvertedXUV != flipX)
                rectTask.InvertedXUV = flipX;

            // if (rectTask.InvertedYUV != flipY)
            //      rectTask.InvertedYUV = flipY;

            rectTask.Execute(renderTarget);
            new SetColorTintTask(Color.White).Execute(renderTarget);
        }
    }
}
﻿namespace Nurose.Core
{
    public struct SetFloatArrayUniformTask : IRenderTask
    {
        public string Name;
        public float[] Value;

        public SetFloatArrayUniformTask(string name, float[] value)
        {
            Name = name;
            Value = value;
        }

        public void Execute(IRenderTarget renderTarget)
        {
            renderTarget.SetCurrentShaderUniform(Name, Value);
        }
    }
}
namespace Nurose.Core
{
    public struct SetFloatUniformTask : IRenderTask
    {
        public string Name;
        public float Value;

        public SetFloatUniformTask(string name, float value)
        {
            Name = name;
            Value = value;
        }

        public void Execute(IRenderTarget renderTarget)
        {
            renderTarget.SetCurrentShaderUniform(Name, Value);
        }
    }
}
﻿namespace Nurose.Core
{
    public struct SetClearColorTask : IRenderTask
    {
        public Color Color;

        public SetClearColorTask(Color color)
        {
            Color = color;
        }

        public void Execute(IRenderTarget renderTarget)
        {
            renderTarget.ClearColor = Color;
        }
    }
}

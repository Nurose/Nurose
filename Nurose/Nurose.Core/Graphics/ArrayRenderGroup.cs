﻿namespace Nurose.Core
{
    public struct TaskGroup : IRenderGroup
    {
        public float Depth { get; set; }
        public IRenderTask[] Tasks;

        public void Execute(IRenderTarget target)
        {
            foreach (var task in Tasks)
            {
                task.Execute(target);
            }
        }

        public override string ToString()
        {
            return $"Taskgroup {{Depth:{Depth}, Count={Tasks.Length}}}";
        }
    }
}

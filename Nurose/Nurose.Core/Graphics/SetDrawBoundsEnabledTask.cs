﻿namespace Nurose.Core
{
    public struct SetDrawBoundsEnabledTask : IRenderTask
    {
        public bool value;


        public void Execute(IRenderTarget renderTarget)
        {
            renderTarget.DrawBoundsEnabled = value;
        }
    }
}
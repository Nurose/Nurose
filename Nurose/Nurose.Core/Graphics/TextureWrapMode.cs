﻿namespace Nurose.Core
{
    public enum TextureWrapMode
    {
        Clamp = 10496,
        Repeat = 10497,
        ClampToBorder = 33069,
        ClampToEdge = 33071,
        MirroredRepeat = 33648
    }
}

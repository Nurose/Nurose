﻿namespace Nurose.Core
{
    public sealed class FragmentShaders
    {

        public static Shader Default => Shader.LoadFromCode(@"

            #version 330 core
            uniform sampler2D mainTex;

            in vec2 uv;
            in vec2 screenPos;
            in vec4 vertexColor;
            out vec4 color;

            //uniform float time;

            void main()
            {
                color = texture(mainTex, uv);
                color *= vertexColor;
                if(color.a < 0.01)
                    discard;
            }
            ");

        public static Shader SingleColor => Shader.LoadFromCode(@"

            #version 330 core
            uniform sampler2D mainTex;

            in vec2 uv;
            in vec2 screenPos;
            in vec4 vertexColor;
            out vec4 color;

            //uniform float time;

            void main()
            {
	            color = vertexColor;
            }
            ");
    }
}
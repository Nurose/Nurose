﻿namespace Nurose.Core
{
    public static class VertexShaders
    {
        public static Shader Default => Shader.LoadFromCode(@"

            #version 330 core
            #define ClipToWorld(model,view,projection,pos) (projection * view * model * vec4(pos,1.,1.))
            #define TransformDepth(z, p) ((1./z - 1./p.x) / (1./p.y - 1./p.x))
            #define ClippingPlanes(m) (vec2(m[3][2] * m[2][2], (1. / m[2][2] - m[3][2] * m[2][2]) * - 1.))

            float near = 0.1; 
            float far  = 100.0; 
  
            float LinearizeDepth(float depth) 
            {
                float z = depth * 2.0 - 1.0; // back to NDC 
                return (2.0 * near * far) / (far + near - z * (far - near));	
            }

            layout (location = 0) in vec2 position;
            layout (location = 1) in vec2 texcoord;
            layout (location = 2) in vec4 color;

            uniform mat4 model;
            uniform mat4 view;
            uniform mat4 projection;
            uniform vec4 colorTint;
            uniform float depth;


            //uniform float time;

            out vec2 uv;
            out vec2 screenPos;
            out vec4 vertexColor;

            void main()
            {
               gl_Position = ClipToWorld(model, view, projection, position);
               //gl_Position.z = TransformDepth(depth, ClippingPlanes(projection));
               gl_Position.z = LinearizeDepth(depth);
               uv = texcoord;
	           vertexColor = color * colorTint;
	           screenPos = gl_Position.xy;
            }
            ");
    }
}
﻿namespace Nurose.Core
{
    public static class ShaderConstants
    {
        public const string ModelMatrix = "model";
        public const string ViewMatrix = "view";
        public const string ProjectionMatrix = "projection";
        public const string Time = "time";
        public const string ColorTint = "colorTint";
        public const string Depth = "depth";

        public static readonly string[] All = new string[] { ModelMatrix, ViewMatrix, ProjectionMatrix, Time, ColorTint,Depth };
    }
}

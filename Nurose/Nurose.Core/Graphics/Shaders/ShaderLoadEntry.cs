﻿namespace Nurose.Core
{
    public class Shader
    {
        public bool LoadedFromFile;
        public string Code;
        public string FilePath;

        public static Shader LoadFromFile(string path)
        {
            return new Shader
            {
                LoadedFromFile = true,
                FilePath = path,
            };
        }

        public static Shader LoadFromCode(string code)
        {
            return new Shader
            {
                LoadedFromFile = false,
                Code = code,
            };
        }

    }
}

﻿using System;
using System.Collections.Generic;

namespace Nurose.Core
{
    public sealed class ShaderProgram
    {
        public ShaderProgram()
        {
            VertexShader = VertexShaders.Default;
            FragmentShader = FragmentShaders.Default;
        }

        public ShaderProgram(Shader fragmentShader, Shader vertexShader)
        {
            FragmentShader = fragmentShader;
            VertexShader = vertexShader;
        }

        private readonly Dictionary<string, ShaderUniform> data = new();
        private readonly Dictionary<string, int> TextureUnits = new();
        private readonly Dictionary<string, int> UniformBufferIndex = new();


        public IRenderTarget RenderTarget { get; set; }



        public Shader FragmentShader { get; }
        public Shader VertexShader { get; }

        public bool Initialised { get; set; }

        private int LastBoundedTextureUnit = -1;

        public void SetMainTexture(Texture texture)
        {
            // TODO Dit moet hier zo lief staan. Moet ergens een constant zijn (bij FragmentShader?).
            SetUniform("mainTex", texture);
        }


        public void AddUniformBuffer(string name, int index)
        {
            UniformBufferIndex.Add(name, index);
        }

        public bool HasUniformBuffer(string name)
        {
            return UniformBufferIndex.ContainsKey(name);
        }

        public int GetUniformBuffer(string name)
        {
            return UniformBufferIndex[name];
        }


        public int GetTextureUnit(string uniformName)
        {
            if (!TextureUnits.ContainsKey(uniformName))
            {
                TextureUnits.Add(uniformName, LastBoundedTextureUnit + 1);
                LastBoundedTextureUnit += 1;
            }

            return TextureUnits[uniformName];
        }

        public static ShaderProgram Default { get; } = new ShaderProgram();

        public void SetUniform(string name, uint value)
        {
            RenderTarget.SetCurrentShaderUniform(name, value);
            UpdateValue(name, value);
        }

        public void SetUniform(string name, int value)
        {
            RenderTarget.SetCurrentShaderUniform(name, value);
            UpdateValue(name, value);
        }

        public void SetUniform(string name, float value)
        {
            RenderTarget.SetCurrentShaderUniform(name, value);
            UpdateValue(name, value);
        }

        public void SetUniform(string name, double value)
        {
            RenderTarget.SetCurrentShaderUniform(name, value);
            UpdateValue(name, value);
        }

        public void SetUniform(string name, Color value)
        {
            RenderTarget.SetCurrentShaderUniform(name, value);
            UpdateValue(name, value);
        }

        public void SetUniform(string name, Vector2 value)
        {
            RenderTarget.SetCurrentShaderUniform(name, value);
            UpdateValue(name, value);
        }

        public void SetUniform(string name, Vector3 value)
        {
            RenderTarget.SetCurrentShaderUniform(name, value);
            UpdateValue(name, value);
        }

        public void SetUniform(string name, Matrix4x4 value)
        {
            RenderTarget.SetCurrentShaderUniform(name, value);
            UpdateValue(name, value);
        }

        public void SetUniform(string name, ITexture value)
        {
            RenderTarget.SetCurrentShaderUniform(name, value);
            UpdateValue(name, value);
        }

        private void UpdateValue(string name, object value)
        {
            if (data.TryGetValue(name, out ShaderUniform result))
            {
                result.Value = value;
            }
            else
            {
                data.Add(name, new ShaderUniform() { Value = value });
            }
        }

        public object GetUniform(string name)
        {
            if (data.TryGetValue(name, out ShaderUniform result))
            {
                return result.Value;
            }
            else
            {
                throw new ArgumentException("Invalid uniform name");
            }
        }

        public T GetUniform<T>(string name)
        {
            return (T)GetUniform(name);
        }

        public void SetUniformLocation(string name, int location)
        {
            if (data.TryGetValue(name, out ShaderUniform result))
            {
                result.Location = location;
            }
            else
            {
                data.Add(name, new ShaderUniform() { Location = location });
            }
        }

        public int? GetUniformLocation(string name)
        {
            if (data.TryGetValue(name, out ShaderUniform uniform))
                return uniform.Location;

            return null;
        }

        public Dictionary<string, ShaderUniform>.Enumerator GetEnumerator()
        {
            return data.GetEnumerator();
        }
    }
}
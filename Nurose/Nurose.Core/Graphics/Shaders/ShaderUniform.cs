﻿namespace Nurose.Core
{
    public class ShaderUniform
    {
        public ShaderUniform()
        {
        }

        public ShaderUniform(object value)
        {
            Value = value;
        }

        public object Value { get; set; }
        public int Location { get; set; } = -1;
    }
}

﻿namespace Nurose.Core
{
    public struct UnsortedGroupsTask : IRenderTask
    {
        public float Depth { get; set; }
        public UnsortedGroupsTask[] Children;
        public IRenderTask[] BeforeTasks;
        public IRenderTask[] AfterTasks;

        public void Execute(IRenderTarget target)
        {
            foreach (var task in BeforeTasks)
                task.Execute(target);

            foreach (var child in Children)
                child.Execute(target);

            foreach (var task in AfterTasks)
                task.Execute(target);
        }
    }
}

﻿namespace Nurose.Core
{
    public struct SetColorUniformTask : IRenderTask
    {
        public string Name;
        public Color Color;

        public SetColorUniformTask(string name, Color color)
        {
            Name = name;
            Color = color;
        }

        public void Execute(IRenderTarget renderTarget)
        {
            renderTarget.SetCurrentShaderUniform(Name, Color);
        }
    }
}
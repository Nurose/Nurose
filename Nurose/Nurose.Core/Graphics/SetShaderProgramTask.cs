﻿namespace Nurose.Core
{
    public struct SetShaderProgramTask : IRenderTask
    {
        public ShaderProgram Shader;

        public SetShaderProgramTask(ShaderProgram shader)
        {
            Shader = shader;
        }

        public void Execute(IRenderTarget renderTarget)
        {
            renderTarget.SetShader(Shader);
        }
    }
}

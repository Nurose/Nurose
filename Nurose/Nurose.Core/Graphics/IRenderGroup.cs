﻿namespace Nurose.Core
{
    public interface IRenderGroup
    {
        float Depth { get; }
        void Execute(IRenderTarget target);
    }
}

﻿namespace Nurose.Core
{
    public struct DrawQueueTask : IRenderTask
    {
        public RenderQueue Queue;

        public DrawQueueTask(RenderQueue drawer)
        {
            Queue = drawer;
        }

        public void Execute(IRenderTarget renderTarget)
        {
            renderTarget.Render(Queue);
        }
    }
}
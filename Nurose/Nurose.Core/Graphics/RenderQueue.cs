﻿using System;
using System.Collections.Generic;

namespace Nurose.Core
{
    public class RenderQueue
    {
        public List<TaskGroup> Groups = new();
        private float? z;
        private List<IRenderTask> CurrentList = new();



        public void TakeGroupsFrom(RenderQueue renderQueue)
        {
            foreach (var taskGroup in renderQueue.Groups)
                Groups.Add(taskGroup);

            renderQueue.Clear();
        }

        public virtual void Clear()
        {
            Groups.Clear();
            CurrentList.Clear();
            z = null;
        }

        public virtual void StartGroup(float ZIndex)
        {
            z = ZIndex;
            CurrentList.Clear();
        }

        public virtual void AddGroup(TaskGroup group)
        {
            Groups.Add(group);
        }

        public virtual void Add(IRenderTask task)
        {
            if (z == null)
            {
                throw new Exception("Not currently in group");
            }
            CurrentList.Add(task);
        }

        public virtual void EndGroup()
        {
            Groups.Add(new TaskGroup()
            {
                Tasks = CurrentList.ToArray(),
                Depth = z.Value
            });
            z = null;
        }
    }
}
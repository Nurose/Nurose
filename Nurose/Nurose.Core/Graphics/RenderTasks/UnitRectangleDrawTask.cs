﻿namespace Nurose.Core
{
    public class UnitRectangleDrawTask : IRenderTask
    {
        public Color Color { get => color; set { requiresUpdate = value != Color; color = value; } }
        public bool InvertedXUV { get => invertedXUV; set { requiresUpdate = value != invertedXUV; invertedXUV = value; } }
        public bool InvertedYUV { get => invertedYUV; set { requiresUpdate = value != invertedYUV; invertedYUV = value; } }

        public readonly VertexBuffer buffer;
        private Color color;

        private bool requiresUpdate;
        private bool invertedXUV;
        private bool invertedYUV;

        private void Update()
        {
            buffer.SetVertices(GenerateVertices());
        }

        public UnitRectangleDrawTask(Color color, bool invertedYUV = true, bool invertedXUV = false)
        {
            this.color = color;
            this.invertedXUV = invertedXUV;
            this.invertedYUV = invertedYUV;
            this.buffer = new VertexBuffer();
            this.requiresUpdate = true;
            buffer = new VertexBuffer(GenerateVertices());
        }

        private Vertex[] GenerateVertices()
        {
            requiresUpdate = false;
            Vertex[] vertices = new Vertex[6];
            int x1 = invertedXUV ? 0 : 1;
            int x2 = invertedXUV ? 1 : 0;
            int y1 = invertedYUV ? 1 : 0;
            int y2 = invertedYUV ? 0 : 1;

            vertices[0] = new Vertex(new Vector2(0, 0), Color, new Vector2(x2, y1));
            vertices[1] = new Vertex(new Vector2(1, 0), Color, new Vector2(x1, y1));
            vertices[2] = new Vertex(new Vector2(0, 1), Color, new Vector2(x2, y2));

            vertices[3] = new Vertex(new Vector2(1, 1), Color, new Vector2(x1, y2));
            vertices[4] = new Vertex(new Vector2(0, 1), Color, new Vector2(x2, y2));
            vertices[5] = new Vertex(new Vector2(1, 0), Color, new Vector2(x1, y1));


            return vertices;
        }

        public void Execute(IRenderTarget RenderTarget)
        {
            if (requiresUpdate)
                Update();

            RenderTarget.RenderVertexBuffer(buffer, PrimitiveType.Triangles);
        }
    }
}

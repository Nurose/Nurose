﻿namespace Nurose.Core
{
    public class VertexBuffer
    {
        private Vertex[] _vertices;

        public int Updates { get; private set; }

        public Vertex[] GetVertices()
        {
            return _vertices;
        }

        public void SetVertices(Vertex[] value)
        {
            _vertices = value;
            Updates++;
        }

        public void ForceUpdate()
        {
            Updates++;
        }

        public VertexBuffer(Vertex[] vertices)
        {
            SetVertices(vertices);
        }

        public VertexBuffer()
        {
            SetVertices(System.Array.Empty<Vertex>());
        }
    }
}

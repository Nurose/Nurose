﻿namespace Nurose.Core
{
    public struct SetModelMatrixTask : IRenderTask
    {
        private Matrix3x2 matrix;

        public SetModelMatrixTask(Matrix3x2 matrix)
        {
            this.matrix = matrix;
        }

        public void Execute(IRenderTarget RenderTarget)
        {
            RenderTarget.ModelMatrix = matrix;
        }
    }
}

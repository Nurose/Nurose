﻿namespace Nurose.Core
{
    public struct SetPixelCoordinates : IRenderTask
    {
        public void Execute(IRenderTarget target)
        {
            target.ProjectionMatrix = Matrix4x4.CreateOrthographicOffCenter(0, target.Size.X, target.Size.Y, 0, 1, 10);
            target.ViewMatrix = Matrix4x4.Identity;
            target.ModelMatrix = Matrix3x2.Identity;
        }
    }
}

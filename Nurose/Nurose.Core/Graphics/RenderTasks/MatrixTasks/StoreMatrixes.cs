﻿namespace Nurose.Core
{
    public struct StoreMatrixes : IRenderTask
    {
        private readonly ResourceManager resourceManager;
        private readonly string name;

        public StoreMatrixes(ResourceManager resourceManager, string name)
        {
            this.resourceManager = resourceManager;
            this.name = name;
        }

        public void Execute(IRenderTarget target)
        {
            resourceManager.OverrideHold($"{name}-model", target.ModelMatrix);
            resourceManager.OverrideHold($"{name}-view", target.ViewMatrix);
            resourceManager.OverrideHold($"{name}-projection", target.ProjectionMatrix);
        }
    }
}

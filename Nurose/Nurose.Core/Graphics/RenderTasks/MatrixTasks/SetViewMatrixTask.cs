﻿namespace Nurose.Core
{
    public struct SetViewMatrixTask : IRenderTask
    {
        private Matrix4x4 matrix;

        public SetViewMatrixTask(Matrix4x4 matrix)
        {
            this.matrix = matrix;
        }

        public void Execute(IRenderTarget RenderTarget)
        {
            RenderTarget.ViewMatrix = matrix;
        }
    }
}

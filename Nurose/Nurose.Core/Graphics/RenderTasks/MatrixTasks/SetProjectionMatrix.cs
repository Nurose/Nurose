﻿namespace Nurose.Core
{
    public struct SetProjectionMatrix : IRenderTask
    {
        private Matrix4x4 matrix;

        public SetProjectionMatrix(Matrix4x4 matrix)
        {
            this.matrix = matrix;
        }

        public void Execute(IRenderTarget RenderTarget)
        {
            RenderTarget.ProjectionMatrix = matrix;
        }
    }
}
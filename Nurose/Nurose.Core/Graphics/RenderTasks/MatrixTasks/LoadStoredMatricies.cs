﻿namespace Nurose.Core
{
    public struct LoadStoredMatricies : IRenderTask
    {
        private readonly ResourceManager resourceManager;
        private readonly string Name;

        public LoadStoredMatricies(ResourceManager resourceManager, string name)
        {
            this.resourceManager = resourceManager;
            Name = name;
        }

        public void Execute(IRenderTarget target)
        {
            target.ModelMatrix = resourceManager.Get<Matrix3x2>($"{Name}-model");
            target.ViewMatrix = resourceManager.Get<Matrix4x4>($"{Name}-view");
            target.ProjectionMatrix = resourceManager.Get<Matrix4x4>($"{Name}-projection");
        }
    }
}

﻿namespace Nurose.Core
{
    public struct VertexRenderTask : IRenderTask
    {
        public PrimitiveType PrimitiveType;
        public VertexBuffer VertexBuffer;

        public VertexRenderTask(PrimitiveType primitiveType, VertexBuffer vertexBuffer)
        {
            PrimitiveType = primitiveType;
            VertexBuffer = vertexBuffer;
        }

        public void Execute(IRenderTarget RenderTarget)
        {
            RenderTarget.RenderVertexBuffer(VertexBuffer, PrimitiveType);
        }
    }
}

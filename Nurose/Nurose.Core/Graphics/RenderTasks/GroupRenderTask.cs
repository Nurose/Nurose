﻿using System.Collections.Generic;

namespace Nurose.Core
{
    public struct GroupRenderTask : IRenderTask
    {
        private readonly IEnumerable<IRenderTask> tasks;

        public GroupRenderTask(IEnumerable<IRenderTask> tasks)
        {
            this.tasks = tasks;
        }

        public void Execute(IRenderTarget RenderTarget)
        {
            foreach (var task in tasks)
            {
                task.Execute(RenderTarget);
            }
        }
    }
}

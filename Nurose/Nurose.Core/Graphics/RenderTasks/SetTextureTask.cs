﻿namespace Nurose.Core
{
    public struct SetTextureTask : IRenderTask
    {
        public readonly ITexture Texture;

        public SetTextureTask(ITexture texture)
        {
            Texture = texture;
        }

        public void Execute(IRenderTarget RenderTarget)
        {
            RenderTarget.Texture = Texture;
        }
    }


    public struct UpdateVertexBufferTask : IRenderTask
    {
        private readonly VertexBuffer vertexBuffer;
        private readonly Vertex[] vertices;

        public UpdateVertexBufferTask(VertexBuffer vertexBuffer, Vertex[] vertices)
        {
            this.vertexBuffer = vertexBuffer;
            this.vertices = vertices;
        }

        public void Execute(IRenderTarget RenderTarget)
        {
            vertexBuffer.SetVertices(vertices);
        }
    }
}
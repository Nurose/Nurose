﻿namespace Nurose.Core
{
    public interface IPoolableRenderTask : IRenderTask
    {
        int PoolIndex { get; set; }
    }

    public interface IRenderTask
    {
        void Execute(IRenderTarget renderTarget);
    }
}

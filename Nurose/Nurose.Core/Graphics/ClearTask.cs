﻿namespace Nurose.Core
{
    public struct ClearTask : IRenderTask
    {
        public void Execute(IRenderTarget renderTarget)
        {
            renderTarget.Clear();
        }
    }
}

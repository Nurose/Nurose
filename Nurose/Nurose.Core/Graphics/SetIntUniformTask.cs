﻿namespace Nurose.Core
{
    public struct SetIntUniformTask : IRenderTask
    {
        public string Name;
        public int Value;

        public SetIntUniformTask(string name, int value)
        {
            Name = name;
            Value = value;
        }

        public void Execute(IRenderTarget renderTarget)
        {
            renderTarget.SetCurrentShaderUniform(Name, Value);
        }
    }
}
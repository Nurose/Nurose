﻿namespace Nurose.Core
{
    public struct SetDrawBoundsTask : IRenderTask
    {
        public FloatRect FloatRect;

        public SetDrawBoundsTask(FloatRect floatRect)
        {
            FloatRect = floatRect;
        }

        public void Execute(IRenderTarget renderTarget)
        {
            renderTarget.SetDrawBounds((Vector2Int)FloatRect.LeftTop, (Vector2Int)FloatRect.Size);
        }
    }
}
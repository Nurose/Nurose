﻿namespace Nurose.Core
{
    public enum TextureLoadOption
    {
        File,
        Bytes,
        ColorAttachment,
        DepthComponent,
        DepthStencilComponent,
    }

    public class Texture : ITexture
    {
        public TextureFilter MinFilter { get; set; } = TextureFilter.Nearest;
        public TextureFilter MagFilter { get; set; } = TextureFilter.Linear;
        public TextureWrapMode WrapModeX { get; set; } = TextureWrapMode.Clamp;
        public TextureWrapMode WrapModeY { get; set; } = TextureWrapMode.Clamp;
        public Vector2Int Size { get; set; }
        public TextureLoadOption LoadOption { get; init; }
        public bool LoadedFromFile => LoadOption == TextureLoadOption.File;
        public RGBAPixel[] Pixels { get; private set; }
        public bool Initilized { get; set; }

        public string FilePath { get; }

        public static Texture White1x1 { get; } = new Texture(new Vector2Int(1, 1), new RGBAPixel[]
        {
            Color.White
        })
        {
            MinFilter = TextureFilter.Nearest,
            MagFilter = TextureFilter.Nearest
        };
        public static Texture Transparent { get; } = new Texture(new Vector2Int(1, 1), new RGBAPixel[]
        {
            Color.Transparent
        })

        {
            MinFilter = TextureFilter.Nearest,
            MagFilter = TextureFilter.Nearest
        };

        public static Texture PurpleBlackCheckerboard { get; } = new Texture(new Vector2Int(2, 2),
            new RGBAPixel[]
            {
                Color.Black,
                Color.Magenta,
                Color.Magenta,
                Color.Black,
            })
        {
            MagFilter = TextureFilter.Nearest,
            MinFilter = TextureFilter.Nearest
        };

        public Texture(string filePath)
        {
            LoadOption = TextureLoadOption.File;
            FilePath = filePath;
        }

        public Texture()
        {

        }

        public Texture(Vector2Int size, RGBAPixel[] pixels)
        {
            LoadOption = TextureLoadOption.Bytes;
            Size = size;
            Pixels = pixels;
        }
    }
}
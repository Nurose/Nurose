namespace Nurose.Core
{
    public struct SetColorTintTask : IRenderTask
    {
        public Color Color;

        public SetColorTintTask(Color color)
        {
            Color = color;
        }

        public void Execute(IRenderTarget renderTarget)
        {
            renderTarget.SetCurrentShaderUniform("colorTint", Color);
        }
    }
}
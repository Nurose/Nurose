﻿using System.Collections.Generic;

namespace Nurose.Core
{
    public class Material
    {
        public ShaderProgram ShaderProgram;
        public Dictionary<string, object> Uniforms;
    }
}

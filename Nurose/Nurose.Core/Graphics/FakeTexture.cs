﻿namespace Nurose.Core
{
    public class FakeTexture : ITexture
    {
        public Vector2Int Size { get; set; }
        public bool Initilized { get; set; }
    }
}
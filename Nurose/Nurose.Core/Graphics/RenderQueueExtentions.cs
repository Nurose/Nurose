﻿using System.Runtime.CompilerServices;

namespace Nurose.Core
{
    public static class RenderQueueExtentions
    {
        private static ResourceManager ResourceManager { get; set; } = new ResourceManager();
        private static UnitRectangleDrawTask rectTask = new(Color.White, false);


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void DrawVertexBuffer(this RenderQueue renderQueue, VertexBuffer buffer, PrimitiveType primitiveType)
        {
            renderQueue.Add(new VertexRenderTask(primitiveType, buffer));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void SetTexture(this RenderQueue renderQueue, ITexture texture)
        {
            renderQueue.Add(new SetTextureTask(texture));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void StartPixelSpace(this RenderQueue drawer)
        {
            drawer.Add(new StoreMatrixes(ResourceManager, "world"));
            drawer.Add(new SetPixelCoordinates());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void EndPixelSpace(this RenderQueue drawer)
        {
            drawer.Add(new LoadStoredMatricies(ResourceManager, "world"));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void SetTextureUniform(this RenderQueue renderQueue, string name, ITexture texture)
        {
            renderQueue.Add(new SetTextureUniformTask(name, texture));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void SetFloatUniform(this RenderQueue renderQueue, string name, float value)
        {
            renderQueue.Add(new SetFloatUniformTask(name, value));
        }
        public static void SetFloatArrayUniform(this RenderQueue renderQueue, string name, float[] value)
        {
            renderQueue.Add(new SetFloatArrayUniformTask(name, value));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void SetColorUniform(this RenderQueue renderQueue, string name, Color value)
        {
            renderQueue.Add(new SetColorUniformTask(name, value));
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void SetVector2Uniform(this RenderQueue renderQueue, string name, Vector2 value)
        {
            renderQueue.Add(new SetVector2UniformTask(name, value));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void ClearTexture(this RenderQueue renderQueue)
        {
            renderQueue.Add(new SetTextureTask(null));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void UpdateVertexBuffer(this RenderQueue renderQueue, VertexBuffer vertexBuffer, Vertex[] vertices)
        {
            renderQueue.Add(new UpdateVertexBufferTask(vertexBuffer, vertices));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void SetDrawBounds(this RenderQueue renderQueue, FloatRect bounds)
        {
            renderQueue.Add(new SetDrawBoundsTask(bounds));
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void SetDrawBounds(this RenderQueue renderQueue, Vector2Int pos, Vector2Int size)
        {
            renderQueue.Add(new SetDrawBoundsTask(new FloatRect(pos, size)));
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void SetDrawBoundsEnabled(this RenderQueue renderQueue, bool value)
        {
            renderQueue.Add(new SetDrawBoundsEnabledTask() { value = value });
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void SetShaderProgram(this RenderQueue renderQueue, ShaderProgram shader)
        {
            renderQueue.Add(new SetShaderProgramTask(shader));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void SetColorTint(this RenderQueue renderQueue, Color tint)
        {
            renderQueue.Add(new SetColorTintTask(tint));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void SetCameraMatrix(this RenderQueue renderQueue, Matrix4x4 matrix)
        {
            renderQueue.Add(new SetViewMatrixTask(matrix));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void SetModelMatrix(this RenderQueue renderQueue, Matrix3x2 matrix)
        {
            renderQueue.Add(new SetModelMatrixTask(matrix));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void SetProjectionMatrix(this RenderQueue renderQueue, Matrix4x4 matrix)
        {
            renderQueue.Add(new SetProjectionMatrix(matrix));
        }

        public static void DrawRect(this RenderQueue drawer, Vector2 position, Vector2 size, Color color, ITexture texture = null)
        {
            if (texture == null)
                drawer.SetTexture(Texture.White1x1);
            else
                drawer.SetTexture(texture);

            drawer.Add(new RenderRectTask(rectTask, color, new TransformData(position, size).CalcModelMatrix()));
        }

        public static void DrawRect(this RenderQueue drawer, Vector2 position, Vector2 size, Color color, ITexture texture, float rotation)
        {
            if (texture == null)
                drawer.SetTexture(Texture.White1x1);
            else
                drawer.SetTexture(texture);

            drawer.Add(new RenderRectTask(rectTask, color, new TransformData(position, size, rotation, Vector2.One / 2).CalcModelMatrix()));
        }

    }
}

﻿namespace Nurose.Core
{
    public interface IAudioFactory
    {
        IAudioPlayer CreateAudioPlayer();
    }
}
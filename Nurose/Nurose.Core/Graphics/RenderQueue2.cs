﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Nurose.Core
{
    public class RenderQueue2
    {
        public readonly Queue<IRenderTask> Tasks;
        public RenderQueue2()
        {
            Tasks = new Queue<IRenderTask>();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void ClearQueue()
        {
            Tasks.Clear();
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Draw(IRenderTask task)
        {
#if DEBUG
            if (task == null)
            {
                Logger.LogError("Can't add task with null value");
            }
#endif
            Tasks.Enqueue(task);
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Draw(VertexBuffer buffer, PrimitiveType primitiveType)
        {
            Draw(new VertexRenderTask(primitiveType, buffer));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SetTexture(Texture texture)
        {
            Draw(new SetTextureTask(texture));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void ClearTexture()
        {
            Draw(new SetTextureTask(null));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void UpdateVertexBuffer(VertexBuffer vertexBuffer, Vertex[] vertices)
        {
            Draw(new UpdateVertexBufferTask(vertexBuffer, vertices));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SetDrawBounds(FloatRect bounds)
        {
            Draw(new SetDrawBoundsTask(bounds));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SetDrawBoundsEnabled(bool value)
        {
            Draw(new SetDrawBoundsEnabledTask() { value = value });
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Draw(RenderQueue queue)
        {
            Draw(new DrawQueueTask(queue));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SetShaderProgram(ShaderProgram shader)
        {
            Draw(new SetShaderProgramTask(shader));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SetCameraMatrix(Matrix4x4 matrix)
        {
            Draw(new SetViewMatrixTask(matrix));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SetModelMatrix(Matrix3x2 matrix)
        {
            Draw(new SetModelMatrixTask(matrix));
        }

    }
}

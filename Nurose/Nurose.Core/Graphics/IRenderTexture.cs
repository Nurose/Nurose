﻿namespace Nurose.Core
{
    public sealed class RenderTexture
    {
        private Vector2Int size;
        public readonly bool hasStencilBuffer;

        public IRenderTarget RenderTarget { get; set; }
        public ITexture DepthTexture;
        public ITexture ColorTexture;
        public Vector2Int Size
        {
            get => size;
            set
            {
                size = value;
                
                if (RenderTarget != null)
                    RenderTarget.Size = value;

                ColorTexture.Initilized = false;
                
                if (DepthTexture != null)
                    DepthTexture.Initilized = false;
            }
        }
        public bool Initilized
        {
            get => ColorTexture.Initilized;
        }

        public RenderTexture(Vector2Int size, bool depthBuffer = false, bool hasStencilBuffer = false)
        {
            this.size = size;
            this.hasStencilBuffer = hasStencilBuffer;
            ColorTexture = new Texture
            {
                LoadOption = TextureLoadOption.ColorAttachment,
                MinFilter = TextureFilter.Nearest,
                MagFilter = TextureFilter.Nearest,
            };
            if (depthBuffer)
                DepthTexture = new Texture
                {
                    LoadOption = hasStencilBuffer ? TextureLoadOption.DepthStencilComponent : TextureLoadOption.DepthComponent,
                    MinFilter = TextureFilter.Nearest,
                    MagFilter = TextureFilter.Nearest,
                };

            NuroseMain.Window.RenderTarget.ForceLoad(this);
        }
    }
}
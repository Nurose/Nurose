﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Nurose.Core
{
    public sealed class NuroseMain
    {
        public static IWindowFactory WindowFactory => Instance.windowFactory;
        public static IAudioFactory AudioFactory => Instance.audioFactory;
        public static IWindow Window => Instance.window;
        public static IAudioPlayer AudioPlayer => Instance.audioPlayer;


        public static IMonitor Monitor => Instance.monitor;


        public static Action AlternativeMainLoop { get; set; }

        public static event EventHandler OnRenderStart;
        public static event EventHandler OnRenderEnd;

        private static NuroseMain Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new NuroseMain();
                }

                return instance;
            }
            set => instance = value;
        }

        private IWindow window;
        private IMonitor monitor;
        private IAudioPlayer audioPlayer;

        private IAudioFactory audioFactory;
        private IWindowFactory windowFactory;
        private IInputFactory inputFactory;

        private static NuroseMain instance;

        private List<IEntryLogic> logics = new();


        public static T GetLogic<T>() where T : IEntryLogic
        {
            return (T)instance.logics.SingleOrDefault(e => e is T);
        }

        public static T AddLogic<T>() where T : IEntryLogic
        {
            var entryLogic = Activator.CreateInstance<T>();

            entryLogic.RenderTarget = Window.RenderTarget;
            entryLogic.Input = Window.Input;

            instance.logics.Add(entryLogic);
            return entryLogic;
        }


        private NuroseMain()
        {
            Instance = this;
        }

        private void DrawFrame()
        {
            if (Window.ClearEnabled)
            {
                Window.RenderTarget.ClearColor = Window.ClearColor;
                Window.RenderTarget.Clear();
            }

            foreach (var logic in logics)
                logic.Render();
        }


        public static void SetWindowFactory<T>() where T : IWindowFactory
        {
            Instance.windowFactory = Activator.CreateInstance(typeof(T)) as IWindowFactory;
            instance.monitor = instance.windowFactory.CreateMonitorInfo();
        }

        public static void SetInputFactory<T>() where T : IInputFactory
        {
            Instance.inputFactory = Activator.CreateInstance(typeof(T)) as IInputFactory;
            instance.monitor = instance.windowFactory.CreateMonitorInfo();
        }


        public static void SetAudioFactory<T>() where T : IAudioFactory
        {
            Instance.audioFactory = Activator.CreateInstance(typeof(T)) as IAudioFactory;
        }


        public static void Create(WindowConstructorArgs args)
        {
            if (WindowFactory == null)
            {
                Logger.LogError("WindowFactory is null", Instance);
            }

            if (AudioFactory == null)
            {
                Logger.LogError("AudioFactory is null", Instance);
            }


            Instance.window = WindowFactory.CreateWindow(args);
            Instance.window.InputHandler = instance.inputFactory.CreateInput();
            instance.audioPlayer = AudioFactory.CreateAudioPlayer();

            if (Window == null)
                Logger.LogError(new NullReferenceException("Window is not set."), Instance);
        }

        public static void Stop()
        {
            Window.Close();
        }

        public static void CatchUp()
        {
            while (TimeLeftToSimulate > 0)
            {
                foreach (var logic in instance.logics)
                    logic.Update();

                TimeLeftToSimulate -= 1f / 60f;
            }
        }

        private static float TimeLeftToSimulate;


        private static void InitCheckAssemblies()
        {
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies()
                .Where(w => w.GetName().FullName.Contains("Nurose", StringComparison.InvariantCulture)).ToList())
            {
                ReflectionSaver.InitCheck(assembly);
            }
        }

        public static Stopwatch FixedUpdateStopwatch { get; set; }
        public static void Start()
        {
            ReflectionSaver.InitCheck(Assembly.GetCallingAssembly());
            InitCheckAssemblies();

            instance.window.Start();

            foreach (var logic in instance.logics)
                logic.Start();

            if (AlternativeMainLoop != null)
                AlternativeMainLoop();
            else
                StandardMainLoop();
        }

        private static void StandardMainLoop()
        {
            FixedUpdateStopwatch = new Stopwatch();
            FixedUpdateStopwatch.Start();
            double t = 0.0;
            double currentTime = FixedUpdateStopwatch.Elapsed.TotalSeconds;
            double FUaccumulator = 0.0;
            double lastDrawTime = 0.0;
            Time.DeltaTime = 0.02f;


            while (Window.IsOpen)
            {
                {
                    double frameStartTime = FixedUpdateStopwatch.Elapsed.TotalSeconds;
                    double newTime = FixedUpdateStopwatch.Elapsed.TotalSeconds;
                    double frameTime = newTime - currentTime;
                    currentTime = newTime;

                    FUaccumulator += frameTime;


                    while (FUaccumulator >= Time.FixedDeltaTime)
                    {
                        Window.ProcessEvents();

                        foreach (var logic in instance.logics)
                            logic.Update();

                        FUaccumulator -= Time.FixedDeltaTime;
                        t += Time.FixedDeltaTime;

                        if (Window.InputRefreshOption == InputRefreshOption.FixedUpdate)
                            Window.InputHandler?.RefreshInput();

                    }

                    Time.SecondsSinceStart = (float)FixedUpdateStopwatch.Elapsed.TotalSeconds;
                    Time.DeltaTime = (float)(FixedUpdateStopwatch.Elapsed.TotalSeconds - lastDrawTime);
                    lastDrawTime = FixedUpdateStopwatch.Elapsed.TotalSeconds;

                    float totalMilliseconds = ((float)FixedUpdateStopwatch.Elapsed.TotalSeconds);
                    Time.InterpolationFactor = (float)FUaccumulator / Time.FixedDeltaTime;

                    if (Window.IsOpen)
                    {
                        instance.DrawFrame();
                        OnRenderStart?.Invoke(null, null);
                        Window.Render();
                        OnRenderEnd?.Invoke(null, null);

                        if (Window.InputRefreshOption == InputRefreshOption.Draw)
                            Window.InputHandler?.RefreshInput();
                    }

                    double frameEndTime = FixedUpdateStopwatch.Elapsed.TotalSeconds;
                    while (Window.TargetFps != default && frameEndTime - frameStartTime < 1f / Window.TargetFps)
                    {
                        frameEndTime = FixedUpdateStopwatch.Elapsed.TotalSeconds;
                    }

                }
            }

            instance = null;
        }
    }
}
﻿namespace Nurose.Core
{
    public static class Time
    {
        public static float DeltaTime { get; set; }
        public static float InterpolationFactor { get; set; }
        public static float TimeScale { get; set; } = 1;
        public static float FixedDeltaTime { get => 1f / FixedUpdatesPerSecond; set => FixedUpdatesPerSecond = 1f / value; }
        public static float FixedUpdatesPerSecond { get; set; } = 30;
        public static float SecondsSinceStart { get; set; }
    }
}
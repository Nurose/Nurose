﻿namespace Nurose.Core
{
    public interface IEntryLogic
    {
        public InputState Input { get; set; }
        public IRenderTarget RenderTarget { get; set; }

        void Start();
        void Update();
        void Render();
    }
}
﻿namespace Nurose.Core
{
    public interface IWindowFactory
    {
        IWindow CreateWindow(WindowConstructorArgs args);
        IMonitor CreateMonitorInfo();
    }
}
﻿using System;
using System.Runtime.CompilerServices;

namespace Nurose.Core
{
    /// <summary>
    /// A way to reference a resource by name. The name string will be internally replaced with a <see cref="ResourceRef{T}"/> after the first <see cref="GetResource(ResourceManager)"/> call.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct NamedResourceRef<T> : IEquatable<NamedResourceRef<T>>
    {
        public string Name;
        private int cachedChecksum;
        private int cachedResourceId;

        public NamedResourceRef(string name)
        {
            Name = name;
            cachedChecksum = -434;
            unchecked
            {
                for (int i = 0; i < name.Length; i++)
                    cachedChecksum += name[i] + name[i] * (11 + i);
            }
            cachedResourceId = -1;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T GetResource(ResourceManager resourceManager)
        {
            if (cachedResourceId == -1)
            {
                cachedResourceId = resourceManager.GetResourceRef<T>(Name).ResourceId;
            }
            return resourceManager.Get<T>(cachedResourceId);
        }

        public static implicit operator NamedResourceRef<T>(string name)
        {
            return new NamedResourceRef<T>(name);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return -853 + cachedChecksum;
            }
        }

        public override bool Equals(object obj)
        {
            return obj is NamedResourceRef<T> @ref &&
                   cachedChecksum == @ref.cachedChecksum;
        }
        public bool Equals(NamedResourceRef<T> other)
        {
            return cachedChecksum == other.cachedChecksum;
        }

        public static bool operator ==(NamedResourceRef<T> left, NamedResourceRef<T> right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(NamedResourceRef<T> left, NamedResourceRef<T> right)
        {
            return !(left == right);
        }
    }
}

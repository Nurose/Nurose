﻿using System;
using System.Numerics;
using System.Runtime.CompilerServices;

namespace Nurose.Core
{
    public static class Utils
    {
        public static Random RandomInstance { get; set; } = new Random();

        public const float Pi = (float)Math.PI;
        public const float Tau = 2 * Pi;
        public const float DegreeToRadian = Pi / 180f;
        public const float RadianToDegree = 180f / Pi;


        public static void SetRandomSeed(int seed)
        {
            RandomInstance = new Random(seed);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Sin(float f)
        {
            return (float)Math.Sin(f);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Cos(float f)
        {
            return (float)Math.Cos(f);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Distance(Vector2 a, Vector2 b)
        {
            Vector2 c = a - b;
            return (float)Math.Sqrt(c.X * c.X + c.Y * c.Y);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float DistanceSqrt(Vector2 a, Vector2 b)
        {
            Vector2 c = a - b;
            return (c.X * c.X + c.Y * c.Y);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Abs(float a)
        {
            return Math.Abs(a);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 RandomPointInUnitCircle()
        {
            float r = Sqrt(RandomFloat());
            return RadianToVector(RandomFloat() * 2 * Pi) * r;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Difference(float a, float b)
        {
            return Math.Abs(a - b);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Atan2(float a, float b)
        {
            return (float)Math.Atan2(a, b);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Remainder(float a)
        {
            return a - Floor(a);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Floor(float a)
        {
            return (float)Math.Floor(a);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 Floor(Vector2 v)
        {
            return new Vector2(Floor(v.X), Floor(v.Y));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 Round(Vector2 v)
        {
            return new Vector2(Round(v.X), Round(v.Y));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Round(float f)
        {
            return MathF.Round(f);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Dot(Vector2 a, Vector2 b)
        {
            return a.X * b.X + a.Y * b.Y;
        }

        public static Vector2 LerpAngle(Vector2 a, Vector2 b, float c)
        {
            return DegreeToVector(LerpAngleDegrees(VectorToDegree(a), VectorToDegree(b), c));
        }

        public static float LerpAngleDegrees(float a, float b, float c)
        {
            float result;
            float diff = b - a;
            if (diff < -180f)
            {
                b += 360f;
                result = Lerp(a, b, c);
                if (result >= 360f)
                {
                    result -= 360f;
                }
            }
            else if (diff > 180f)
            {
                b -= 360f;
                result = Lerp(a, b, c);
                if (result < 0f)
                {
                    result += 360f;
                }
            }
            else
            {
                result = Lerp(a, b, c);
            }
            return result;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Sigmoid(double value)
        {
            return 1.0f / (1.0f + (float)Math.Exp(-value));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int MinMax(int min, int max, int value)
        {
            return Math.Min(Math.Max(min, value), max);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 MinMax(Vector2 min, Vector2 max, Vector2 value)
        {
            float x = Math.Min(Math.Max(min.X, value.X), max.X);
            float y = Math.Min(Math.Max(min.Y, value.Y), max.Y);
            return new Vector2(x, y);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Min(float v1, float v2)
        {
            return Math.Min(v1, v2);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Max(float v1, float v2)
        {
            return Math.Max(v1, v2);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 Multiply(Vector2 a, Vector2 b)
        {
            return new Vector2(a.X * b.X, a.Y * b.Y);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 Divide(Vector2 a, Vector2 b)
        {
            return new Vector2(a.X / b.X, a.Y / b.Y);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float MinMax(float min, float max, float value)
        {
            return Math.Min(Math.Max(min, value), max);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Sqrt(float f)
        {
            return (float)Math.Sqrt(f);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 RadianToVector(float f)
        {
            return new Vector2(Cos(f), Sin(f));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float VectorToDegree(Vector2 vector)
        {
            return Atan2(vector.Y, vector.X) * RadianToDegree;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 DegreeToVector(float f)
        {
            float r = f * DegreeToRadian;
            return new Vector2(Cos(r), Sin(r));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float LerpClamped(float a, float b, float c, float cMin = 0, float cMax = 1)
        {
            return Lerp(a, b, MinMax(cMin, cMax, c));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Color LerpClamped(Color a, Color b, float c, float cMin = 0, float cMax = 1)
        {
            return Lerp(a, b, MinMax(cMin, cMax, c));
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 LerpClamped(Vector2 a, Vector2 b, float c, float cMin = 0, float cMax = 1)
        {
            return Lerp(a, b, MinMax(cMin, cMax, c));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 LerpClamped(Vector3 a, Vector3 b, float c, float cMin = 0, float cMax = 1)
        {
            return Lerp(a, b, MinMax(cMin, cMax, c));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T Lerp<T>(T a, T b, float c) where T : IAdditionOperators<T, T, T>, IMultiplyOperators<T, float, T>
        {
            return a * (1.0f - c) + b * c;
        }

/*
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Lerp(float a, float b, float c)
        {
            return a * (1.0f - c) + b * c;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 Lerp(Vector2 a, Vector2 b, float c)
        {
            return a * (1.0f - c) + b * c;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 Lerp(Vector3 a, Vector3 b, float c)
        {
            return a * (1.0f - c) + b * c;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Color Lerp(Color a, Color b, float c)
        {
            var cR = Lerp(a.R, b.R, c);
            var cG = Lerp(a.G, b.G, c);
            var cB = Lerp(a.B, b.B, c);
            var cA = Lerp(a.A, b.A, c);
            return new Color(cR, cG, cB, cA);
        }*/

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float RandomFloat(float min, float max)
        {
            return Lerp(min, max, (float)RandomInstance.NextDouble());
        }

        /// <summary>
        /// Default range >= -1.0f < 1.0f;
        /// </summary>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float RandomFloat()
        {
            return (float)RandomInstance.NextDouble();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int RandomInt(int min, int max)
        {
            return RandomInstance.Next(min, max);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool RandomBool()
        {
            return RandomFloat() > 0.5f;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool RandomBool(float chance)
        {
            return RandomFloat() > MinMax(0, 1, 1 - chance);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 RandomVector2(float min, float max)
        {
            return new Vector2(RandomFloat(min, max), RandomFloat(min, max));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 RandomVector2(Vector2 min, Vector2 max)
        {
            return new Vector2(RandomFloat(min.X, max.X), RandomFloat(min.Y, max.Y));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Color RandomColor(float alpha = 1f)
        {
            return new Color(RandomFloat(), RandomFloat(), RandomFloat(), alpha);
        }

        public static Vector2 RotatePoint(Vector2 point, float degrees, Vector2 pivot = default)
        {
            float radians = degrees * DegreeToRadian;
            Vector2 offsetPoint = point - pivot;
            return pivot + new Vector2(
                Cos(radians) * offsetPoint.X - Sin(radians) * offsetPoint.Y,
                Cos(radians) * offsetPoint.Y + Sin(radians) * offsetPoint.X);
        }

        public static bool IsPointInsideBounds(Vector2Int point, Vector2Int min, Vector2Int max)
        {
            if (point.X < min.X) return false;
            if (point.Y < min.Y) return false;

            if (point.X > max.X) return false;
            if (point.Y > max.Y) return false;

            return true;
        }

        public static bool IsPointInsideBounds(Vector2 point, Vector2 min, Vector2 max)
        {
            if (point.X < min.X) return false;
            if (point.Y < min.Y) return false;

            if (point.X > max.X) return false;
            if (point.Y > max.Y) return false;

            return true;
        }


        public static bool IsPointInsideBounds(Vector2Int point, IntRect rect)
        {
            if (point.X < rect.Left) return false;
            if (point.Y < rect.Top) return false;

            if (point.X > rect.Left + rect.Width) return false;
            if (point.Y > rect.Top + rect.Height) return false;

            return true;
        }

        public static bool IsPointInsideBounds(Vector2 point, FloatRect rect)
        {
            if (point.X < rect.Left) return false;
            if (point.Y < rect.Top) return false;

            if (point.X > rect.Left + rect.Width) return false;
            if (point.Y > rect.Top + rect.Height) return false;

            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Nurose.Core
{
    /// <summary>
    /// This class should hold most if not all reflection code throughout Nurose for optimization purposes.
    /// </summary>
    public static class ReflectionSaver
    {
        private static readonly List<Type> customTypes = new();
        private static readonly HashSet<Assembly> assembliesRegistered = new();
        private static readonly HashSet<Type> CustomTypesHash = new();
        private static readonly List<PropertyInfo> CustomParameters = new();
        private static readonly List<MethodInfo> customMethods = new();
        private static readonly Dictionary<Type, List<MethodInfo>> AllMethodsOfType = new();
        private static readonly Dictionary<Type, IReadOnlyList<Type>> BaseTypesOfType = new();
        private static readonly Dictionary<Type, IReadOnlyList<Type>> InterfacesOfType = new();
        private static readonly Dictionary<MethodInfo, IReadOnlyList<ParameterInfo>> ParametersOfType = new();

        public static IReadOnlyCollection<Type> CustomTypes => customTypes;
        public static IReadOnlyCollection<MethodInfo> CustomMethods => customMethods;

        public static Type GetCustomType(string typeName)
        {
            return customTypes.SingleOrDefault(t => t.Name == typeName);
        }

        private static IEnumerable<MethodInfo> GetMethodsInternal(Type type)
        {
            var methods = customMethods.Where(t => t.DeclaringType == type).ToList();
            methods.AddRange(GetExtensionMethods(type));
            return methods;
        }

        public static List<MethodInfo> GetMethods(Type type)
        {
            InitCheck(type);
            //TODO Fix getmethods. Use the memory tests in Nurose.Tests to validate. 
            AllMethodsOfType.TryGetValue(type, out _);
            //return val;
            return type.GetRuntimeMethods().ToList();
        }

        public static void InitCheck(Type type)
        {
            if (!CustomTypesHash.Contains(type))
            {
                RegisterAssembly(type.Assembly);
            }
        }

        public static void InitCheck(Assembly assembly)
        {
            if (!assembliesRegistered.Contains(assembly))
                RegisterAssembly(assembly);
        }


        public static IReadOnlyList<Type> GetBaseTypes(Type type)
        {
            InitCheck(type);
            BaseTypesOfType.TryGetValue(type, out var baseTypes);
            return baseTypes;
        }

        public static IReadOnlyList<Type> GetInterfaces(Type type)
        {
            InitCheck(type);
            InterfacesOfType.TryGetValue(type, out var interfaces);
            return interfaces;
        }

        private static void RegisterAssembly(Assembly assembly)
        {
            assembliesRegistered.Add(assembly);
            var types = assembly.GetTypes().Where(t => !customTypes.Contains(t)).ToList();
            types = types.SkipWhile(t => t.Name.StartsWith("<>", StringComparison.InvariantCulture)).ToList();
            var parameters = new List<PropertyInfo>();
            var methods = new List<MethodInfo>();
            types.ForEach(t => parameters.AddRange(t.GetProperties().ToList()));
            types.ForEach(t => methods.AddRange(t.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static).ToList()));
            //types.ForEach(t => methods.AddRange(GetExtensionMethods(t)));


            customMethods.AddRange(methods);
            CustomParameters.AddRange(parameters);

            foreach (var type in types)
            {
                List<MethodInfo> ms = new();
                ms.AddRange(GetMethodsInternal(type));
                // ms.AddRange(GetExtensionMethods(type));
                AllMethodsOfType.Add(type, ms);
                //customMethods.AddRange(ms);
            }

            foreach (var type in types)
            {
                BaseTypesOfType.Add(type, FindBaseTypes(type));
                InterfacesOfType.Add(type, type.GetInterfaces());
            }

            foreach (var method in methods)
            {
                if (!ParametersOfType.ContainsKey(method))
                    ParametersOfType.Add(method, method.GetParameters());
            }
            foreach (var type in types)
            {
#if DEBUG
                if (CustomTypesHash.Contains(type))
                {
                    throw new Exception("Reflectionsaver already initilized type " + type.FullName);
                }
#endif
                CustomTypesHash.Add(type);
                customTypes.Add(type);
            }
        }

        private static Type[] FindBaseTypes(Type obj)
        {
            List<Type> baseTypes = new();
            Type baseType = obj.BaseType;
            while (baseType != null)
            {
                baseTypes.Add(baseType);
                baseType = baseType.BaseType;
            }
            return baseTypes.ToArray();
        }


        public static IReadOnlyList<ParameterInfo> GetParameterOfMethodinfo(MethodInfo methodInfo)
        {
            ParametersOfType.TryGetValue(methodInfo, out var parameters);
            return parameters;
        }

        //Source : https://stackoverflow.com/questions/299515/reflection-to-identify-extension-methods
        private static IEnumerable<MethodInfo> GetExtensionMethods(Type tyoe)
        {
            var query = from type in customTypes
                        where type.IsSealed && !type.IsGenericType && !type.IsNested
                        from method in type.GetMethods(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
                        where method.IsDefined(typeof(ExtensionAttribute), false)
                        where method.GetParameters()[0].ParameterType == tyoe
                        select method;
            return query;
        }
    }
}

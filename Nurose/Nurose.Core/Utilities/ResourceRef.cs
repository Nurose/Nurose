﻿using System.Runtime.CompilerServices;

namespace Nurose.Core
{
    public struct ResourceRef<T> : System.IEquatable<ResourceRef<T>>
    {
        public int ResourceId { get; internal set; }

        public ResourceRef(int resourceId)
        {
            ResourceId = resourceId;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T GetResource(ResourceManager resourceManager)
        {
            return resourceManager.Get(this);
        }

        public override bool Equals(object obj)
        {
            return obj is ResourceRef<T> @ref &&
                   ResourceId == @ref.ResourceId;
        }

        public override int GetHashCode()
        {
            return -419350640 + ResourceId.GetHashCode();
        }

        public bool Equals(ResourceRef<T> other)
        {
            return ResourceId == other.ResourceId;
        }

        public static bool operator ==(ResourceRef<T> left, ResourceRef<T> right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(ResourceRef<T> left, ResourceRef<T> right)
        {
            return !(left == right);
        }
    }
}

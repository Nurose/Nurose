﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Nurose.Core
{

    public class ResourceManager
    {
        private Dictionary<int, object> LoadedResources { get; } = new Dictionary<int, object>();
        private Dictionary<string, int> ResourceAliases { get; } = new Dictionary<string, int>();
        private int maxResourceId = 1;

        public ResourceRef<T> GetResourceRef<T>(string name)
        {
            return new ResourceRef<T>() { ResourceId = ResourceAliases[name] };
        }

        public int Count => LoadedResources.Count;

        public ResourceRef<T> GetOrCreateResourceRef<T>(string name)
        {
            if (!Contains(name))
            {
                UnsafeHold<T>(name, default);
            }
            return new ResourceRef<T>() { ResourceId = ResourceAliases[name] };
        }

        public object this[string name]
        {
            get
            {
                return LoadedResources[ResourceAliases[name]];
            }
        }

        public bool TryHold<T>(string name, T resource)
        {
            bool alreadyExists = Contains(name);
            if (!alreadyExists)
            {
                Hold(name, resource);
            }
            return !alreadyExists;
        }

        public string GetAlias<T>(ResourceRef<T> resource)
        {
            return ResourceAliases.SingleOrDefault(s => s.Value == resource.ResourceId).Key;
        }

        public bool Contains(string name)
        {
            return ResourceAliases.ContainsKey(name) && LoadedResources.ContainsKey(ResourceAliases[name]);
        }

        public ResourceRef<T> OverrideHold<T>(string name, T resource)
        {
            if (Contains(name))
            {
                int resourceId = ResourceAliases[name];
                LoadedResources[resourceId] = resource;
                return new ResourceRef<T>(resourceId);
            }
            else
                return UnsafeHold(name, resource);
        }

        public ResourceRef<T> Hold<T>(string name, T resource)
        {
#if DEBUG
            if (Contains(name))
                throw new Exception("Resource already exists");
#endif
            return UnsafeHold(name, resource);
        }

        private ResourceRef<T> UnsafeHold<T>(string name, T resource)
        {
            ResourceAliases.Add(name, maxResourceId);
            LoadedResources.Add(maxResourceId, resource);
            var t = new ResourceRef<T>() { ResourceId = maxResourceId };
            maxResourceId++;
            return t;
        }

        private void Remove(string name)
        {
            LoadedResources.Remove(ResourceAliases[name]);
            ResourceAliases.Remove(name);
        }



        public T Get<T>(string name)
        {
#if DEBUG
            if (!Contains(name))
                Logger.LogError("Resource named: \'" + name + "\' could not be found", this);
#endif

            LoadedResources.TryGetValue(ResourceAliases[name], out object o);
            return (T)o;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T Get<T>(ResourceRef<T> resourceRef)
        {
            return (T)LoadedResources[resourceRef.ResourceId];
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T Get<T>(NamedResourceRef<T> resourceRef)
        {
            return resourceRef.GetResource(this);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T Get<T>(int resourceId)
        {
            return (T)LoadedResources[resourceId];
        }

        public List<object> GetAllResources()
        {
            return LoadedResources.Select(t => t.Value).ToList();
        }

        public List<ResourceRef<T>> GetResourceRefsOfType<T>() where T : class
        {
            return LoadedResources.Where(r => r.Value is T c).Select(r => new ResourceRef<T>(r.Key)).ToList();
        }

        public List<T> GetResourcesOfType<T>() where T : class
        {
            return LoadedResources.Where(r => r.Value is T c).Select(r => r.Value as T).ToList();
        }

    }
}

namespace Nurose.Core
{
    public interface IMonitor
    {
        Vector2Int MonitorSizeInPixels { get; }
        Vector2Int PhysicalSizeInMilimeters { get; }
        float ContentScale { get; }
    }
}
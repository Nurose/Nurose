﻿namespace Nurose.Core
{
    public enum WindowState
    {
        //
        // Summary:
        //     The window is in its normal state.
        Normal,
        //
        // Summary:
        //     The window is minimized to the taskbar (also known as 'iconified').
        Minimized,
        //
        // Summary:
        //     The window covers the whole working area, which includes the desktop but not
        //     the taskbar and/or panels.
        Maximized,
        //
        // Summary:
        //     The window covers the whole screen, including all taskbars and/or panels.
        Fullscreen
    }
}
﻿namespace Nurose.Core
{
    public struct WindowConstructorArgs
    {
        public string Title;
        public Vector2Int Size;
        public WindowBorder WindowBorder;
        public bool StartCentered;
        public int NumberOfAntiAliasSamples;
    }
}
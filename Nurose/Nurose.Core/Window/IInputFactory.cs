﻿namespace Nurose.Core
{
    public interface IInputFactory
    {
        InputHandler CreateInput();
    }
}
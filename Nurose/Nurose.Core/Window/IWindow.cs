﻿using System;

namespace Nurose.Core
{
    public interface IWindow
    {
        IRenderTarget RenderTarget { get; }
        WindowBorder WindowBorder { get; set; }
        InputRefreshOption InputRefreshOption { get; set; }  
        InputState Input { get; set; }
        InputHandler InputHandler { get; set; }
        bool VSync { get; set; }
        bool ClearEnabled { get; set; }
        Color ClearColor { get; set; }
        Vector2Int Size { get; set; }
        int Width { get; }
        int Height { get; }
        Vector2Int Position { get; set; }
        string Title { get; set; }
        bool Resizable { get; set; }
        bool IsOpen { get; }
        bool IsFocused { get; }
        WindowState WindowState { get; set; }
        string GPUVendor { get; }
        uint TargetFps { get; set; }
        public Cursor CursorShape { get; set; }
        public string Clipboard { get; set; }
        void CenterWindow();
        void ProcessEvents();
        void Render();
        void Start();
        void Close();
        Vector2Int MousePosition();
        void Focus();
        public event EventHandler<string[]> OnFileDropped;
    }
}
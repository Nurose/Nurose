﻿namespace Nurose.Core
{
    //
    // Summary:
    //     Mouse wheels
    public enum Wheel
    {
        //
        // Summary:
        //     The vertical mouse wheel
        VerticalWheel = 0,
        //
        // Summary:
        //     The horizontal mouse wheel
        HorizontalWheel = 1
    }
}

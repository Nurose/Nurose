﻿namespace Nurose.Core
{
    public enum InputRefreshOption
    {
        FixedUpdate,
        Draw,
        None
    }
}
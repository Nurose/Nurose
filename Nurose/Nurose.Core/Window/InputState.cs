﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Nurose.Core
{
    /// <summary>
    /// Represents the mouse and keyboard state of a single frame
    /// </summary>
    public class InputState : IEquatable<InputState>
    {
        public Vector2Int MonitorSize;
        public Vector2Int WindowSize;
        public Vector2Int MousePosition;
        public Vector2Int PreviousMousePosition;
        public sbyte JoystickMoveX;
        public sbyte JoystickMoveY;
        public sbyte JoystickLookX;
        public sbyte JoystickLookY;
        public int MouseWheelDelta;
        public string CharInputs = "";

        public List<MouseButton> ButtonsHeld = new();
        public List<MouseButton> ButtonsPressed = new();
        public List<MouseButton> ButtonsReleased = new();
        public List<Key> KeysPressed = new();
        public List<Key> KeysReleased = new();
        public List<Key> KeysHeld = new();
        public List<int> ControllerPressed = new();



        public bool IsKeyHeld(Key key) => KeysHeld.Contains(key);
        public bool IsKeyPressed(Key key) => KeysPressed.Contains(key);
        public bool IsButtonReleased(MouseButton button) => ButtonsReleased.Contains(button);
        public bool IsButtonHeld(MouseButton button) => ButtonsHeld.Contains(button);
        public bool IsButtonPressed(MouseButton button) => ButtonsPressed.Contains(button);
        public bool IsKeyReleased(Key key) => KeysReleased.Contains(key);

        public bool AnyKeyHeld => KeysHeld.Any();
        public bool AnyKeyPressed => KeysPressed.Any();
        public bool AnyKeyReleased => KeysReleased.Any();

        public Key? PressedKey => AnyKeyPressed ? new Key?(KeysPressed.First()) : null;
        public Key? HeldKey => AnyKeyHeld ? new Key?(KeysHeld.First()) : null;
        public Key? ReleasedKey => AnyKeyReleased ? new Key?(KeysReleased.First()) : null;

        public bool AnyButtonHeld => ButtonsHeld.Any();
        public bool AnyButtonPressed => ButtonsPressed.Any();
        public bool AnyButtonReleased => ButtonsReleased.Any();

        public MouseButton? PressedButton => AnyButtonPressed ? new MouseButton?(ButtonsPressed.First()) : null;
        public MouseButton? HeldButton => AnyButtonHeld ? new MouseButton?(ButtonsHeld.First()) : null;
        public MouseButton? ReleasedButton => AnyButtonReleased ? new MouseButton?(ButtonsReleased.First()) : null;

        public void RefreshInput()
        {
            KeysPressed.Clear();
            KeysReleased.Clear();
            ButtonsReleased.Clear();
            ButtonsPressed.Clear();
            MouseWheelDelta = 0;
            PreviousMousePosition = MousePosition;
            CharInputs = "";
        }

        public InputState Clone()
        {
            return new InputState()
            {
                MonitorSize = this.MonitorSize,
                WindowSize = this.WindowSize,
                PreviousMousePosition = this.PreviousMousePosition,
                MousePosition = this.MousePosition,
                MouseWheelDelta = this.MouseWheelDelta,
                CharInputs = this.CharInputs,
                ButtonsHeld = new List<MouseButton>(this.ButtonsHeld),
                ButtonsPressed = new List<MouseButton>(this.ButtonsPressed),
                ButtonsReleased = new List<MouseButton>(this.ButtonsReleased),
                KeysPressed = new List<Key>(this.KeysPressed),
                KeysReleased = new List<Key>(this.KeysReleased),
                KeysHeld = new List<Key>(this.KeysHeld),
                JoystickMoveX = this.JoystickMoveX,
                JoystickMoveY = this.JoystickMoveY,
                JoystickLookX = this.JoystickLookX,
                JoystickLookY = this.JoystickLookY,
                ControllerPressed = this.ControllerPressed,
            };
        }



        public override int GetHashCode()
        {
            System.HashCode hash = new();
            hash.Add(MonitorSize);
            hash.Add(WindowSize);
            hash.Add(PreviousMousePosition);
            hash.Add(CharInputs);
            hash.Add(MousePosition);
            hash.Add(MouseWheelDelta);
            hash.Add(ButtonsHeld);
            hash.Add(ButtonsPressed);
            hash.Add(ButtonsReleased);
            hash.Add(KeysPressed);
            hash.Add(KeysReleased);
            hash.Add(KeysHeld);
            return hash.ToHashCode();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as InputState);
        }

        private bool ListEquals<T>(List<T> x, List<T> y)
        {
            if (x.Count != y.Count)
                return false;

            for (int i = 0; i < x.Count; i++)
            {
                if (!x[i].Equals(y[i]))
                    return false;
            }
            return true;
        }

        public bool Equals(InputState other)
        {
            return other is not null &&
                   MonitorSize.Equals(other.MonitorSize) &&
                   WindowSize.Equals(other.WindowSize) &&
                   PreviousMousePosition.Equals(other.PreviousMousePosition) &&
                   MousePosition.Equals(other.MousePosition) &&
                   JoystickMoveX == other.JoystickMoveX &&
                   JoystickMoveY == other.JoystickMoveY &&
                   JoystickLookX == other.JoystickLookX &&
                   JoystickLookY == other.JoystickLookY &&
                   MouseWheelDelta == other.MouseWheelDelta &&
                   CharInputs == other.CharInputs &&
                   ListEquals(ButtonsHeld, other.ButtonsHeld) &&
                   ListEquals(ButtonsPressed, other.ButtonsPressed) &&
                   ListEquals(ButtonsReleased, other.ButtonsReleased) &&
                  ListEquals(KeysPressed, other.KeysPressed) &&
                   ListEquals(KeysReleased, other.KeysReleased) &&
                  ListEquals(KeysHeld, other.KeysHeld) &&
                   ListEquals(ControllerPressed, other.ControllerPressed);
        }
    }
}
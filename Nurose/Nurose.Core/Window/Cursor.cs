﻿namespace Nurose.Core
{
    public enum Cursor
    {
        //
        // Summary:
        //     The standard arrow shape. Used in almost all situations.
        Arrow = 221185,
        //
        // Summary:
        //     The I-Beam shape. Used when mousing over a place where text can be entered.
        IBeam = 221186,
        //
        // Summary:
        //     The crosshair shape. Used when dragging and dropping.
        Crosshair = 221187,
        //
        // Summary:
        //     The hand shape. Used when mousing over something that can be dragged around.
        Hand = 221188,
        //
        // Summary:
        //     The horizontal resize shape. Used when mousing over something that can be horizontally
        //     resized.
        HResize = 221189,
        //
        // Summary:
        //     The vertical resize shape. Used when mousing over something that can be vertically
        //     resized.
        VResize = 221190
    }
}
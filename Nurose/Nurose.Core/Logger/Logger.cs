﻿using System;
using System.Collections.Generic;

namespace Nurose.Core
{
    public static class Logger
    {
        private static readonly List<LogMessage> messages = new();

        public static event EventHandler<LogMessageEventArgs> OnNewMessage;

        private static void Log(LogMessage logMessage)
        {
            messages.Add(logMessage);
            OnNewMessage?.Invoke(null, new LogMessageEventArgs(logMessage));
        }

        public static void LogTemp(object message, object sender = null)
        {
            Log(new LogMessage(LogLevel.Temp, message.ToString(), sender));
        }

#if (DEBUG)
        public static void LogDebug(object message, object sender = null)
        {
            Log(new LogMessage(LogLevel.Debug, message.ToString(), sender));
        }
#else
        public static void LogDebug(object message , object sender=null)
        {
            Log(new LogMessage(LogLevel.Debug, message.ToString(), sender));
        }
#endif

        public static void LogWarning(object message, object sender = null)
        {
            Log(new LogMessage(LogLevel.Warn, message.ToString(), sender));
        }

        public static void LogAnnouncement(object message, object sender = null)
        {
            Log(new LogMessage(LogLevel.Info, message.ToString(), sender));
        }

        public static void ClearMessages()
        {
            messages.Clear();
        }

        public static IEnumerable<LogMessage> GetMessages()
        {
            return messages;
        }

        public static void LogError(Exception exception, object sender = null)
        {
            Log(new LogMessage(exception, sender));
            throw exception;
        }

        public static void LogError(object exception, object sender = null)
        {
            Log(new LogMessage(LogLevel.Error, exception.ToString(), sender));
            throw new Exception($"message={exception}  [sender={sender}]");
        }
    }
}

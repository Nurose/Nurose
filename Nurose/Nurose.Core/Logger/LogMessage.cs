﻿using System;

namespace Nurose.Core
{
    public struct LogMessage
    {
        public DateTime DateTime { get; }
        public LogLevel MessageType { get; }
        public string Description { get; }
        public object Sender { get; }
        public Exception Exception { get; }

        public LogMessage(LogLevel messageType, string description, object sender)
        {
            DateTime = DateTime.Now;
            MessageType = messageType;
            Description = description;
            Sender = sender;
            Exception = null;
        }

        public LogMessage(Exception exception, object sender)
        {
            DateTime = DateTime.Now;
            MessageType = LogLevel.Error;
            Description = exception.Message;
            Sender = sender;
            Exception = exception;
        }

        public override string ToString()
        {
            return $"[{DateTime.ToLongTimeString()} {MessageType.ToString().ToUpperInvariant().Replace("DEBUG","DEBG")}]: {Description}";
        }
    }
}

﻿using System;
using System.Globalization;

namespace Nurose.Core
{
    public class ConsoleLogger
    {
        private static ConsoleColor GetLogLevelColor(LogLevel logLevel)
        {
            switch (logLevel)
            {
                case LogLevel.Debug: return ConsoleColor.Blue;
                case LogLevel.Info: return ConsoleColor.Green;
                case LogLevel.Warn: return ConsoleColor.Magenta;
                case LogLevel.Error: return ConsoleColor.Red;
                default: return ConsoleColor.Blue;

            }
        }

        private void NewMessage(object sender, LogMessageEventArgs args)
        {
            Console.ForegroundColor = GetLogLevelColor(args.Message.MessageType);
            Console.Write("[" + args.Message.MessageType.ToString().ToUpper(CultureInfo.InvariantCulture) + "] ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(args.Message.Description);

            Console.ForegroundColor = ConsoleColor.Gray;
            if (args.Message.Sender != null)
            {
                Console.Write("  [" + args.Message.Sender.GetType().Name + "{" + args.Message.Sender.GetHashCode() + "}] ");
            }
            Console.Write("\n");
        }

    }
}

﻿using System;

namespace Nurose.Core
{
    public class LogMessageEventArgs : EventArgs
    {
        public LogMessageEventArgs(LogMessage message)
        {
            Message = message;
        }

        public LogMessage Message { get; }
    }
}

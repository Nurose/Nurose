﻿namespace Nurose.Core
{
    public enum LogLevel
    {
        Temp,
        Debug,
        Info,
        Warn,
        Error
    }
}

﻿using System;

namespace Nurose.Core
{
    /// <summary>
    /// FloatRect starts from LeftTop as origin. So <see cref="Right"/> > <see cref="Left"/> and  <see cref="Bottom"/> > <see cref="Top"/>
    /// </summary>
    public struct FloatRect : IEquatable<FloatRect>
    {
        public float Left { get; set; }
        public float Top { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }

        /// <summary>
        ///  <see cref="Right"/> = <see cref="Left"/> + <see cref="Width"/>
        /// </summary>
        public float Right { get => Left + Width; set => Width = value - Left; }
        /// <summary>
        ///  <see cref="Bottom"/> = <see cref="Top"/> + <see cref="Height"/>
        /// </summary>
        public float Bottom { get => Top + Height; set => Height = value - Top; }


        public FloatRect(float left, float top, float width, float height) : this()
        {
            Left = left;
            Top = top;
            Width = width;
            Height = height;
        }

        public FloatRect(Vector2 v, Vector2 size)
        {
            Left = v.X;
            Top = v.Y;
            Width = size.X;
            Height = size.Y;
        }

        public FloatRect Crop(FloatRect cropRect)
        {
            float left = Math.Max(Left, cropRect.Left);
            float right = Math.Min(Right, cropRect.Right);
            float top = Math.Max(Top, cropRect.Top);
            float bottom = Math.Min(Bottom, cropRect.Bottom);
            return new FloatRect(left, top, right - left, bottom - top);
        }

        public FloatRect Translate(Vector2 delta)
        {
            return new FloatRect(Left + delta.X, Top + delta.Y, Width, Height);
        }

        public bool Contains(Vector2 v)
        {
            return v.X > Left && v.X < Right && v.Y > Top && v.Y < Bottom;
        }

        public float Area()
        {
            return Width * Height;
        }

        public Vector2 LeftTop => new(Left, Top);
        public Vector2 RightTop => new(Right, Top);
        public Vector2 RightBot => new(Right, Bottom);
        public Vector2 LeftBot => new(Left, Bottom);

        public Vector2 Size => new(Width, Height);

        public static bool operator ==(FloatRect r1, FloatRect r2)
        {
            return r1.Left == r2.Left && r1.Top == r2.Top
                && r1.Width == r2.Width && r1.Height == r2.Height;
        }

        public static bool operator !=(FloatRect r1, FloatRect r2)
        {
            return !(r1 == r2);
        }

        public bool Equals(FloatRect other)
        {
            return this == other;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}

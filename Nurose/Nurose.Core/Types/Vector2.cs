﻿using System;
using System.Numerics;
using System.Runtime.CompilerServices;

namespace Nurose.Core
{
    [Serializable]
    public struct Vector2 :
        IEquatable<Vector2>,
        IMultiplyOperators<Vector2, float, Vector2>,
        IAdditionOperators<Vector2, Vector2, Vector2>,
        IEqualityOperators<Vector2, Vector2, bool>
    {
#pragma warning disable CA1051 // Do not declare visible instance fields
        public float X;
        public float Y;
#pragma warning restore CA1051 // Do not declare visible instance fields
        public static int SizeInBytes { get; } = 16;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Vector2(float x, float y)
        {
            X = x;
            Y = y;
        }

        public static readonly Vector2 Zero = new(0, 0);
        public static readonly Vector2 One = new(1, 1);
        public static readonly Vector2 Right = new(1, 0);
        public static readonly Vector2 Left = new(-1, 0);
        public static readonly Vector2 Up = new(0, 1);
        public static readonly Vector2 Down = new(0, -1);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 operator +(Vector2 v1, Vector2 v2) => new(v1.X + v2.X, v1.Y + v2.Y);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 operator -(Vector2 v1, Vector2 v2) => new(v1.X - v2.X, v1.Y - v2.Y);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 operator *(Vector2 v1, Vector2 v2) => new(v1.X * v2.X, v1.Y * v2.Y);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 operator /(Vector2 v1, Vector2 v2) => new(v1.X / v2.X, v1.Y / v2.Y);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 operator -(Vector2 v1) => new(-v1.X, -v1.Y);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Dot(Vector2 v1, Vector2 v2)
        {
            return v1.X * v2.X + v1.Y * v2.Y;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 operator *(Vector2 v1, float f) => new(v1.X * f, v1.Y * f);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 operator /(Vector2 v1, float f) => new(v1.X / f, v1.Y / f);

        public static Vector2 operator *(float f, Vector2 v1) => new(f * v1.X, f * v1.Y);


        public static Vector2 operator /(float f, Vector2 v1) => new(f / v1.X, f / v1.Y);

        public static explicit operator Vector2(Vector3 v) => new(v.X, v.Y);


        public static implicit operator Vector2((float, float) tuple) => new(tuple.Item1, tuple.Item2);

        public Vector2Int ToVector2Int()
        {
            return new Vector2Int((int)MathF.Floor(X), (int)MathF.Floor(Y));
        }

        public static bool operator ==(Vector2 v1, Vector2 v2) => v1.X == v2.X && v1.Y == v2.Y;
        public static bool operator !=(Vector2 v1, Vector2 v2) => !(v1 == v2);


        public float Magnitude => (float)Math.Sqrt(X * X + Y * Y);
        public float Area => X * Y;
        public float SquaredMagnitude => X * X + Y * Y;

        public Vector2 Normalized
        {
            get
            {
                var mag = Magnitude;
                if (mag == 0)
                {
                    return Zero;
                }
                return this / Magnitude;
            }
        }


        public bool IsBetweenRect(Vector2 a, Vector2 b)
        {
            return X > a.X && X < b.X && Y > a.Y && Y < b.Y;
        }

        public float Distance(Vector2 b)
        {
            Vector2 c = this - b;
            return (float)Math.Sqrt(c.X * c.X + c.Y * c.Y);
        }

        public static float DistanceSquared(Vector2 a, Vector2 b)
        {
            Vector2 c = a - b;
            return c.X * c.X + c.Y * c.Y;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public bool Equals(Vector2 other)
        {
            return this == other;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override string ToString()
        {
            return $"({X}, {Y})";
        }

        public static Vector2 op_CheckedMultiply(Vector2 left, float right)
        {
            return left * Right;
        }

        public static Vector2 op_CheckedAddition(Vector2 left, Vector2 right)
        {
            return left + right;
        }
    }
}

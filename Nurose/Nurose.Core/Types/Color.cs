﻿using System;
using System.Globalization;
using System.Numerics;
using System.Runtime.CompilerServices;

namespace Nurose.Core
{
    [Serializable]
    public struct Color : IEquatable<Color>, IMultiplyOperators<Color, float, Color>, IAdditionOperators<Color, Color, Color>
    {
        public float R { get; set; }
        public float G { get; set; }
        public float B { get; set; }
        public float A { get; set; }

        /// <summary>
        /// Color structure, with values ranging from 0.0 to 1.0f
        /// </summary>
        /// <param name="r"></param>
        /// <param name="g"></param>
        /// <param name="b"></param>
        /// <param name="a"></param>
        public Color(float r, float g, float b, float a = 1f)
        {
            R = r;
            G = g;
            B = b;
            A = a;
        }

        public static readonly Color Transparent = new(0, 0, 0, 0);
        public static readonly Color Orange = new(1, .61f, 0);
        public static readonly Color Black = new(0, 0, 0);
        public static readonly Color White = new(1, 1, 1);
        public static readonly Color Red = new(1, 0, 0);
        public static readonly Color Green = new(0, 1, 0);
        public static readonly Color Blue = new(0, 0, 1);
        public static readonly Color Cyan = new(0, 1, 1);
        public static readonly Color Yellow = new(1, 1, 0);
        public static readonly Color Magenta = new(1, 0, 1);

        public static Color Grey(float whiteness) => new(whiteness, whiteness, whiteness, 1);

        public Color WithAlpha(float a) => new(R, G, B, a);

        public static Color operator *(Color left, float right)
        {
            return new Color(left.R * right, left.G * right, left.B * right, left.A * right);
        }

        public static Color operator /(Color left, float right)
        {
            return new Color(left.R / right, left.G / right, left.B / right, left.A / right);
        }

        public static Color operator *(float left, Color right)
        {
            return new Color(right.R * left, right.G * left, right.B * left, right.A * left);
        }

        public static Color operator /(float left, Color right)
        {
            return new Color(right.R / left, right.G / left, right.B / left, right.A / left);
        }

        //
        public static Color operator *(Color left, Color right)
        {
            return new Color(left.R * right.R, left.G * right.G, left.B * right.B, left.A * right.A);
        }

        public static Color operator /(Color left, Color right)
        {
            return new Color(left.R / right.R, left.G / right.G, left.B / right.B, left.A / right.A);
        }

        public static Color operator +(Color left, Color right)
        {
            return new Color(left.R + right.R, left.G + right.G, left.B + right.B, left.A + right.A);
        }

        public static Color operator -(Color left, Color right)
        {
            return new Color(left.R - right.R, left.G - right.G, left.B - right.B, left.A - right.A);
        }
        //

        public static bool operator ==(Color left, Color right)
        {
            return left.R == right.R && left.G == right.G && left.B == right.B && left.A == right.A;
        }

        public static bool operator !=(Color left, Color right)
        {
            return !(left == right);
        }

        public override string ToString()
        {
            return $"({R},{G},{B},{A})";
        }

        public string ToHexString()
        {
            ToBytes(out byte r, out byte g, out byte b, out byte a);
            return $"#({r:X2}{g:X2}{b:X2})";
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Color))
            {
                return false;
            }

            var color = (Color)obj;
            return Equals(color);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Color FromUint(uint color)
        {
            float r = color & 0xFF;
            float g = color >> 8 & 0xFF;
            float b = color >> 16 & 0xFF;
            float a = color >> 24 & 0xFF;
            return new Color(r / 255, g / 255, b / 255, a / 255);
        }

        public uint ToUint()
        {
            return (uint)(((int)(A * 255) << 24) | ((int)(R * 255) << 16) |
                               ((int)(G * 255) << 8) | ((int)(B * 255) << 0));
        }

        public static Color FromBytes(byte r, byte g, byte b, byte a = 255)
        {
            return new Color(r / 255f, g / 255f, b / 255f, a / 255f);
        }


        public static Color FromHexString(string hex)
        {
            var r = GetFloat(hex, 0);
            var g = GetFloat(hex, 2);
            var b = GetFloat(hex, 4);
            var a = 1;

            return new Color(r, g, b, a);

            static float GetFloat(string hex, int i)
            {
                return Convert.ToInt32(hex.Substring(i, 2), 16) / 255f;
            }
        }

        public static Color FromHSL(float h, float s, float l, float alpha = 1)
        {
            float p2;
            if (l <= 0.5f) p2 = l * (1 + s);
            else p2 = l + s - l * s;

            float p1 = 2 * l - p2;
            float double_r, double_g, double_b;
            if (s == 0)
            {
                double_r = l;
                double_g = l;
                double_b = l;
            }
            else
            {
                double_r = QqhToRgb(p1, p2, h + 120);
                double_g = QqhToRgb(p1, p2, h);
                double_b = QqhToRgb(p1, p2, h - 120);
            }

            return new Color(double_r, double_g, double_b, alpha);
        }

        private static float QqhToRgb(float q1, float q2, float hue)
        {
            if (hue > 360) hue -= 360;
            else if (hue < 0) hue += 360;

            if (hue < 60) return q1 + (q2 - q1) * hue / 60;
            if (hue < 180) return q2;
            if (hue < 240) return q1 + (q2 - q1) * (240 - hue) / 60;
            return q1;
        }

        //source: http://csharphelper.com/blog/2016/08/convert-between-rgb-and-hls-color-models-in-c/
        public (float h, float s, float l) ToHSL()
        {
            // Get the maximum and minimum RGB components.
            float max = R;
            if (max < G) max = G;
            if (max < B) max = B;

            float min = R;
            if (min > G) min = G;
            if (min > B) min = B;

            float diff = max - min;
            var l = (max + min) / 2;
            var s = 0f;
            var h = 0f;
            if (Math.Abs(diff) >= 0.00001f)
            {
                if (l <= 0.5f) s = diff / (max + min);
                else s = diff / (2 - max - min);

                var r_dist = (max - R) / diff;
                var g_dist = (max - G) / diff;
                var b_dist = (max - B) / diff;

                if (R == max) h = b_dist - g_dist;
                else if (G == max) h = 2 + r_dist - b_dist;
                else h = 4 + g_dist - r_dist;

                h = h * 60;
                if (h < 0) h += 360;
            }

            return (h, s, l);
        }

        public void ToBytes(out byte r, out byte g, out byte b, out byte a)
        {
            r = (byte)((int)(R * 255));
            g = (byte)((int)(G * 255));
            b = (byte)((int)(B * 255));
            a = (byte)((int)(A * 255));
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(R, G, B, A);
        }

        public bool Equals(Color color)
        {
            return R == color.R &&
                   G == color.G &&
                   B == color.B &&
                   A == color.A;
        }

        public static Color op_CheckedMultiply(Color left, float right)
        {
            return left * right;
        }

        public static Color op_CheckedAddition(Color left, Color right)
        {
            return left + right;
        }

        public static Color Parse(string v)
        {
            var parts = v[1..^1].Split(',');
            float r = float.Parse(parts[0], CultureInfo.InvariantCulture);
            float g = float.Parse(parts[1], CultureInfo.InvariantCulture);
            float b = float.Parse(parts[2], CultureInfo.InvariantCulture);
            float a = 1;
            if (parts.Length == 4)
                a = float.Parse(parts[3], CultureInfo.InvariantCulture);

            return new Color(r, g, b, a);
        }

        public Vector3 ToRGBVector3()
        {
            return new Vector3(R, G, B);
        }
    }
}

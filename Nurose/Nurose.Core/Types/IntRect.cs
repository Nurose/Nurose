﻿using System;

namespace Nurose.Core
{
    public struct IntRect : IEquatable<IntRect>
    {
        public int Left { get; set; }
        public int Top { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public int Right { get => Left + Width; set => Width = value - Left; }
        public int Bottom { get => Top + Height; set => Height = value - Top; }


        public IntRect(int left, int top, int widht, int height) : this()
        {
            Left = left;
            Top = top;
            Width = widht;
            Height = height;
        }

        public IntRect(Vector2 v, Vector2 size)
        {
            Left = (int)v.X;
            Top = (int)v.Y;
            Width = (int)size.X;
            Height = (int)size.Y;
        }

        public IntRect Crop(IntRect cropRect)
        {
            int left = Math.Max(Left, cropRect.Left);
            int right = Math.Min(Right, cropRect.Right);
            int top = Math.Max(Top, cropRect.Top);
            int bottom = Math.Min(Bottom, cropRect.Bottom);
            return new IntRect(left, top, right - left, bottom - top);
        }

        public bool Contains(Vector2 v)
        {
            return v.X > Left && v.X < Right && v.Y > Top && v.Y < Bottom;
        }


        public int AreaSquared()
        {
            return Width * Width + Height * Height;
        }

        public int Area()
        {
            return (int)Math.Sqrt(AreaSquared());
        }

        public static bool operator ==(IntRect r1, IntRect r2)
        {
            return r1.Left == r2.Left && r1.Top == r2.Top
                && r1.Width == r2.Width && r1.Height == r2.Height;
        }
        public static bool operator !=(IntRect r1, IntRect r2)
        {
            return !(r1 == r2);
        }


        public bool Equals(IntRect other)
        {
            return this == other;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}

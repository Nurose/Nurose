﻿using System;
using System.Runtime.CompilerServices;

namespace Nurose.Core
{
    [Serializable]
    public struct Vertex
    {
        public Vector2 Position;
        public float padding;
        public Vector2 TexCoords;
        public Color Color;
        public const int SizeInBytes = 8 + 12 + 16;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Vertex(Vector2 position, Color color)
        {
            Position = position;
            padding = 0;
            Color = color;
            TexCoords = Vector2.Zero;
        }
        /*
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Vertex(Vector3 position, Color color)
        {
            Position = position;
            Color = color;
            TexCoords = Vector2.Zero;
        }
        */
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Vertex(Vector2 position, Color color, Vector2 texCords) : this(position, color)
        {
            TexCoords = texCords;
        }
        /*
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Vertex(Vector3 position, Color color, Vector2 texCords) : this(position, color)
        {
            TexCoords = texCords;
        }
        */
    }
}

﻿using System;
using System.Numerics;

namespace Nurose.Core
{
    public struct Vector2Int : 
        IEquatable<Vector2Int>,
        IAdditionOperators<Vector2Int, Vector2Int, Vector2Int>,
        IEqualityOperators<Vector2Int, Vector2Int, bool>

    {
        public int X;
        public int Y;

        public static readonly Vector2Int Zero = new(0, 0);
        public static readonly Vector2Int One = new(1, 1);
        public static readonly Vector2Int Right = new(1, 0);
        public static readonly Vector2Int Left = new(-1, 0);
        public static readonly Vector2Int Up = new(0, 1);
        public static readonly Vector2Int Down = new(0, -1);


        public Vector2Int(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override bool Equals(object obj)
        {
            return obj is Vector2Int && Equals((Vector2Int)obj);
        }


        public static bool operator ==(Vector2Int left, Vector2Int right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Vector2Int left, Vector2Int right)
        {
            return !(left == right);
        }

        public static Vector2Int operator +(Vector2Int v1, Vector2Int v2) => new(v1.X + v2.X, v1.Y + v2.Y);
        public static Vector2Int operator -(Vector2Int v1, Vector2Int v2) => new(v1.X - v2.X, v1.Y - v2.Y);
        public static Vector2Int operator *(Vector2Int v1, Vector2Int v2) => new(v1.X * v2.X, v1.Y * v2.Y);
        public static Vector2Int operator /(Vector2Int v1, Vector2Int v2) => new(v1.X / v2.X, v1.Y / v2.Y);

        public static Vector2Int operator /(Vector2Int v1, int i) => new(v1.X / i, v1.Y / i);
        public static Vector2Int operator *(Vector2Int v1, int i) => new(v1.X * i, v1.Y * i);
        public static Vector2Int operator -(Vector2Int v1) => new(-v1.X, -v1.Y);


        public static implicit operator Vector2(Vector2Int vector2Int) => new(vector2Int.X, vector2Int.Y);

        public static implicit operator Vector2Int((int, int) tuple) => new(tuple.Item1, tuple.Item2);

        public override int GetHashCode()
        {
            return HashCode.Combine(X, Y);
        }

        public bool Equals(Vector2Int other)
        {
            return X.Equals(other.X) && Y.Equals(other.Y);
        }


        public bool IsBetweenRect(Vector2Int rectBegin, Vector2Int rectEnd)
        {
            return X > rectBegin.X && X < rectEnd.X && Y > rectBegin.Y && Y < rectEnd.Y;
        }


        public static int DistanceSquared(Vector2Int a, Vector2Int b)
        {
            var c = a - b;
            return c.X * c.X + c.Y * c.Y;
        }

        public override string ToString()
        {
            return $"({X}, {Y})";
        }

        public Vector2 ToVector2()
        {
            return new Vector2(X, Y);
        }

        public static explicit operator Vector2Int(Vector2 v)
        {
            return new Vector2Int((int)v.X, (int)v.Y);
        }
    }
}

﻿using System;
using System.Numerics;
using System.Runtime.CompilerServices;

namespace Nurose.Core
{
    [Serializable]
    public struct Vector3 : 
        IEquatable<Vector3>,
        IMultiplyOperators<Vector3, float, Vector3>,
        IAdditionOperators<Vector3, Vector3, Vector3>,
        IEqualityOperators<Vector3, Vector3, bool>
    {
        public float X;
        public float Y;
        public float Z;

        public static int SizeInBytes { get; } = 12;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Vector3(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Vector3(Vector2 v, float z = 0)
        {
            X = v.X;
            Y = v.Y;
            Z = z;
        }

        public static bool operator ==(Vector3 v1, Vector3 v2)
        {
            return v1.X == v2.X && v1.Y == v2.Y;
        }
        public static bool operator !=(Vector3 v1, Vector3 v2)
        {
            return !(v1 == v2);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 operator -(Vector3 v1, Vector3 v2) => new(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 operator +(Vector3 v1, Vector3 v2) => new(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 operator *(Vector3 v1, Vector3 v2) => new(v1.X * v2.X, v1.Y * v2.Y, v1.Z * v2.Z);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 operator /(Vector3 v1, Vector3 v2) => new(v1.X / v2.X, v1.Y / v2.Y, v1.Z / v2.Z);
        public static Vector3 operator /(Vector3 v1, float f) => new(v1.X / f, v1.Y / f, v1.Z / f);
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 operator *(Vector3 v1, float f) => new(v1.X * f, v1.Y * f, v1.Z * f);

        public float Magnitude => (float)Math.Sqrt(X * X + Y * Y + Z * Z);
        public float SquaredMagnitude => X * X + Y * Y;

        public Vector3 Normalized
        {
            get
            {
                var mag = Magnitude;
                if (mag == 0)
                {
                    return default;
                }
                return this / Magnitude;
            }
        }


        public bool Equals(Vector3 other)
        {
            return this == other;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return $"({X}, {Y}, {Z})";
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static explicit operator Vector3(Vector2 v)
        {
            return new Vector3(v.X, v.Y, 1);
        }
    }
}

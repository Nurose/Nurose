﻿using Nurose.Core;
using System.Runtime.CompilerServices;

namespace Nurose.ECS
{
    public class ECSWorld : IMessageReceiver
    {
        public InputState Input { get; set; }
        public IRenderTarget RenderTarget { get; set; }

        public RenderQueue RenderQueue { get; set; }
        public ResourceManager ResourceManager { get; set; }

        public SystemContainer Systems { get; private set; }
        public ComponentContainer Components { get; set; }


        public ECSWorld()
        {
            ResourceManager = new ResourceManager();
            RenderQueue = new RenderQueue();
            Systems = new SystemContainer(this);
            Components = new ComponentContainer();
        }

        public void Update()
        {
            UpdateContent();
            RecieveMessage("FixedUpdate");
            RecieveMessage("LateFixedUpdate");
        }

        public void Clear()
        {
            Systems.Clear();
            Components.Clear();
        }

        public void Render()
        {
            NuroseMain.AudioPlayer.Update(Utils.MinMax(0, 1, Time.DeltaTime));
            RecieveMessage("Update");
            RecieveMessage("Draw");
            //Systems.MessageCaller.SendMessageToAllInParralel("Draw");
            RecieveMessage("AfterDraw");

            RecieveMessage("Render");
            RenderTarget.Render(RenderQueue);
            RecieveMessage("AfterRender");
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void RecieveMessage(string message)
        {
            Systems.SendMessage(message);
        }

        public void UpdateContent()
        {
            Systems.UpdateContent();
            Components.UpdateContent();
        }

        private void Entity_OnComponentRemoved(object sender, GenericEventArgs<Component> e)
        {
            Components.Remove(e.Value);
        }

        private void Entity_OnComponentAdded(object sender, GenericEventArgs<Component> e)
        {
            Components.Add(e.Value);
        }
    }
}
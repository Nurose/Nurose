﻿using System;
namespace Nurose.ECS
{
    /// <summary>
    /// This class is a wrapper around a <see cref="EntityID"/> in a specific <see cref="ECSWorld"/> instance.
    /// </summary>
    public struct Entity
    {
        public EntityID Id { get; private set; }
        private readonly ComponentContainer Components;



        /// <summary>
        /// Generate a new entity id for a specific <see cref="ECSWorld"/> and returns a wrapper.
        /// </summary>
        /// <param name="componentContainer"></param>
        public Entity(ECSWorld world)
        {
            Id = world.Components.GetNewEntityId();
            Components = world.Components;
        }


        /// <summary>
        /// Wrapper around an existing entity.
        /// </summary>
        /// <param name="componentContainer"></param>
        /// <param name="entityID"></param>
        public Entity(ComponentContainer componentContainer, EntityID entityID)
        {
            Id = entityID;
            Components = componentContainer;
        }


        public void AddComponent(Component component)
        {
#if DEBUG
            if (component == null)
            {
                throw new ArgumentException("Component is null");
            }
            if(component.EntityID != 0)
            {
                throw new ArgumentException("Don't set EntityId manually, because reasons.");
            }
#endif
            component.EntityID = Id;
            Components.Add(component);
        }

        public T AddComponent<T>() where T : Component, new()
        {
            T component = new();
            AddComponent(component);
            return component;
        }

        public T GetComponent<T>() where T : Component
        {
            return Components.Get<T>(Id);
        }

        public void RemoveComponent<T>() where T : Component
        {
            Components.Remove(GetComponent<T>());
        }
    }
}

﻿using Nurose.Core;

namespace Nurose.ECS
{
    public abstract class ECSSystem
    {
        protected InputState Input { get => World.Input; set => World.Input = value; }
        protected ECSWorld World { get; private set; }
        protected RenderQueue RenderQueue { get => World.RenderQueue; }
        protected ResourceManager ResourceManager { get => World.ResourceManager; }

        internal void SetWorld(ECSWorld world)
        {
            World = world;
        }
    }
}

﻿using System;

namespace Nurose.ECS
{
    [Serializable]
    public abstract class Component
    {
        public EntityID EntityID;

        public virtual Component Clone()
        {
            return MemberwiseClone() as Component;
        }

        /// <summary>
        /// To dispose/return pooled objects
        /// </summary>
        public virtual void Return()
        {

        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace Nurose.ECS
{
    public class SystemContainer : MessageContainer<ECSSystem>
    {
        private readonly ECSWorld world;
        private readonly Dictionary<Type, ECSSystem> SystemsByType = new();

        public SystemContainer(ECSWorld world)
        {
            this.world = world;
        }

        public T Add<T>() where T : ECSSystem, new()
        {
            T system = new();
            Add(system);
            return system;
        }

        public void Remove<T>() where T : ECSSystem, new()
        {
            var system = Get<T>();
            Remove(system);
        }


        public override void Add(ECSSystem entity)
        {
            entity.SetWorld(world);
            base.Add(entity);
        }

        protected override void AddToAdds()
        {
            foreach (var system in toAdd)
            {
                SystemsByType.Add(system.GetType(), system);
            }
            base.AddToAdds();
        }

        protected override void RemoveToRemoves()
        {
            foreach (var system in toRemove)
            {
                SystemsByType.Remove(system.GetType());
            }
            base.RemoveToRemoves();
        }

        public T Get<T>() where T : ECSSystem
        {
            SystemsByType.TryGetValue(typeof(T), out ECSSystem system);
            return (T)system;
        }

        public ECSSystem GetOrDefault(Type type)
        {
            if (!SystemsByType.ContainsKey(type))
                return null;
            return SystemsByType[type];
        }

        public bool Has<T>() where T : ECSSystem
        {
            return SystemsByType.ContainsKey(typeof(T));
        }


    }
}

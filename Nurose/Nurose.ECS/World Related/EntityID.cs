﻿using System;
using System.Runtime.CompilerServices;

namespace Nurose.ECS
{
    [Serializable]
    public struct EntityID : IEquatable<EntityID>, IComparable<EntityID>
    {
        internal long Id;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public EntityID(long id)
        {
            Id = id;
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static implicit operator long(EntityID id) => id.Id;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static implicit operator EntityID(long id) => new() { Id = id };


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Equals(EntityID other)
        {
            return Id == other.Id;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override bool Equals(object obj)
        {
            return obj is EntityID && Equals((EntityID)obj);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool operator ==(EntityID left, EntityID right)
        {
            return left.Equals(right);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool operator !=(EntityID left, EntityID right)
        {
            return !(left == right);
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetChecksum()
        {
            unchecked
            {
                int i = 2342742;
                i *= (int)Id;
                return i;
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override string ToString()
        {
            // return NamedEntityIds.Get(this);
            return Id.ToString();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int CompareTo(EntityID other)
        {
            return Id.CompareTo(other.Id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.CompilerServices;
using System.Text;

namespace Nurose.ECS
{
    public static class Profiler
    {
        public enum EntryType
        {
            None,
            Unfinished,
            Finished,
            Fake,
        }
        public struct Entry
        {
            public EntryType Type;
            public TimeSpan Duration;
            public int EntriesCount;
            public int Depth;
            public string Category;
        }

        public static int CurrentEntries = 0;
        public static Entry[] Entries = new Entry[10000];
        private static List<int> Parents = new();
        private static Stopwatch stopwatch = Stopwatch.StartNew();
        private static bool IsPaused;

        public static void Cleanup()
        {
#if DEBUG

            for (int i = Entries.Length / 2 + 1; i < Entries.Length; i++)
            {
                if (Entries[i].Type == EntryType.Finished && Entries[i].Depth == 0)
                {
                    int s = 0;
                    for (int j = i; j < Entries.Length; j++)
                    {
                        Entries[s] = Entries[j];
                        s++;
                    }
                    CurrentEntries = s;
                    break;
                }
            }


            for (int i = 0; i < Parents.Count; i++)
            {
                Entries[CurrentEntries++] = new Entry
                {
                    Type = EntryType.Fake,
                    Category = Entries[Parents[i]].Category,
                    Depth = Parents.Count,
                    EntriesCount = -3,
                    Duration = default,
                };
            }
#endif
        }

        public static void Begin(string categorie)
        {
#if DEBUG
            if (IsPaused)
                return;

            if (CurrentEntries > Entries.Length - 1)
                Cleanup();


            Entries[CurrentEntries++] = new Entry
            {
                Type = EntryType.Unfinished,
                Category = categorie,
                Depth = Parents.Count,
                EntriesCount = -1,
                Duration = stopwatch.Elapsed,
            };
            Parents.Add(CurrentEntries - 1);
#endif
        }


        public static void End(string categorie)
        {
#if DEBUG

            if (IsPaused)
                return;

            int parentId = Parents.Last();
            Parents.RemoveAt(Parents.Count - 1);
            var parent = Entries[parentId];

            Entries[parentId] = new Entry
            {
                Category = parent.Category,
                EntriesCount = CurrentEntries - parentId,
                Depth = parent.Depth,
                Type = EntryType.Finished,
                Duration = stopwatch.Elapsed - parent.Duration,
            };

            if (categorie != parent.Category)
                throw new Exception();

#endif
        }

        public static void Pause()
        {
#if DEBUG
            Begin("Profiler pause");
            IsPaused = true;
#endif
        }

        public static void Unpause()
        {
#if DEBUG
            IsPaused = false;
            End("Profiler pause");
#endif
        }
    }


    public static class ProfilerOld
    {


        public static bool Enabled = true;

#if DEBUGs
        public static List<ProfilerEntry> ParentLessEntries = new();
        public static DictionaryList<string, ProfilerEntry> ByCategorie = new();
        private static Stack<ProfilerEntry> Parents = new();
        private static Stack<ProfilerEntry> ProfilerEntryCache;
        static Stack<TimeSpan> BeginTimes = new();
        private static TimeSpan LastEndTime;
        private static readonly Clock Stopwatch;
#endif


        static ProfilerOld()
        {
#if DEBUGs
            Stopwatch = new Clock();
            Logger.LogDebug(Stopwatch.IsPrecise);
            ProfilerEntryCache = new();

#endif
        }

        /// <summary>
        /// Removes entries older than given timespan
        /// </summary>
        /// <param name="seconds"></param>
        public static void Cleanup(TimeSpan timeSpan)
        {
#if DEBUGs
            var minTime = DateTime.Now.TimeOfDay.Subtract(timeSpan);
            foreach (var pair in ByCategorie)
            {
                var toRemoveCount = 0;
                foreach (var e in pair.Value)
                {
                    if (e.StartTime >= minTime)
                        break;

                    toRemoveCount++;
                }

                for (var i = 0; i < toRemoveCount; i++)
                {
                    //if (!ProfilerEntryCache.Contains(pair.Value.First.Value))
                        ProfilerEntryCache.Push(pair.Value.First.Value);
                    pair.Value.RemoveFirst();
                }
            }

            foreach (var e in ParentLessEntries.Where(e => e.StartTime < minTime).ToList())
            {
                ParentLessEntries.Remove(e);

            }

           // while (ProfilerEntryCache.Count > 1000)
           //     ProfilerEntryCache.Pop();
#endif
        }

#if DEBUGs
        private static ProfilerEntry GetUnusedEntry()
        {
            if (ProfilerEntryCache.TryPop(out ProfilerEntry entry))
            {
                return entry;
            }
            return new ProfilerEntry() { Children = new()};
        }
#endif
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Begin(string categorie)
        {
#if DEBUGs
            var elapsed = Stopwatch.UtcNow;

            if (Parents.Count > 0)
            {
                /*
                    string name = $"~ Before {categorie}";
                    var e = new ProfilerEntry()
                    {
                        Category = name,
                        Children = new(),
                        Parent = Parents.LastOrDefault(),
                        StartTime = DateTime.Now.TimeOfDay,
                        Duration = Stopwatch.UtcNow - LastEndTime,
                        IsOptional = true,
                    };
                    ByCategorie[GetFullName(e)].AddLast(e);
                    Parents.Peek().Children.Add(e);
                    */
            }


            BeginTimes.Push(Stopwatch.UtcNow);
            var entry = GetUnusedEntry();

            entry.Category = categorie;
            entry.Children.Clear();
            entry.Parent = Parents.LastOrDefault();
            entry.StartTime = DateTime.Now.TimeOfDay;

            Parents.Push(entry);
            ByCategorie[GetFullName(entry)].AddLast(entry);

#endif
        }

        public static string GetFullName(ProfilerEntry entry)
        {
            StringBuilder name = new();
            var cur = entry;
            while (cur != null)
            {
                name.Append(cur.Category + ".");
                cur = cur.Parent;
            }

            return name.ToString();
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void End(string categorie)
        {
#if DEBUGs
            var endTime = Stopwatch.UtcNow;
            var entry = Parents.Pop();


            if (categorie != entry.Category)
            {
                throw new Exception();
            }

            entry.Duration = endTime - BeginTimes.Pop();


            if (entry.Parent == null)
            {
                ParentLessEntries.Add(entry);
            }
            else
                Parents.Peek().Children.Add(entry);

            LastEndTime = endTime;
#endif
        }

#if DEBUGs
        public static string DumpJsonTracing()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("[");

            foreach (var baseEntry in ParentLessEntries)
                WriteEntry(baseEntry, builder);

            builder = builder.Remove(builder.Length - 3, 3);
            builder.Append("]");
            return builder.ToString();
        }

        private static void WriteEntry(ProfilerEntry entry, StringBuilder stringBuilder)
        {
            stringBuilder.AppendLine(@$"{{""name"":""{entry.Category}"", ""cat"":""PERF"", ""ph"":""B"", ""ts"":{(ulong)(entry.StartTime.TotalMilliseconds * 1000)},""pid"":""0""}},");
            foreach (var entryChild in entry?.Children)
            {
                WriteEntry(entryChild, stringBuilder);
            }
            stringBuilder.AppendLine(@$"{{""name"":""{entry.Category}"", ""cat"":""PERF"", ""ph"":""E"", ""ts"":{(ulong)((entry.StartTime + entry.Duration).TotalMilliseconds * 1000)},""pid"":""0""}},");
        }
#endif
    }
}
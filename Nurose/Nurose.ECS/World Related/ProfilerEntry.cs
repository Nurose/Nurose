﻿using System;
using System.Collections.Generic;

namespace Nurose.ECS
{
    public class ProfilerEntry
    {
        public bool IsOptional;
        public string Category;
        public TimeSpan StartTime;
        public TimeSpan Duration;
        public ProfilerEntry Parent;
        public List<ProfilerEntry> Children;
    }
}
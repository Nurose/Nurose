﻿using System;

namespace Nurose.ECS
{
    public sealed class GenericEventArgs<T> : EventArgs
    {
        public GenericEventArgs(T value)
        {

            Value = value;
        }

        /// <summary>
        /// Relevant component
        /// </summary>
        public T Value { get; }
    }
}

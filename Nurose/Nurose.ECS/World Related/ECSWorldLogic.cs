﻿using Nurose.Core;

namespace Nurose.ECS
{
    public class ECSWorldLogic : IEntryLogic
    {
        public ECSWorld World { get; set; } = new ECSWorld();

        public InputState Input { get => World.Input; set => World.Input = value; }
        public IRenderTarget RenderTarget { get => World.RenderTarget; set => World.RenderTarget = value; }

        public void Render()
        {
            World.Render();
        }

        public void Start()
        {
        }

        public void Update()
        {
            World.Update();
        }
    }
}
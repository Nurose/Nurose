﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Nurose.ECS
{
    public sealed class DictionaryList<TKey, TItem> : IEnumerable<KeyValuePair<TKey, LinkedList<TItem>>>
    {
        private readonly Dictionary<TKey, LinkedList<TItem>> dictonary = new();

        public DictionaryList()
        {
        }

        public DictionaryList(DictionaryList<TKey, TItem> toCopy)
        {
            dictonary = new Dictionary<TKey, LinkedList<TItem>>(toCopy.dictonary);

            var originalLists = toCopy.dictonary.OrderBy(v => v).ToList();

            for (int i = 0; i < originalLists.Count; i++)
            {
                dictonary.Add(originalLists[i].Key, new LinkedList<TItem>(originalLists[i].Value));
            }
        }

        public LinkedList<TItem> this[TKey key] => Get(key);


        //public KeyValuePair<TKey, LinkedList<TItem>> this[int i] => dictonary.ElementAt(i);

        public int Count => dictonary.Count;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public LinkedList<TItem> Get(TKey key)
        {
            if (!dictonary.ContainsKey(key))
            {
                dictonary.Add(key, new LinkedList<TItem>());
            }
            return dictonary[key];
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Remove(TKey key, TItem item)
        {
            var list = Get(key);
            list.Remove(item);

            if (list.Count == 0)
            {
                dictonary.Remove(key);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Update(TKey key, TItem old, TItem @new)
        {
            var list = Get(key);
            list.Remove(old);
            list.AddLast(@new);

            if (list.Count == 0)
            {
                dictonary.Remove(key);
            }
        }



        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool ContainsKey(TKey type)
        {
            return dictonary.ContainsKey(type);
        }


        public IEnumerator<KeyValuePair<TKey, LinkedList<TItem>>> GetEnumerator()
        {
            return dictonary.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}

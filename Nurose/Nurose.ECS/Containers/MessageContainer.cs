﻿using System.Runtime.CompilerServices;

namespace Nurose.ECS
{
    /// <summary>
    /// A container that can call messages on its entries that implement <see cref="IMessageReceiver"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MessageContainer<T> : Container<T>, IMessageReceiver where T : class
    {
        public MessageCaller<T> MessageCaller { get; set; }
        public bool SkipCallingStart { get; set; }

        public MessageContainer() : base()
        {
            MessageCaller = new MessageCaller<T>();
            OnContainableAdded += MessageContainer_OnContainableAdded;
            OnContainableRemoved += MessageContainer_OnContainableRemoved;
            AddCurrentlyInContainer();
        }

        private void AddCurrentlyInContainer()
        {
            foreach (T item in GetAll())
            {
                MessageCaller.Add(item);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public virtual void RecieveMessage(string message)
        {
            SendMessage(message);
        }

        private void MessageContainer_OnContainableRemoved(object sender, GenericEventArgs<T> e)
        {
            MessageCaller.Remove(e.Value);
        }

        private void MessageContainer_OnContainableAdded(object sender, GenericEventArgs<T> e)
        {
            MessageCaller.Add(e.Value);
            if (!SkipCallingStart)
                MessageCaller.SendToOne(e.Value, "Start");
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public virtual void SendMessage(string messageName)
        {
            MessageCaller.SendMessageToAll(messageName);
        }
            
        public virtual void SendMessageToOne(T target, string messageName)
        {
            MessageCaller.SendToOne(target, messageName);
        }
    }
}

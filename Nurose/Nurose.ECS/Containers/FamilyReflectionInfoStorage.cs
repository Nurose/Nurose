﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace Nurose.ECS
{
    public static class FamilyReflectionInfoStorage
    {
        private static Dictionary<Type, FamilyTypeReflectionInfo> SetDelegatesByFamily = new();

        public static FamilyTypeReflectionInfo GetReflectionInfo(Type famType)
        {
            if (!SetDelegatesByFamily.ContainsKey(famType))
            {
                SetDelegatesByFamily.Add(famType, ConstructEntry(famType));
            }
            return SetDelegatesByFamily[famType];
        }

        private static FamilyTypeReflectionInfo ConstructEntry(Type famType)
        {
            var e = new FamilyTypeReflectionInfo();
            e.FieldInfos = famType.GetFields();
            e.IsOptionalField = e.FieldInfos
                .Select(t => t.GetCustomAttribute<OptionalComponentAttribute>() != null)
                .ToArray();
            e.SetDelegates = new Action<object, object>[e.FieldInfos.Count()];
            for (int i = 0; i < e.FieldInfos.Length; i++)
            {
                e.SetDelegates[i] = CreateSetAccessor(e.FieldInfos[i]);
            }
            return e;
        }

        private static Action<object, object> CreateSetAccessor(FieldInfo field)
        {
            DynamicMethod setMethod = new(field.Name, typeof(void), new[] { typeof(object), typeof(object) });
            ILGenerator generator = setMethod.GetILGenerator();
            LocalBuilder local = generator.DeclareLocal(field.DeclaringType);
            generator.Emit(OpCodes.Ldarg_0);
            if (field.DeclaringType.IsValueType)
            {
                generator.Emit(OpCodes.Unbox_Any, field.DeclaringType);
                generator.Emit(OpCodes.Stloc_0, local);
                generator.Emit(OpCodes.Ldloca_S, local);
            }
            else
            {
                generator.Emit(OpCodes.Castclass, field.DeclaringType);
                generator.Emit(OpCodes.Stloc_0, local);
                generator.Emit(OpCodes.Ldloc_0, local);
            }
            generator.Emit(OpCodes.Ldarg_1);
            if (field.FieldType.IsValueType)
            {
                generator.Emit(OpCodes.Unbox_Any, field.FieldType);
            }
            else
            {
                generator.Emit(OpCodes.Castclass, field.FieldType);
            }
            generator.Emit(OpCodes.Stfld, field);
            generator.Emit(OpCodes.Ret);
            return (Action<object, object>)setMethod.CreateDelegate(typeof(Action<object, object>));
        }

    }
}
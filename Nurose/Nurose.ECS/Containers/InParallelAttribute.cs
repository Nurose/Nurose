﻿using System;

namespace Nurose.ECS
{
    [AttributeUsage(AttributeTargets.Class)]
    public class InParallelAttribute : Attribute
    {
    }
}
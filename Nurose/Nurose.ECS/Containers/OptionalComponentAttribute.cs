﻿using System;

namespace Nurose.ECS
{
    [AttributeUsage(AttributeTargets.Field)]
    public class OptionalComponentAttribute : Attribute
    {

    }
}

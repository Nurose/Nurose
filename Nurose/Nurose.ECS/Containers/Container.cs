﻿using Nurose.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Nurose.ECS
{
    /// <summary>
    /// <see cref="Container{T}"/> is an implementation of <see cref="ICollection{T}"/>. It updates its content only when <see cref="UpdateContent"/> is called. 
    /// <para> Adding and removing objects can be done while iterating through a container. </para> 
    /// </summary>  
    public class Container<T> : ICollection<T>
    {
        public event EventHandler<GenericEventArgs<T>> OnContainableAdded;
        public event EventHandler<GenericEventArgs<T>> OnContainableRemoved;

        protected List<T> objects = new();
        protected List<T> toAdd = new();
        protected List<T> toRemove = new();


        public bool InsertAtBeginning = false;
        public bool IsReadOnly => false;
        public bool UpdateWillAlterContent => toAdd.Count > 0 || toRemove.Count > 0;
        public int Count => objects.Count;


        /// <summary>
        /// Marks all items to be removed during the next <see cref="UpdateContent"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Clear()
        {
            foreach (var item in objects)
            {
                Remove(item);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Container{T}"/> class.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Container()
        {
        }

        /// <summary>
        /// Determine whether an <typeparamref name="T"/> is in the Container. This includes the ones who are marked to be added during the next FixedUpdate. 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Contains(T item) => objects.Contains(item);

        /// <summary>
        /// Determine whether an object will be added in the next update.
        /// </summary>
        /// <param name="item"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool WillBeAdded(T item) => toAdd.Contains(item);

        /// <summary>
        /// Get objects currently in the <see cref="Container{T}"/>.
        /// </summary>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public IReadOnlyList<T> GetAll() => objects.ToList();


        /// <summary>
        /// Add an object of type <seealso cref="T"/> to the queue to be added during the next <see cref="UpdateContent"/>. 
        /// </summary>
        /// <param name="item"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public virtual void Add(T item)
        {
#if DEBUG
            if (objects.Contains(item))
            {
                Logger.LogWarning("Container already has instance of " + item.GetType().Name);
                return;
            }
#endif
            toAdd.Add(item);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void AddRange(IEnumerable<T> objects)
        {
            foreach (T obj in objects)
            {
                Add(obj);
            }
        }

        /// <summary>
        /// Add an <see cref="T"/> to the queue to be removed during the next <see cref="UpdateContent"/>. 
        /// </summary>
        /// <param name="item"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Remove(T item)
        {
            toRemove.Add(item);
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void RemoveRange(IEnumerable<T> objects)
        {
            foreach (T obj in objects)
            {
                Remove(obj);
            }
        }

        /// <summary>
        /// Add all the entities in <see cref="toAdd"/>
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        virtual protected void AddToAdds()
        {
            if (InsertAtBeginning)
                foreach (var item in toAdd)
                    objects.Insert(0, item);
            else
                foreach (var item in toAdd)
                    objects.Add(item);

            foreach (var item in toAdd)
            {
                if (OnContainableAdded != null)
                {
                    OnContainableAdded.Invoke(this, new GenericEventArgs<T>(item));
                }
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected void FinishAddToAdds()
        {
            toAdd.Clear();
        }

        /// <summary>
        /// Remove each <see cref="object"/> from the <see cref="Container"/> that are in <see cref="toRemove"/>
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected virtual void RemoveToRemoves()
        {
            foreach (var item in toRemove.Distinct())
            {
                ExecuteRemove(item);
            }
            toRemove.Clear();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ExecuteRemove(T item)
        {
            objects.Remove(item);
            OnContainableRemoved?.Invoke(this, new GenericEventArgs<T>(item));
        }

        /// <summary>
        /// Update the content of the <see cref="Container{T}"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public virtual void UpdateContent()
        {
            RemoveToRemoves();
            AddToAdds();
            FinishAddToAdds();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public IEnumerator<T> GetEnumerator()
        {
            return objects.GetEnumerator();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        IEnumerator IEnumerable.GetEnumerator()
        {
            return objects.GetEnumerator();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void CopyTo(T[] array, int arrayIndex)
        {
            objects.CopyTo(array, arrayIndex);
        }
    }
}
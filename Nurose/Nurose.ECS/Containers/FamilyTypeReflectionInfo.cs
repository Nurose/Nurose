﻿using System;
using System.Reflection;

namespace Nurose.ECS
{
    public class FamilyTypeReflectionInfo
    {
        public FieldInfo[] FieldInfos;
        public bool[] IsOptionalField;
        public Action<object, object>[] SetDelegates;
    }
}
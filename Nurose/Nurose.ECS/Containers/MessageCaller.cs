﻿using Nurose.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Nurose.ECS
{
    public class MessageCaller<T> where T : class
    {
        private readonly Dictionary<string, List<Action>> Delegates;
        private readonly List<IMessageReceiver> ChildSendMessages;
        public event EventHandler<Action> OnActionStart;
        public event EventHandler<Action> OnActionEnd;
        public event EventHandler<string> OnMessageRecievedStart;
        public event EventHandler<string> OnMessageRecievedEnd;

        public MessageCaller()
        {
            Delegates = new Dictionary<string, List<Action>>();
            ChildSendMessages = new List<IMessageReceiver>();
        }

        public void Remove(T obj)
        {
            List<string> messageInfos = new();
            foreach (var methodInfo in ReflectionSaver.GetMethods(obj.GetType()))
            {
                if (methodInfo.ContainsGenericParameters || methodInfo.ReturnType != typeof(void) || methodInfo.GetParameters().Length != 0)
                {
                    continue;
                }
                messageInfos.Add(methodInfo.Name);
            }

            foreach (var messageInfo in messageInfos)
            {
                if (!Delegates.TryGetValue(messageInfo, out var list))
                {
                    continue;
                }

                for (int i = list.Count - 1; i >= 0; i--)
                {
                    if (list[i].Target == obj)
                    {
                        list.RemoveAt(i);
                        if (list.Count == 0)
                        {
                            Delegates.Remove(messageInfo);
                        }
                        break;
                    }
                }
            }

            var target = ChildSendMessages.SingleOrDefault(c => c == obj);
            if (target != null)
            {
                ChildSendMessages.Remove(target);
            }
        }

        public void Add(T item)
        {
            Type type = item.GetType();

            if (item is IMessageReceiver receiver)
            {
                ChildSendMessages.Add(receiver);
            }

            if (type.IsGenericType)
            {
                return;
            }

            foreach (var methodInfo in type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy | BindingFlags.NonPublic))
            {
                if (methodInfo.ContainsGenericParameters || methodInfo.ReturnType != typeof(void) || methodInfo.GetParameters().Length != 0)
                {
                    continue;
                }

                Action del = (Action)Delegate.CreateDelegate(typeof(Action), item, methodInfo.Name);
                AddDelegate(methodInfo.Name, del);
            }
        }

        private void AddDelegate(string messageEnum, Action action)
        {
            if (Delegates.ContainsKey(messageEnum))
            {
                Delegates[messageEnum].Add(action);
            }
            else
            {
                Delegates.Add(messageEnum, new List<Action>()
                {
                    action
                });
            }
        }

        public void SendMessageToAll(string messageName)
        {
            bool exists = Delegates.TryGetValue(messageName, out List<Action> match);
            bool sendMessages = OnMessageRecievedStart != null;
            bool hasEndAction = OnActionEnd != null;
            if (exists)
            {
                if (sendMessages)
                    OnMessageRecievedStart.Invoke(this, messageName);
                Profiler.Begin(messageName);
                foreach (Action action in match)
                {
                    if (hasEndAction)
                        OnActionStart.Invoke(this, action);
                    Profiler.Begin(action.Target.GetType().Name + " " + action.Method.Name);

                    action();
                    Profiler.End(action.Target.GetType().Name + " " + action.Method.Name);

                    if (hasEndAction)
                        OnActionEnd.Invoke(this, action);
                }

                if (sendMessages)
                    OnMessageRecievedEnd.Invoke(this, messageName);
                Profiler.End(messageName);
            }

            foreach (var receiver in ChildSendMessages)
            {
                receiver.RecieveMessage(messageName);
            }
        }

        List<Action> parallellList = new();
        public void SendMessageToAllInParralel(string messageName)
        {
            bool exists = Delegates.TryGetValue(messageName, out List<Action> match);
            if (!exists)
                return;

            foreach (var action in match)
            {
                var parallelAttribute = action.Method.DeclaringType.GetCustomAttribute<InParallelAttribute>();
                if (parallelAttribute != null)
                {
                    parallellList.Add(action);
                }
                else
                {
                    if (parallellList.Count > 0)
                    {
                        Parallel.ForEach(parallellList, m => m());
                        parallellList.Clear();
                    }

                    OnActionStart?.Invoke(this, action);
                    action();
                    OnActionEnd?.Invoke(this, action);
                }
            }
        }


        public void SendToOne(T target, string message)
        {
            var succes = Delegates.TryGetValue(message, out List<Action> methods);
            if (succes)
            {
                foreach (var method in methods)
                {
                    if (method.Target == target)
                    {
                        method();
                    }
                }
            }
        }
    }
}
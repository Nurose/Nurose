﻿using System.Collections;
using System.Collections.Generic;

namespace Nurose.ECS
{
    public class MessageList<T> : IList<T> where T : class
    {
        private readonly List<T> list = new();
        private readonly MessageCaller<T> messageCaller = new();

        public int Count => list.Count;

        public bool IsReadOnly => ((IList<T>)list).IsReadOnly;

        public T this[int index] { get => list[index]; set => list[index] = value; }

        public void Add(T item)
        {
            list.Add(item);
            messageCaller.Add(item);
        }

        public void Remove(T item)
        {
            list.Remove(item);
            messageCaller.Remove(item);
        }

        public void SendMessage(string message)
        {
            messageCaller.SendMessageToAll(message);
        }

        public void CallStart(T target)
        {
            messageCaller.SendToOne(target, "Start");
        }

        public void AddRange(IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                Add(item);
            }
        }

        public MessageList<T> Clone()
        {
            var clone = new MessageList<T>();
            clone.AddRange(list);
            return clone;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return list.GetEnumerator();
        }

        public int IndexOf(T item)
        {
            return list.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            list.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            list.RemoveAt(index);
        }

        public void Clear()
        {
            list.Clear();
        }

        public bool Contains(T item)
        {
            return list.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            list.CopyTo(array, arrayIndex);
        }

        bool ICollection<T>.Remove(T item)
        {
            return list.Remove(item);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ((IList<T>)list).GetEnumerator();
        }
    }
}

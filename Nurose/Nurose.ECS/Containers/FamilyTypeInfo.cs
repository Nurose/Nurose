﻿using System;

namespace Nurose.ECS
{

    public class FamilyTypeInfo
    {
        public Type Type { get; private set; }
        public FamilyTypeReflectionInfo FamilyTypeReflectionInfo { get; private set; }

        public FamilyTypeInfo(Type type)
        {
            Type = type;
            FamilyTypeReflectionInfo = FamilyReflectionInfoStorage.GetReflectionInfo(type);
        }

        public object CreateInstance(object[] values)
        {
            var entry = ActivatorFast.CreateInstance(Type);

            int i = 0;
            foreach (var field in FamilyTypeReflectionInfo.FieldInfos)
            {
                FamilyTypeReflectionInfo.SetDelegates[i](entry, values[i]);
                i++;
            }
            return entry;
        }



        public override string ToString()
        {
            return Type.Name;
        }

        public FamilyTypeInfo Clone()
        {
            return (FamilyTypeInfo)MemberwiseClone();
        }
    }
}
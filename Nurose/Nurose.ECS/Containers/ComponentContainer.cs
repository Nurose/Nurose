﻿using Nurose.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Nurose.ECS
{
    [Serializable]
    public class ComponentContainer : Container<Component>
    {
        public Dictionary<Type, FamilyTypeInfo> FamiliesRegisteredByType = new();
        public DictionaryList<Type, FamilyTypeInfo> RelevantFamiliesByComponentType = new();
        public List<FamilyTypeInfo> FamilieTypeInfos = new();

        public Dictionary<FamilyTypeInfo, IList> FamilyEntriesByFamilyType = new();
        public DictionaryList<EntityID, object> FamilieEntriesByEntityID = new();
        public Dictionary<object, EntityID> IdsByFamilyEntries = new();

        public Dictionary<EntityID, Dictionary<Type, Component>> ComponentsByEntityID = new();
        public DictionaryList<Type, Component> byType = new();

        private long lastGeneratedEntityId;

        public ComponentContainer()
        {
        }

        public EntityID GetNewEntityId()
        {
            return new EntityID(++lastGeneratedEntityId);
        }

        public ComponentContainer(IEnumerable<Component> components)
        {
            AddRange(components);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public IEnumerable<T> GetByFamily<T>()
        {
            Type key = typeof(T);
#if DEBUG
            if (!key.IsClass)
                throw new Exception("Family has to be class");
#endif

            if (!FamiliesRegisteredByType.ContainsKey(key))
                RegisterFamily(key);

            return FamilyEntriesByFamilyType[FamiliesRegisteredByType[key]] as List<T>;
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T GetFamily<T>(EntityID entityID)
        {
            return (T)GetFamily(typeof(T), entityID);
        }

        public object GetFamily(Type type, EntityID entityID)
        {
            if (!FamiliesRegisteredByType.ContainsKey(type))
                RegisterFamily(type);

            foreach (var f in FamilieEntriesByEntityID[entityID])
            {
                if (f.GetType() == type)
                    return f;
            }
            return null;

/*            var fams = FamilieEntriesByEntityID[entityID];

            int validCount = 0;
            object fam = null;
            foreach (var f in fams)
            {
                if (f.GetType() == type)
                {
                    validCount++;
                    fam = f;
                    return f;
                }

                if (validCount > 1)
                    return null;
            }
            return fam;*/

        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T Get<T>() where T : Component
        {
            var entries = byType[typeof(T)];

            if (entries.Count == 1)
                return (T)entries.First.Value;

            if (entries.Count > 1)
                throw new Exception($"More than one instance of {nameof(T)}");

            return null;
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T Get<T>(EntityID id) where T : Component
        {
            if (!ComponentsByEntityID.TryGetValue(id, out Dictionary<Type, Component> dict))
                return null;

            dict.TryGetValue(typeof(T), out Component component);

            return component as T;
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T GetFast<T>(EntityID id) where T : Component
        {
            return (T)ComponentsByEntityID[id][typeof(T)];
        }

        public IEnumerable<EntityID> GetEntityIDs()
        {
            return ComponentsByEntityID.Keys;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public IEnumerable<Component> Get(Type compType)
        {
            return byType[compType];
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Component Get(Type componentType, EntityID id)
        {
            if (!ComponentsByEntityID.TryGetValue(id, out Dictionary<Type, Component> dict))
                return null;

            dict.TryGetValue(componentType, out Component component);

            return component;
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public IEnumerable<T> GetAll<T>()
        {
            return byType[typeof(T)].Cast<T>();
        }


        /// <summary>
        /// Get all components with a specific <see cref="EntityID"/>.
        /// </summary>
        /// <param name="entityID"></param>
        /// <returns></returns>
        public IEnumerable<Component> GetAll(EntityID entityID)
        {
            if (ComponentsByEntityID.TryGetValue(entityID, out Dictionary<Type, Component> result))
                return result.Values.Distinct();
            else
                return Enumerable.Empty<Component>();
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void RegisterFamily(Type key)
        {
            var info = new FamilyTypeInfo(key);
            foreach (var field in info.FamilyTypeReflectionInfo.FieldInfos)
            {
                var fieldType = field.FieldType;
                RelevantFamiliesByComponentType[fieldType].AddLast(info);
            }

            FamilieTypeInfos.Add(info);

            FamiliesRegisteredByType.Add(key, info);
            IList list = ActivatorFast.CreateInstance(typeof(List<>).MakeGenericType(info.Type)) as IList;
            FamilyEntriesByFamilyType.Add(info, list);
            var entitiesChecked = new HashSet<EntityID>();
            foreach (var comp in GetAll())
            {
                if (entitiesChecked.Contains(comp.EntityID))
                {
                    continue;
                }

                AddFamilyEntryIfPossible(info, comp.EntityID);
                entitiesChecked.Add(comp.EntityID);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void RemoveAllOf(EntityID id)
        {
            //var methodBase = new System.Diagnostics.StackTrace().GetFrame(1).GetMethod();
            //Console.WriteLine($"{methodBase.DeclaringType}.{methodBase.Name} removed all of id={id}");
            foreach (var comp in GetAll(id))
            {
                Remove(comp);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override void Add(Component comp)
        {
            if (comp.EntityID == default)
                throw new Exception("Can't add a component with EntityID = 0");

            base.Add(comp);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected override void AddToAdds()
        {
            var toCheckForNewFamEntries = new HashSet<EntityID>();
            foreach (var comp in toAdd)
            {
#if DEBUG
                var componentType = comp.GetType();
                if (Get(componentType, comp.EntityID) != null)
                    throw new Exception("Duplicate component!");
#endif

                List<Type> types = GetRelevantBaseTypes(comp);
                foreach (var type in types)
                {
                    byType[type].AddLast(comp);
                }

                if (!ComponentsByEntityID.ContainsKey(comp.EntityID))
                    ComponentsByEntityID.Add(comp.EntityID, new Dictionary<Type, Component>());


                var dict = ComponentsByEntityID[comp.EntityID];
                var t = comp.GetType();
                while (t != typeof(Component))
                {
                    if (!dict.ContainsKey(t))
                        dict.Add(t, comp);

                    t = t.BaseType;
                }

                toCheckForNewFamEntries.Add(comp.EntityID);
            }

            foreach (var entityId in toCheckForNewFamEntries)
            {
                foreach (var familyTypeInfo in FamilieTypeInfos)
                {
                    AddFamilyEntryIfPossible(familyTypeInfo, entityId);
                }
            }

            base.AddToAdds();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static List<Type> GetRelevantBaseTypes(Component comp)
        {
            var types = new List<Type>();
            Type type = comp.GetType();
            if (type.IsGenericType)
            {
                types.Add(type);
                type = type.GetGenericTypeDefinition();
            }
            types.AddRange(ReflectionSaver.GetBaseTypes(type));
            types.AddRange(ReflectionSaver.GetInterfaces(type));
            types.Remove(typeof(Component));
            types.Remove(typeof(object));
            types.Add(type);
            return types;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected override void RemoveToRemoves()
        {
            foreach (var comp in toRemove.Distinct())
            {
                comp.Return();
                Dictionary<Type, Component> componentsOfEntity = ComponentsByEntityID[comp.EntityID];


                foreach (var type in GetRelevantBaseTypes(comp))
                {
                    byType.Remove(type, comp);
                    componentsOfEntity.Remove(type);
                }

                if (componentsOfEntity.Count == 0)
                    ComponentsByEntityID.Remove(comp.EntityID);

                foreach (var familyType in RelevantFamiliesByComponentType[comp.GetType()])
                {
                    var famToRemove = FindFamEntryWithComponent(familyType, comp);
                    if (famToRemove != null)
                    {
                        FieldInfo field = familyType.FamilyTypeReflectionInfo.FieldInfos.Single(f => f.FieldType == comp.GetType());
                        bool requiredComp = field.GetCustomAttribute<OptionalComponentAttribute>() == null;
                        FamilyEntriesByFamilyType[familyType].Remove(famToRemove);
                        FamilieEntriesByEntityID.Remove(comp.EntityID, famToRemove);
                        IdsByFamilyEntries.Remove(famToRemove);

                        if (!requiredComp)
                        {
                            AddFamilyEntryIfPossible(familyType, comp.EntityID);
                        }
                    }
                }
            }

            base.RemoveToRemoves();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private object FindFamEntryWithComponent(FamilyTypeInfo famType, Component component)
        {
            if (FamilieEntriesByEntityID.ContainsKey(component.EntityID))
            {
                foreach (var e in FamilieEntriesByEntityID[component.EntityID])
                {
                    if (e.GetType() == famType.Type)
                    {
                        return e;
                    }
                }
            }

            return null;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void AddFamilyEntryIfPossible(FamilyTypeInfo familyInfo, EntityID entityID)
        {
            if (!CreateFamily(familyInfo, entityID, out var familyInstance))
                return;

            if (GetFamily(familyInfo.Type, entityID) != null)
                return;

            FamilyEntriesByFamilyType[familyInfo].Add(familyInstance);
            IdsByFamilyEntries.Add(familyInstance, entityID);
            FamilieEntriesByEntityID[entityID].AddLast(familyInstance);
        }

        private bool CreateFamily(FamilyTypeInfo familyInfo, EntityID entityID, out object familyInstance)
        {
            object[] fields = new object[familyInfo.FamilyTypeReflectionInfo.FieldInfos.Count()];

            int i = 0;
            var components = ComponentsByEntityID[entityID];
            foreach (var field in familyInfo.FamilyTypeReflectionInfo.FieldInfos)
            {
                Component comp = GetComponentOfType(components, field.FieldType);
                fields[i] = comp;

                if (fields[i] == null && !familyInfo.FamilyTypeReflectionInfo.IsOptionalField[i])
                {
                    familyInstance = null;
                    return false;
                }

                i++;
            }

            familyInstance = familyInfo.CreateInstance(fields);
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static Component GetComponentOfType(Dictionary<Type, Component> components, Type targetType)
        {
            components.TryGetValue(targetType, out Component component);
            return component;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ComponentContainer ShallowCopy()
        {
            var comps = new LinkedList<Component>(objects);
            var clone = new ComponentContainer(comps);
            clone.UpdateContent();
            return clone;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ComponentContainer DeepCopy()
        {
            var clone = new ComponentContainer();

            var compDict = new Dictionary<Component, Component>();
            foreach (var oriComp in objects)
            {
                var c = oriComp.Clone();
                compDict.Add(oriComp, c);
                clone.objects.Add(c);
            }

            var famTypeDict = new Dictionary<FamilyTypeInfo, FamilyTypeInfo>();
            foreach (var oriFamTypeInfo in FamilieTypeInfos)
            {
                famTypeDict.Add(oriFamTypeInfo, oriFamTypeInfo.Clone());
            }

            foreach (var c in toAdd)
                clone.toAdd.Add(c.Clone());

            foreach (var c in toRemove)
                clone.toRemove.Add(compDict[c]);

            foreach (var pair in byType)
            {
                var linkedList = clone.byType[pair.Key];
                foreach (var oriComp in pair.Value)
                {
                    linkedList.AddLast(compDict[oriComp]);
                }
            }

            foreach (var pair in ComponentsByEntityID)
            {
                var components = new Dictionary<Type, Component>(pair.Value.Count);
                var entityId = pair.Key;

                foreach (var dictPair in pair.Value)
                    components.Add(dictPair.Key, compDict[dictPair.Value]);

                clone.ComponentsByEntityID.Add(entityId, components);
            }

            var famDict = new Dictionary<object, object>();
            foreach (var pair in FamilyEntriesByFamilyType)
            {
                foreach (var fam in pair.Value)
                {
                    var id = IdsByFamilyEntries[fam];
                    clone.CreateFamily(pair.Key, id, out object newfam);
                    famDict.Add(fam, newfam);
                }
            }

            clone.FamilieTypeInfos = FamilieTypeInfos.Select(t => famTypeDict[t]).ToList();
            foreach (var pair in FamiliesRegisteredByType)
            {
                var cloneFamilyInfo = famTypeDict[pair.Value];
                clone.FamiliesRegisteredByType.Add(pair.Key,
                    cloneFamilyInfo);
            }

            foreach (var pair in RelevantFamiliesByComponentType)
            {
                var familyTypeInfos = clone.RelevantFamiliesByComponentType[pair.Key];
                foreach (var entry in pair.Value)
                {
                    familyTypeInfos.AddLast(famTypeDict[entry]);
                }
            }


            foreach (var pair in FamilieEntriesByEntityID)
            {
                var entityId = pair.Key;
                var fams = pair.Value;
                var linkedList = clone.FamilieEntriesByEntityID[pair.Key];
                foreach (var fam in fams)
                {
                    var famclone = famDict[fam];
                    linkedList.AddLast(famclone);
                }
            }

            foreach (var pair in FamilyEntriesByFamilyType)
            {
                var list = pair.Value.Cast<object>().Select(o => famDict[o]);
                clone.FamilyEntriesByFamilyType.Add(famTypeDict[pair.Key],
                    ActivatorFast.CreateInstance(typeof(List<>).MakeGenericType(pair.Key.Type)) as IList);

                var cloneEntriesByPairType = clone.FamilyEntriesByFamilyType[famTypeDict[pair.Key]];
                foreach (var famClone in list)
                {
                    cloneEntriesByPairType.Add(famClone);
                }
            }


            foreach (var pair in IdsByFamilyEntries)
            {
                clone.IdsByFamilyEntries.Add(famDict[pair.Key], pair.Value);
            }
#if DEBUG
            foreach (var familyTypeInfo in FamilyEntriesByFamilyType.Keys)
            {
                if (clone.FamilyEntriesByFamilyType[famTypeDict[familyTypeInfo]].Count != FamilyEntriesByFamilyType[familyTypeInfo].Count)
                    throw new Exception("DeepClone bug found!");
            }
#endif
            clone.lastGeneratedEntityId = lastGeneratedEntityId;

            return clone;
        }

        public void Return()
        {
            foreach (var comp in objects)
                comp.Return();
            foreach (var comp in toAdd)
                comp.Return();
            foreach (var comp in toRemove)
                comp.Return();
        }
    }
}
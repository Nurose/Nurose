﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Nurose.ECS
{
    public class Pool<T> : IEnumerable<T> where T : Component
    {
        public T[] values;
        public bool[] valid;
        public int MaxSize;

        public Pool(int maxSize)
        {
            MaxSize = maxSize;
            values = new T[maxSize];
            valid = new bool[maxSize];
        }

        public void Remove(T obj)
        {
            for (int i = 0; i < values.Length; i++)
            {
                if (values[i] == obj)
                {
                    valid[i] = false;
                }
            }
        }

        public int AliveCount => valid.Sum(v => v ? 1 : 0);

        public IEnumerator GetEnumerator()
        {
            for (int i = 0; i < values.Length; i++)
            {
                if (valid[i])
                    yield return values[i];
            }
        }

        public void Add(T obj)
        {
            for (int i = 0; i < values.Length; i++)
            {
                if (!valid[i])
                {
                    values[i] = obj;
                    valid[i] = true;
                    return;
                }
            }
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            for (int i = 0; i < values.Length; i++)
            {
                if (valid[i])
                    yield return values[i];
            }
        }
    }
}

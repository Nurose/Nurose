﻿using System;
using System.Runtime.CompilerServices;

namespace Nurose.ECS
{
    public struct ComponentRef<T> : IEquatable<ComponentRef<T>> where T : Component
    {
        private EntityID entityID;

        public EntityID EntityID { get => entityID; private set => entityID = value; }


        public ComponentRef(EntityID entityID)
        {
            this.entityID = entityID;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T Get(ComponentContainer components)
        {
            return components.Get<T>(EntityID);
        }

        public override bool Equals(object obj)
        {
            return obj is ComponentRef<T> r && r.EntityID.Equals(EntityID);
        }

        public static bool operator ==(ComponentRef<T> left, ComponentRef<T> right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(ComponentRef<T> left, ComponentRef<T> right)
        {
            return !(left == right);
        }

        public static implicit operator ComponentRef<T>(T component)
        {
            if (component == null)
            {
                return new ComponentRef<T>();
            }
            return new ComponentRef<T>(component.EntityID);
        }

        public static implicit operator ComponentRef<T>(EntityID id)
        {
            return new ComponentRef<T>(id);
        }

        public bool Equals(ComponentRef<T> other)
        {
            return other.EntityID.Equals(EntityID);
        }

        public override int GetHashCode()
        {
            return EntityID.GetHashCode();
        }

        public override string ToString()
        {
            return $"ComponentRef<{typeof(T).Name}> referencing = {EntityID}";
        }

    }
}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Nurose.ECS
{
    //source:https://trenki2.github.io/blog/2018/12/28/activator-createinstance-faster-alternative/
    public static class InstanceFactoryGeneric<TArg1, TArg2, TArg3>
    {
        private static ConcurrentDictionary<Type, Func<TArg1, TArg2, TArg3, object>> cachedFuncs = new();

        public static object CreateInstance(Type type, TArg1 arg1, TArg2 arg2, TArg3 arg3)
        {
            if (cachedFuncs.TryGetValue(type, out Func<TArg1, TArg2, TArg3, object> func))
                return func(arg1, arg2, arg3);
            else
                return CacheFunc(type, arg1, arg2, arg3)(arg1, arg2, arg3);
        }

        private static Func<TArg1, TArg2, TArg3, object> CacheFunc(Type type, TArg1 arg1, TArg2 arg2, TArg3 arg3)
        {
            var constructorTypes = new List<Type>();
            if (typeof(TArg1) != typeof(TypeToIgnore))
                constructorTypes.Add(typeof(TArg1));
            if (typeof(TArg2) != typeof(TypeToIgnore))
                constructorTypes.Add(typeof(TArg2));
            if (typeof(TArg3) != typeof(TypeToIgnore))
                constructorTypes.Add(typeof(TArg3));

            var parameters = new List<ParameterExpression>()
    {
      Expression.Parameter(typeof(TArg1)),
      Expression.Parameter(typeof(TArg2)),
      Expression.Parameter(typeof(TArg3)),
    };

            var constructor = type.GetConstructor(constructorTypes.ToArray());
            var constructorParameters = parameters.Take(constructorTypes.Count).ToList();
            var newExpr = Expression.New(constructor, constructorParameters);
            var lambdaExpr = Expression.Lambda<Func<TArg1, TArg2, TArg3, object>>(newExpr, parameters);
            var func = lambdaExpr.Compile();
            cachedFuncs.TryAdd(type, func);
            return func;
        }
    }
}
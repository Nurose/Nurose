﻿using Nurose.Console;
using Nurose.Core;
using Nurose.ImGUI;
using Nurose.Text;
using System;
using System.Collections.Generic;
using OpenTK.Audio.OpenAL;
using Nurose.ECS;
using Nurose.MsdfText;

namespace BesmsPlayground
{
    public static class TestWorldLoader
    {
        private const int textureAmount = 11;

        internal static void Load(ECSWorld World)
        {
            World.InputRefreshOption = InputRefreshOption.Draw;
            World.ResourceManager.Hold<Texture>("colored_packed", new Texture("Resources/Images/colored_packed.png") { MagFilter = TextureFilter.Nearest, MinFilter = TextureFilter.Nearest });
            for (int i = 0; i < textureAmount; i++)
            {
                GenerateTexture();
                World.ResourceManager.TryHold("image" + i, new Texture($"Resources\\Images\\{i}.png") { MagFilter = TextureFilter.Nearest, MinFilter = TextureFilter.Nearest });
            }


            
            Entity cameraEntity = new Entity(World.Components);
            World.Systems.Add<ShaderTestSystem>();
            World.Systems.Add<ConsoleSystem>();
            cameraEntity.AddComponent<KeyControlledCamera>();
            cameraEntity.AddComponent<Transform>();
            cameraEntity.AddComponent<Camera>();

            World.Systems.Add(new TrueBatchRenderer());
            World.Systems.Add<CameraSystem>();
            World.Systems.Add(new ModeSwitchingSystem());
        }

        private static Texture GenerateTexture()
        {
            var r = new List<float>();
            for (int i = 0; i < 13; i++)
            {
                r.Add(Utils.RandomFloat(0, 10));
            }

            var size = new Vector2Int((int)(r[0] * 5), (int)(r[0] * 5));
            var pixels = new RGBAPixel[(int)(Utils.Floor(size.X * size.Y))];
            int index = 0;


            for (int y = 0; y < (int)size.Y; y++)
            {
                for (int x = 0; x < (int)size.X; x++)
                {

                    pixels[index] = new Color(Utils.Sigmoid(x / r[2]) * y / x * r[5] / Utils.Sigmoid(y * r[7]), Utils.Sigmoid(x / r[3] + r[4]) / x * y * Utils.Sigmoid(y * r[8]), Utils.Sigmoid(x / r[5] + r[6]) * y * x / r[9]);
                    index++;
                }
            }

            return new Texture(size, pixels);
        }
    }
}

﻿using Nurose.Core;
using System;
using System.Diagnostics;
using System.Globalization;

namespace BesmsPlayground
{
    internal class Counter
    {
        private readonly Stopwatch stopwatch;
        public int CallAmount { get; set; }
        public double Elapsed { get; private set; }

        public string Name { get; }

        public Counter(string name)
        {
            stopwatch = new Stopwatch();
            stopwatch.Start();
            this.Name = name;
        }

        public void Call()
        {
            CallAmount++;
            Elapsed = stopwatch.Elapsed.TotalSeconds;
            if (Elapsed >= 1.0)
            {
             //   Logger.LogAnnouncement($"Time between {name} calls: " + Math.Round(1.0f / (CallAmount / Elapsed) * 1000, 1) + " ms");
                stopwatch.Restart();
                CallAmount = 1;
            }
        }
        public string GetRounded()
        {
            return Math.Round(CallAmount / Elapsed, 0).ToString(CultureInfo.InvariantCulture);
        }
    }
}

﻿layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texcoord;
layout (location = 2) in vec4 color;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform vec4 colorTint;


//uniform float time;

out vec2 uv;
out vec2 screenPos;
out vec4 vertexColor;

void main()
{
   gl_Position = ClipToWorld(model, view, projection, position);
   gl_Position.z = TransformDepth(position.z, ClippingPlanes(projection));
   uv = texcoord;
   vertexColor = color * colorTint;
   screenPos = gl_Position.xy;
}

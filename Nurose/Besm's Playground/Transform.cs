﻿using Nurose.ECS;
using System;
using System.Collections.Generic;

namespace Nurose.Core
{
    [Serializable]
    public class Transform : Component
    {
        public TransformData TransformData { get; private set; } = new TransformData().WithSize(Vector2.One);
        public event EventHandler<GenericEventArgs<Transform>> OnChildAdded;
        public event EventHandler<GenericEventArgs<Transform>> OnChildRemoved;

        public Transform Parent { get; set; }
        private readonly List<Transform> _children = new List<Transform>();
        public IReadOnlyList<Transform> Children { get => _children; }

        [NonSerialized]
        private Matrix3x2 modelMatrix;
        private bool needsMatrixRecalculation = true;


        public Vector2 LocalCenterPoint => LocalPosition + (LocalSize / 2);

        public Vector2 LocalPosition
        {
            get => TransformData.Position;
            set
            {
                TransformData = TransformData.WithPosition(value);
                needsMatrixRecalculation = true;
            }
        }
        public Vector2 LocalRotationPivot
        {
            get => TransformData.RotationPivot;
            set
            {
                TransformData = TransformData.WithRotationPivot(value);
                needsMatrixRecalculation = true;
            }
        }
        public float LocalRotation
        {
            get => TransformData.Rotation;
            set
            {
                TransformData = TransformData.WithRotation(value);
                needsMatrixRecalculation = true;
            }
        }
        public Vector2 LocalSize
        {
            get => TransformData.Size;
            set
            {
                TransformData = TransformData.WithSize(value);
                needsMatrixRecalculation = true;
            }
        }

        public Vector2 GlobalPosition
        {
            get
            {
                if (Parent == null)
                {
                    return LocalPosition;
                }
                return LocalPosition + Parent.GlobalPosition;
            }
            set
            {
                if (Parent == null)
                {
                    LocalPosition = value;
                    return;
                }
                LocalPosition = Parent.InverseTransformPoint(value);
            }
        }
        public float GlobalRotation
        {
            get
            {
                if (Parent == null)
                {
                    return LocalRotation;
                }
                return Parent.GlobalRotation + LocalRotation;
            }
        }

        public Matrix3x2 GetModelMatrix()
        {
            if (needsMatrixRecalculation)
            {
                modelMatrix = CalculateModelMatrix();
                needsMatrixRecalculation = false;
            }
            return modelMatrix;
        }

        private Matrix3x2 CalculateModelMatrix()
        {
            if (Parent == null)
            {
                return TransformData.CalcModelMatrix();
            }
            else
            {
                return TransformData.CalcModelMatrix() * Parent.GetModelMatrix();
            }
        }

        public Vector2 Right => Utils.DegreeToVector(GlobalRotation);
        public Vector2 Left => -Utils.DegreeToVector(GlobalRotation);
        public Vector2 Up => Utils.DegreeToVector(GlobalRotation + 90);
        public Vector2 Down => -Utils.DegreeToVector(GlobalRotation + 90);

        public void SetParent(Transform parent)
        {
            Parent = parent;
            parent._children.Add(this);
            needsMatrixRecalculation = true;
        }

        internal void AddChild(Transform child)
        {
            _children.Add(child);
            OnChildAdded?.Invoke(this, new GenericEventArgs<Transform>(child));
        }

        internal void RemoveChild(Transform child)
        {
            _children.Remove(child);
            OnChildRemoved?.Invoke(this, new GenericEventArgs<Transform>(child));
        }

        /// <summary>
        /// Transforms a point from local space to world 
        /// </summary>
        /// <returns></returns>
        public Vector2 TransformPoint(Vector2 point)
        {
            Vector2 result = GlobalPosition;

            result += Right * point.X;
            result += Up * point.Y;

            return result;
        }

        /// <summary>
        /// Transforms a point from world space to local space
        /// </summary>
        /// <returns></returns>
        public Vector2 InverseTransformPoint(Vector2 point)
        {
            Vector2 result = -GlobalPosition;

            result += Right * point.X;
            result += Up * point.Y;

            return result;
        }
    }
}

﻿using Nurose.Core;
using Nurose.ECS;
using Nurose.OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Diagnostics;

namespace BesmsPlayground
{
    public static class EntryProgram
    {
        public static void Main()
        {
            NuroseMain.SetWindowFactory<OpenTKWindowFactory>();
            NuroseMain.SetInputFactory<OpenTKInputFactory>();
            NuroseMain.SetAudioFactory<OpenTKAudioFactory>();
            Logger.LogAnnouncement(NuroseMain.Monitor.ContentScale);

            NuroseMain.Create(new WindowConstructorArgs
            {
                Title = "Besm's Playground",
                Size = (new Vector2(1020, 720) * NuroseMain.Monitor.ContentScale).ToVector2Int(),
                WindowBorder = WindowBorder.Resizable,
                StartCentered = true,
                NumberOfAntiAliasSamples = 4,
            });

            NuroseMain.AddLogic<ECSWorldLogic>();
            TestWorldLoader.Load(NuroseMain.GetLogic<ECSWorldLogic>().World);
            NuroseMain.Start();
        }
    }
}
﻿using Nurose.Core;
using Nurose.ECS;
using System;
using System.Collections.Generic;
using System.Text;

namespace BesmsPlayground
{
    class Bird : Component
    {
        public Vector2 Position { get; set; }
        public Vector2 Velocity { get; set; }
        public Vector2Int TextureIndex { get; set; }
        public ResourceRef<Texture> TextureRef { get; set; } 
        public float Rotation { get; set; }
        public string TextureResourceName { get; set; }
    }
}

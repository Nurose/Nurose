﻿using Nurose.Core;
using Nurose.ECS;
using System;

namespace BesmsPlayground
{
    public abstract class ModeSystem : ECSSystem
    {
        public abstract Type CompType { get; }
        public abstract string DisplayName { get; }
    }
}

﻿using Nurose.Core;
using Nurose.ImGUI;
using Nurose.Console;
using Nurose.Text;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Nurose.ECS;

namespace BesmsPlayground
{



    public class ModeSwitchingSystem : ECSSystem
    {
        private Counter drawCounter;
        private Counter fixedUpdateCounter;
        public int targetBirdAmount = 10_000;
        private int selectedMode = 1;
        private int currentMode ;

        
        public Type CurrentModeComponentType => modes[currentMode].CompType;
        private List<ModeSystem> modes;
        private void Start()
        {
            drawCounter = new Counter("FPS");
            fixedUpdateCounter = new Counter("UPS");
        }

        [Command]
        public void birds(int amount)
        {
            targetBirdAmount = amount;
            Logger.LogAnnouncement("Bird amount changed to " + targetBirdAmount);
        }

        private void FixedUpdate()
        {
          //  targetBirdAmount = (int)(Utils.Sin(Time.SecondsSinceStart) * 1000)+1000;
            fixedUpdateCounter.Call();
        }

        private void Draw()
        {
            if (Input.IsKeyHeld(Key.C))
            {
                World.Components = World.Components.DeepCopy();
            }
            drawCounter.Call();
        }

        private void AfterDraw()
        {
            if (modes == null)
            {
                modes = new List<ModeSystem>();
                IEnumerable<Type> modeTypes = Assembly.GetExecutingAssembly().GetTypes().Where(t => t.IsSubclassOf(typeof(ModeSystem)));
                foreach (var type in modeTypes)
                {
                    var system = (ModeSystem)Activator.CreateInstance(type);
                    World.Systems.Add(system);
                    modes.Add(system);
                }
            }

            KeepBirdAmountInCheck();

            //GUI();

            if (selectedMode != currentMode)
            {
                SwitchMode(selectedMode);
            }

        }

        /*  private void GUI()
          {
              context.UpdateIO(World);

              NuroseMain.Window.VSync = vsync;
              ImGui.NewFrame();
              ImGui.Begin("FPS");
              ImGui.Checkbox("VSync", ref vsync);
              ImGui.Text($"FPS = {drawCounter.GetRounded()}");
              ImGui.Text($"UPS = {fixedUpdateCounter.GetRounded()}");
              ImGui.Text($"Vendor = {NuroseMain.Window.GPUVendor}");
              ImGui.Text($"Renderer = {NuroseMain.Window.Renderer}");
              ImGui.Spacing();
              ImGui.SliderInt("Bird amount", ref targetBirdAmount, 0, 10000);
              ImGui.Combo("Mode", ref selectedMode, modes.Select(m => m.DisplayName).ToArray(), modes.Count);
              ImGui.End();
              ImGui.ShowDemoWindow();
              q.Draw(context.Render());
          }*/

        private void KeepBirdAmountInCheck()
        {
            IEnumerable<Bird> birds = World.Components.GetAll<Bird>();
            var birdAmount = birds.Count();
            if (birdAmount < targetBirdAmount)
            {
                for (int i = 0; i < targetBirdAmount - birdAmount; i++)
                {
                    BirdSpawner.Spawn(World);
                }
            }

            if (birdAmount > targetBirdAmount)
            {
                for (int i = 0; i < birdAmount - targetBirdAmount; i++)
                {
                    World.Components.RemoveAllOf(birds.ElementAt(i).EntityID);
                }
            }
        }

        private void SwitchMode(int selectedMode)
        {
            foreach (var comp in World.Components.Get(modes[currentMode].CompType))
            {
                World.Components.Remove(comp);
                Component comp1 = (Component)Activator.CreateInstance(modes[selectedMode].CompType);
                comp1.EntityID = comp.EntityID;
                World.Components.Add(comp1);
            }
            currentMode = selectedMode;
        }
    }
}

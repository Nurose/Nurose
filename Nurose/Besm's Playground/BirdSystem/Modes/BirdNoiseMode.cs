﻿using Nurose.Core;
using Nurose.ECS;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BesmsPlayground
{

    public class NoiseBird : Component
    {
        public Vector2 Velocity { get; set; }
        public float TimeLastSwirf { get; set; }
    }

    public class BirdNoiseMode : ModeSystem
    {
        private class BirdTransform
        {
            public Bird Bird;
            public NoiseBird NoiseBird;
            [OptionalComponent]
            public Transform Transform;
        }

        public override Type CompType => typeof(NoiseBird);

        public override string DisplayName => "Flying birds";

        private const int noiseZLayers = 40;
        private readonly float speed = 60;
        private float[][][] map;
        private Vector2 max = Vector2.One * 800;

        public void Start()
        {
            SimplexNoise.Noise.Seed = Utils.RandomInt(0, 10000);
            LoadNoiseMap();
        }

        private void LoadNoiseMap()
        {
            var ori = SimplexNoise.Noise.Calc3D((int)max.X, (int)max.Y, noiseZLayers, .01f);
            map = new float[ori.GetLength(0)][][];
            for (int x = 0; x < ori.GetLength(0); x++)
            {
                map[x] = new float[ori.GetLength(1)][];
                for (int y = 0; y < ori.GetLength(1); y++)
                {
                    map[x][y] = new float[ori.GetLength(2)];
                    for (int z = 0; z < ori.GetLength(2); z++)
                    {
                        map[x][y][z] = ori[x, y, z];
                    }
                }
            }
        }

        private float time = 0;
        private bool paused = false;

        

        public void Draw()
        {
            
            
            if (Input.IsKeyPressed(Key.P))
            {
                paused = !paused;
            }
            Time.TimeScale /= 1f - (Input.MouseWheelDelta / 5f);


            if (Input.IsKeyPressed(Key.L))
            {
                var list = new LinkedList<Vertex>();
                var que = new Queue<Vertex>();
                for (int i = 0; i < 100000; i++)
                {
                    Vertex item = new Vertex(Utils.RandomVector2(0, 1), Utils.RandomColor());
                    list.AddLast(item);
                    que.Enqueue(item);
                }
                foreach (var item in que)
                {
                    list.Remove(item);
                }

            }

            if (Input.IsKeyPressed(Key.K))
            {
                foreach (var bird in World.Components.GetAll<Bird>())
                {
                    var transform = World.Components.Get<Transform>(bird.EntityID);
                    World.Components.RemoveAllOf(bird.EntityID);
                }
            }
            
            if (paused)
            {
                return;
            }
            time += Time.DeltaTime * Time.TimeScale;
            if (Input.IsKeyHeld(Key.Space))
            {
                foreach (var fam in World.Components.GetByFamily<BirdTransform>())
                {
                    fam.Bird.Velocity = -fam.Bird.Position.Normalized;
                }
            }
            else
            {
                int timeAsInt = (int)time;
                foreach (var fam in World.Components.GetByFamily<BirdTransform>())
                {
                    if (fam.Bird.Position.Magnitude > max.X / 2)
                    {
                        fam.Bird.Position = -fam.Bird.Position;
                        fam.Bird.Position = fam.Bird.Position.Normalized * max.X / 2f * .9f;
                    }
                    if (fam.NoiseBird.TimeLastSwirf + 3 < time)
                    {
                        fam.Bird.Rotation += 720 * Time.DeltaTime * Time.TimeScale;
                        if (fam.Bird.Rotation >= 1720)
                        {
                            fam.Bird.Rotation = 0;
                            fam.NoiseBird.TimeLastSwirf = time + Utils.RandomFloat() * 2;
                        }
                    }
                    Vector2 newVel = Get(fam.Bird.Position, timeAsInt);
                    fam.Bird.Velocity = Utils.Lerp(fam.Bird.Velocity, newVel, Utils.MinMax(0,Time.DeltaTime,.05f) * Time.TimeScale);
                }
            }
            foreach (var fam in World.Components.GetByFamily<BirdTransform>())
            {
                //fam.Transform.LocalRotation += Time.DeltaTime * 360;
                fam.Bird.Position += fam.Bird.Velocity * speed * Time.DeltaTime * Time.TimeScale;
            }
        }

        private Vector2 Get(Vector2 pos, int t)
        {
            var c = pos + (max / 2);
            int x = (int)c.X;
            int y = (int)c.Y;
            float n1 = map[x][y][t % noiseZLayers];
            float n2 = map[x][(int)c.Y][(t + 1) % noiseZLayers];
            return Utils.DegreeToVector((Utils.Lerp(n1, n2, .5f) / 255) * 720);
        }
    }
    
}

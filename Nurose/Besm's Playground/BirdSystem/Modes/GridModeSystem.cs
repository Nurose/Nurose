﻿using Nurose.Core;
using Nurose.ECS;
using System;
using System.Linq;

namespace BesmsPlayground
{
    public class GridBird : Component
    {

    }

    public class GridModeSystem : ModeSystem
    {
        public override Type CompType => typeof(GridBird);

        public override string DisplayName => "Grid forced";

        struct Fam
        {
            public GridBird GridBird;
            public Bird Bird;

        }


        public void FixedUpdate()
        {
            Vector2 target = Vector2.Zero;
            var enumerable = World.Components.GetAll<Bird>();   
            int max = (int)Math.Sqrt(enumerable.Count());
            foreach (var bird in enumerable)
            {
                var grid = World.Components.Get<GridBird>(bird.EntityID);
                if(grid == null)
                    return;
                target.X++;
                if (target.X > max)
                {
                    target.X = 0;
                    target.Y++;
                }
                bird.Velocity = Utils.Lerp(bird.Velocity, (target - bird.Position), Time.FixedDeltaTime * Time.TimeScale * 5);
                float v = Vector2.DistanceSquared(bird.Velocity, target);
                bird.Position += bird.Velocity * 1 * Time.FixedDeltaTime * Time.TimeScale;

            }
        }

    }
}

﻿using Nurose.Core;
using Nurose.ECS;
using System;

namespace BesmsPlayground
{
    public static class BirdSpawner
    {
        private const int textureAmount = 10;

        public static EntityID Spawn(ECSWorld World)
        {
            Entity box = new Entity(World.Components);
            int waifuIndex = Utils.RandomInt(0, textureAmount);
            var bird = box.AddComponent<Bird>();
            var t = World.Systems.Get<ModeSwitchingSystem>().CurrentModeComponentType;
            box.AddComponent((Component)Activator.CreateInstance(t));
            bird.Position = Utils.RandomPointInUnitCircle() * 400;
            bird.TextureIndex = (Utils.RandomInt(0, 48), Utils.RandomInt(0, 21));
            var resourceName = "image" + waifuIndex;
            bird.TextureRef = World.ResourceManager.GetResourceRef<Texture>(resourceName);
            return box.Id;
        }
    }
}

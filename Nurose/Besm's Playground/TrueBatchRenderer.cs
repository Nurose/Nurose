﻿using Nurose.Core;
using Nurose.ECS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BesmsPlayground
{
    public class TrueBatchRenderer : ECSSystem
    {
        private const int ChunkCount = 10;
        private const int Z = 5;
        TextureMap map;
        VertexBuffer VertexBuffer;
        Vertex[] Vertices;
        Vertex[] sdfsdf;

        private Bird[] birds;

        public void Start()
        {
            map = new TextureMap(World.ResourceManager.Get<Texture>("colored_packed"), (16, 16));
            VertexBuffer = new VertexBuffer();
            Vertices = new Vertex[0];

            sdfsdf = new Vertex[]
{
                    new Vertex(new Vector3(0,0,1),Color.White,Vector2.Zero),
                    new Vertex(new Vector3(100,0,1),Color.White,new Vector2(0,1)),
                    new Vertex(new Vector3(100,100,1),Color.White, Vector2.One),
};
        }

        public void Draw()
        {



            birds = World.Components.GetAll<Bird>().ToArray();
            if (Vertices.Length != birds.Length * 6)
                Vertices = new Vertex[birds.Length * 6];


            Parallel.For(0, birds.Length / ChunkCount, SetChunk);
            VertexBuffer.SetVertices(Vertices);



            RenderQueue.StartGroup(1);
            RenderQueue.SetModelMatrix(Matrix3x2.Identity);
            RenderQueue.SetTexture(map.Texture);
            VertexBuffer.SetVertices(Vertices);
            RenderQueue.DrawVertexBuffer(VertexBuffer, PrimitiveType.Triangles);
            RenderQueue.EndGroup();
        }


        private void SetChunk(int i)
        {
            for (int k = 0; k < ChunkCount; k++)
            {
                int birdIndex = (i * ChunkCount) + k;
                var bird = birds[birdIndex];
                var r = map.GetRect(bird.TextureIndex.X, bird.TextureIndex.Y);
                int v = birdIndex * 6;
                int size = 1;

                Vertices[v + 0] = new Vertex(new Vector3(bird.Position.X, bird.Position.Y, Z), Color.White, r.LeftBot);
                Vertices[v + 1] = new Vertex(new Vector3(bird.Position.X, bird.Position.Y + size, Z), Color.White, r.LeftTop);
                Vertices[v + 2] = new Vertex(new Vector3(bird.Position.X + size, bird.Position.Y, Z), Color.White, r.RightBot);

                Vertices[v + 3] = new Vertex(new Vector3(bird.Position.X + size, bird.Position.Y + size, Z), Color.White, r.RightTop);
                Vertices[v + 4] = new Vertex(new Vector3(bird.Position.X, bird.Position.Y + size, Z), Color.White, r.LeftTop);
                Vertices[v + 5] = new Vertex(new Vector3(bird.Position.X + size, bird.Position.Y, Z), Color.White, r.RightBot); 
            }
        }
    }
}

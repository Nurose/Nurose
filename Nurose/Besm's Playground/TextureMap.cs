﻿using Nurose.Core;

namespace BesmsPlayground
{
    public class TextureMap
    {
        public Texture Texture;
        public Vector2Int EntrySize;
        public Vector2Int Amount;

        public TextureMap(Texture texture, Vector2Int entrySize)
        {
            Texture = texture;
            EntrySize = entrySize;
        }

        public FloatRect GetRect(int x, int y)
        {

            return new FloatRect(((x, y) * EntrySize) / (Vector2)Texture.Size, EntrySize / (Vector2)Texture.Size);
        }
    }
}

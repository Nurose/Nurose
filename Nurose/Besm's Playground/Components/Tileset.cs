﻿using Nurose.Core;
using System.ComponentModel;

namespace BesmsPlayground
{
    public class Tileset : Component
    {
        public Vector2 TileAmount { get; set; } = new Vector2(100, 100);
        public string TextureName { get; set; } = "ppTexture";
        public int BlockSize { get; set; } = 1;

        public Texture Texture { get; set; }
        public VertexBuffer VertexBuffer { get; set; }
    }
}

﻿using Nurose.Core;
using Nurose.ECS;
using System;

namespace BesmsPlayground
{
    public class TextureComponent : Component
    {
        [NonSerialized]
        private Texture texture;
        public string TextureResourceName { get; set; }
        public Texture Texture { get => texture; set => texture = value; }      
    }
}
﻿using Nurose.ECS;

namespace Nurose.Core
{
    public sealed class Camera : Component
    {
        public float NearPlane { get; set; }
        public float FarPlane { get; set; }
        public float PixelsPerUnit { get; set; } = 1;
    }
}
﻿uniform sampler2D mainTex;

in vec2 uv;
in vec2 screenPos;
in vec4 vertexColor;
out vec4 color;

//uniform float time;
void main()
{
    color = texture(mainTex, uv);
    color *= vertexColor;
}


﻿using Nurose.Core;
using System.Runtime.CompilerServices;

namespace BesmsPlayground
{
    public struct DrawBirdTask : IRenderTask
    {
        private static UnitRectangleDrawTask drawTask = new UnitRectangleDrawTask(Color.White,5);

        private Vector2 Position;
        private Texture Texture;

        public DrawBirdTask(Vector2 position, Texture texture)
        {
            Position = position;
            Texture = texture;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Execute(IRenderTarget rt)
        {
            rt.ProjectionMatrix = Matrix4x4.CreateOrthographicOffCenter(0, rt.Size.X, rt.Size.Y, 0, 1, 100);
            rt.ViewMatrix = Matrix4x4.Identity;
            rt.ModelMatrix = new TransformData(Position, ((Vector2)Texture.Size)).CalcModelMatrix();
            rt.Texture = Texture;
            drawTask.Execute(rt);
        }
    }
}

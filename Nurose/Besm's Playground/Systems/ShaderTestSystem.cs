﻿using Nurose.Core;
using Nurose.ECS;
using Nurose.MsdfText;
using System.IO;
using System.Linq;

namespace BesmsPlayground
{
    public class ShaderTestSystem : ECSSystem
    {
        MsdfFont font;
        MsdfTextDrawer drawer;
        public void Start()
        {
            var vertexShader = Shader.LoadFromFile("Resources/Shaders/vertexShader.glsl");
            var fragmentShader = Shader.LoadFromFile("Resources/Shaders/fragmentShader.glsl");
            font = MsdfFontGenerator.Generate(new MsdfFontGenerateArgs { FolderPath = "Resources/Fonts/Consolas" });
            ResourceManager.Hold("msdfDefaultfont", font);
            drawer = new MsdfTextDrawer(font);
        }

        public void Draw()
        {
            drawer.Text = "The quick brown fox jumps";
            drawer.LineHeight = 300000;
            drawer.InvertedY = true;
            drawer.ScaleStyle = ScaleStyle.ForceLineHeight;
            RenderQueue.StartGroup(0);
            RenderQueue.SetModelMatrix(new TransformData(Vector2.Zero, Vector2.One * 40).CalcModelMatrix());
            RenderQueue.Add(drawer);
            var c = drawer.MsdfFont.MsdfFontInfo.Glyphs.Single(g => g.unicode == drawer.Text[0]);
            foreach (var g in drawer.MsdfFont.MsdfFontInfo.Glyphs)
            {
                //   RenderQueue.DrawRect(new Vector2(g.planeBounds.left, g.planeBounds.bottom) - new Vector2(0, 10), new Vector2(g.planeBounds.right, g.planeBounds.top), Color.Red);
            }
            RenderQueue.EndGroup();
            // Drawer.SetShaderProgram(ShaderProgram);
        }
    }
}

﻿using Nurose.Core;

namespace BesmsPlayground
{
    /*
    public class TilesetRendererSystem : ComponentSystem
    {
        private static Vertex[] GenerateVertices(Tileset c)
        {
            var vertices = new Vertex[((int)(c.TileAmount.X * c.TileAmount.Y) * 4)];
            int i = 0;
            for (int x = 0; x < c.TileAmount.X; x++)
            {
                for (int y = 0; y < c.TileAmount.Y; y++)
                {
                    vertices[i + 0] = (new Vertex(
                        new Vector2((x + 0) * c.BlockSize, (y + 0) * c.BlockSize),
                         Color.White, new Vector2(0, 0)));
                    vertices[i + 1] = (new Vertex(
                         new Vector2((x + 0) * c.BlockSize, (y + 1) * c.BlockSize),
                         Color.White, new Vector2(1, 0)));
                    vertices[i + 2] = (new Vertex(
                        new Vector2((x + 1) * c.BlockSize, (y + 1) * c.BlockSize),
                        Color.White, new Vector2(1, 1)));
                    vertices[i + 3] = (new Vertex(
                         new Vector2((x + 1) * c.BlockSize, (y + 0) * c.BlockSize),
                        Color.White, new Vector2(0, 1)));

                    i += 4;

                }
            }
            return vertices;
        }

        public void Start()
        {
            var tiles = World.GetComponentsOfType<Tileset>();
            foreach (Tileset tileset in tiles)
            {
                Init(tileset);
            }
        }

        private void Init(Tileset tileset)
        {
            tileset.Texture = World.ResourceManager.Get<Texture>("ppTexture");
            tileset.VertexBuffer = new VertexBuffer(GenerateVertices(tileset));
        }

        public void Draw()
        {
            var tiles = World.GetComponentsOfType<Tileset>();
            foreach (Tileset tileset in tiles)
            {
                if (tileset.VertexBuffer == null)
                {
                    Init(tileset);
                }
                Drawer.SetTexture(tileset.Texture);
                Drawer.SetModelTransform(tileset.Transform);
                var task = new VertexRenderTask(PrimitiveType.Quads, tileset.VertexBuffer);
                Drawer.Draw(task);
            }
        }
    }
    */
}

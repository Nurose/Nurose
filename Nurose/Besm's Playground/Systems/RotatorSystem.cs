﻿using Nurose.Core;
using Nurose.ECS;
using System;

namespace BesmsPlayground
{
    public class RotatorSystem : ECSSystem
    {
        public void Update()
        {
            foreach (var fam in World.Components.GetAll<Transform>())
            {
                //fam.Transform.LocalRotation += 100.1f * Time.DeltaTime;
                fam.Children[0].LocalPosition += Vector2.Right * .5f * Time.DeltaTime;
            }
        }
    }
}

﻿using Nurose.Core;
using Nurose.ECS;
using Nurose.Text;
using System.Linq;
using System.Reflection;
using System.Windows;

namespace BesmsPlayground
{


    public class BatchRenderSystem : ECSSystem
    {
        private UnitRectangleDrawTask drawTask;
        private Material Material;

        private void Start()
        {
            drawTask = new UnitRectangleDrawTask(Color.White);
            Material = new Material();
        }

        private void AfterDraw()
        {
            if (Input.IsKeyPressed(Key.Space))
            {
                World.RenderTarget.SaveToFile("test.png");
            }
        }

        private void Draw()
        {
            foreach (var bird in World.Components.GetAll<Bird>())
            {
                Texture texture = bird.TextureRef.GetResource(World.ResourceManager);
                RenderQueue.Add(new DrawBirdTask(bird.Position, texture));
            }
        }
    }
}

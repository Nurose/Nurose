﻿using Nurose.Core;
using Nurose.ECS;
using Nurose.Console;
using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Nurose.OpenTK;

namespace BesmsPlayground
{

    public class KeyControlledCamera : Component
    {

    }

    public class CameraSystem : ECSSystem
    {
        struct ControlledCameraFamily
        {
            public KeyControlledCamera Camera;
            public Transform Transform;
        }

        private Stopwatch stopwatch;
        private int i = 0;
        private float MoveSpeed { get; set; } = 300f;

        [Command]
        private void Campos()
        {
          //  Logger.LogAnnouncement("Camera position="+World.Components.GetByFamily<ControlledCameraFamily>().First().Transform.GlobalPosition);
        }

        public void Start()
        {
            stopwatch = new Stopwatch();
            stopwatch.Start();
            
            //NuroseMain.AudioPlayer.Play(new AudioFile("test.wav"));
        }

        public void Draw()
        {
            i++;
            if(stopwatch.ElapsedMilliseconds > 1000)
            {
                Logger.LogAnnouncement($"FPS={Math.Round( i / stopwatch.Elapsed.TotalSeconds)}");
                stopwatch.Restart();
                i = 0;
            }
            

            foreach (var fam in World.Components.GetAll<Camera>())
            {
                var cam = fam;
                var transform = World.Components.Get<Transform>(fam.EntityID);
                if(transform == null) 
                    continue;
                
                World.Components.GetAll(cam.EntityID);
                float f = Time.DeltaTime;
                Vector2 dir = Vector2.Zero;

                if (Input.IsKeyHeld(Key.D)) { dir.X++; fallowing = false; }
                if (Input.IsKeyHeld(Key.A)) { dir.X--; fallowing = false; }
                if (Input.IsKeyHeld(Key.W)) { dir.Y++; fallowing = false; }
                if (Input.IsKeyHeld(Key.S)) { dir.Y--; fallowing = false; }

                if (Input.IsKeyHeld(Key.Q)) { transform.LocalRotation -= 30f * f; }
                if (Input.IsKeyHeld(Key.E)) { transform.LocalRotation += 30f * f; }

                if (Input.IsKeyHeld(Key.Equal)) { transform.LocalSize *= 1f + 2.5f * f; }
                if (Input.IsKeyHeld(Key.Minus)) { transform.LocalSize *= 1 / (1f + 2.5f * f); }

                transform.LocalPosition += (dir * MoveSpeed / transform.LocalSize) * f;
            }


            foreach (var fam in World.Components.GetAll<Camera>())
            {
                
                var transform = World.Components.Get<Transform>(fam.EntityID);
                if (fallowing)
                {
                    transform.LocalPosition = target.LocalPosition + (target.LocalSize / 2);
                }

                RenderQueue.StartGroup(0);
               RenderQueue.SetCameraMatrix(transform.TransformData.CalcViewMatrix());
                RenderQueue.EndGroup();
            }

            if (Input.IsKeyPressed(Key.V))
            {
                NuroseMain.Window.VSync = !NuroseMain.Window.VSync;
            }
        }

        bool fallowing = false;
        readonly Transform target;


    }
}
